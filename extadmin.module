<?php

/**
 * @file
 * ExtAdmin primary module file.
 */

/**
 * Implements hook_init().
 */
function extadmin_init() {
  global $language;

  // This module works only with ExtAdmin theme.
  if (variable_get('admin_theme', '0') != 'extadmin_theme') {
    return;
  }

  // Since ExtAdmin needs only one page,
  // redirect all other admin pages to /admin.
  if (substr($_GET['q'], 0, 6) == 'admin/') {
    drupal_goto('admin');
    return;
  }
  elseif ($_GET['q'] != 'admin') {
    // ExtAdmin is purely admin module,
    // so it doesn't need to run on other pages.
    return;
  }

  $path = drupal_get_path('module', 'extadmin');
  $theme_path = drupal_get_path('theme', 'extadmin_theme');

  drupal_add_js($path . '/ext/ext-all.js', 'module', 'header', FALSE, TRUE, FALSE);

  // Include ExtJS locale file based on current language.
  if ($language->language != 'en') {
    $parts = explode('-', $language->language);
    $filepath = '/ext/locale/ext-lang-' . (sizeof($parts) > 1 ? $parts[0] . '_' . strtoupper($parts[1]) : $parts[0]) . '.js';
    if (file_exists($path . $filepath)) {
      drupal_add_js($path . $filepath);
    }
  }

  // Include Ext.Direct API schema.
  drupal_add_js(
    (variable_get('clean_url', '0') ? '' : 'index.php?q=') . 'extadmin/api.js',
    'module',
    'header',
    FALSE,
    TRUE,
    FALSE
  );

  // Include ExtAdmin core JS files.
  drupal_add_js($path . '/app.js');

  drupal_add_js($path . '/app/ux/CenterLayout.js');
  drupal_add_js($path . '/app/ux/DirectComponentLoader.js');
  drupal_add_js($path . '/app/ux/DrupalComboBox.js');
  drupal_add_js($path . '/app/ux/GridColumns.js');
  drupal_add_js($path . '/app/ux/Date.js');
  drupal_add_js($path . '/app/ux/BasicForm.js');
  drupal_add_js($path . '/app/ux/FieldSet.js');
  drupal_add_js($path . '/app/ux/TabPanel.js');

  drupal_add_js($path . '/app/view/Extension.js');
  drupal_add_js($path . '/app/view/DrupalForm.js');
  drupal_add_js($path . '/app/view/BatchWindow.js');
  drupal_add_js($path . '/app/view/Login.js');
  drupal_add_js($path . '/app/view/Main.js');
  drupal_add_js($path . '/app/view/Welcome.js');
  drupal_add_js($path . '/app/view/Iframe.js');
  drupal_add_js($path . '/app/view/Header.js');

  drupal_add_js($path . '/app/controller/Base.js');
  drupal_add_js($path . '/app/controller/ListBase.js');
  drupal_add_js($path . '/app/controller/TreeBase.js');
  drupal_add_js($path . '/app/controller/SettingsBase.js');
  drupal_add_js($path . '/app/controller/Header.js');
  drupal_add_js($path . '/app/controller/Login.js');


  // Use ExtJS theme configured in ExtAdmin theme settings.
  $settings = theme_get_settings('adminext');
  drupal_add_css($path . '/ext/resources/css/ext-all' . ($settings['ext_theme'] ? '-' . $settings['ext_theme'] : '') . '.css');

  drupal_add_css($theme_path . '/css/Icons.css');
  drupal_add_css($theme_path . '/css/GridIcons.css');
  drupal_add_css($theme_path . '/css/Main.css');

  // Tell other modules that support ExtAdmin to initialize.
  extadmin_invoke_api('init', FALSE);
}

/**
 * Implements hook_menu().
 */
function extadmin_menu() {
  return array(
    'extadmin/api.js' => array(
      'page callback' => 'extadmin_api_schema',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
    'extadmin/api' => array(
      'page callback' => 'extadmin_api_router',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Chopped version of drupal_process_form with a few tweaks.
 *
 * We don't need the following:
 * - Form cache, as it doesn't work properly since we removed
 *   drupal_process_form call from our drupal_get_form implementation.
 * - Form redirects, because we use RPC to process forms
 *   and redirects break everything.
 * - Batch processing, same as above - no redirects.
 *
 * @see drupal_process_form()
 * @see extadmin_get_form()
 */
function extadmin_process_form($form_id, &$form, $data) {
  $form_state = array(
    'values' => array(),
    'storage' => NULL,
    'submitted' => FALSE,
  );
  $form['#post'] = $data;
  $form = form_builder($form_id, $form, $form_state);
  $form_state['process_input'] = TRUE;

  // Sometimes we need to disable the validation.
  if (!$form['#skip_validation']) {
    drupal_validate_form($form_id, $form, $form_state);
  }
  form_clean_id(NULL, TRUE);

  $errors = form_get_errors();
  if (!$errors) {
    form_execute_handlers('submit', $form, $form_state);
  }
  return $form_state;
}

/**
 * Chopped version of drupal_get_form with a few tweaks.
 *
 * Removed the following:
 * - Form cache, since we don't do it in extadmin_process_form either.
 * - Form processing, because we just need form fields.
 * - Form rendering, we don't need HTML as we convert
 *   form presentation into JSON.
 *
 * @see drupal_get_form
 */
function extadmin_get_form($form_id, $values = array()) {
  $args = func_get_args();

  // A little tweak to pass #storage data through values.
  if ($values['#storage']) {
    $storage = $values['#storage'];
    unset($values['#storage']);
  }
  $form_state = array(
    'storage' => $storage,
    'submitted' => FALSE,
    'values' => $values,
    'post' => $values,
  );

  // Sometimes we need to get form state values from $_POST.
  if (is_array($_POST['#extadmin_form_state'])) {
    $form_state += $_POST['#extadmin_form_state'];
  }

  $args[1] = &$form_state;
  $form = call_user_func_array('drupal_retrieve_form', $args);
  drupal_prepare_form($form_id, $form, $form_state);
  return $form;
}

/**
 * Generates common JSON presentation for ExtJS.
 */
function extadmin_default_config($id, $item, $form) {
  return array(
    'xtype' => $item['#type'],
    'disabled' => $item['#disabled'],
    'itemId' => implode('-', array_slice($id, 1)),
    'fieldLabel' => $item['#title'],
    'name' => $item['#name'],
    'allowBlank' => !$item['#required'],
    'value' => $item['#default_value'],
  );
}

/**
 * Convert 'item' form element into Ext.form.field.Display.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Display
 */
function extadmin_item_config($id, $item, $form) {
  $result = extadmin_default_config($id, $item, $form);
  $result['xtype'] = 'displayfield';
  $result['value'] = $item['#value'];
  return $result;
}

/**
 * Show 'textfield' form element with auto complete as Ext.form.field.ComboBox.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.ComboBox
 */
function extadmin_textfield_config($id, $item, $form) {
  $result = extadmin_default_config($id, $item, $form);
  if ($item['#autocomplete_path']) {
    $result['xtype'] = 'drupalcombo';
    $result['autocomplete_path'] = $item['#autocomplete_path'];
    $result['valueField'] = 'id';
    $result['displayField'] = 'text';
    $result['valueNotFoundText'] = $result['value'];
    $result['forceSelection'] = TRUE;
    $result['minChars'] = 1;
    $result['triggerAction'] = 'query';
    $result['typeAhead'] = TRUE;
  }
  return $result;
}

/**
 * Convert 'password' form element to Ext.form.field.Text.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Text-cfg-inputType
 */
function extadmin_password_config($id, $item, $form) {
  $result = extadmin_default_config($id, $item, $form);
  $result['xtype'] = 'textfield';
  $result['inputType'] = 'password';
  return $result;
}

/**
 * Configure 'checkbox' form element.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Checkbox
 */
function extadmin_checkbox_config($id, $item, $form) {
  $result = extadmin_default_config($id, $item, $form);
  unset($result['fieldLabel']);
  $result['boxLabel'] = $item['#title'];
  $result['checked'] = ($item['#default_value'] == $item['#return_value']);
  $result['inputValue'] = $item['#return_value'];
  return $result;
}

/**
 * Configure 'radio' form element.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Radio
 */
function extadmin_radio_config($id, $item, $form) {
  return extadmin_checkbox_config($id, $item, $form);
}

/**
 * Configure 'textarea' form element.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.TextArea
 */
function extadmin_textarea_config($id, $item, $form) {
  $result = extadmin_default_config($id, $item, $form);
  if ($item['#rows']) {
    // Calculate height based on #rows setting.
    $result['height'] = $item['#rows'] * 20;
  }

  // Enforce minimum height.
  if (intval($result['height']) < 100) {
    $result['height'] = 100;
  }

  $resizable = $item['#resizable'];
  if ($resizable || !$resizable && !isset($resizable)) {
    $result['resizable'] = array(
      'pinned' => TRUE,
      'handles' => 'se',
    );
  }
  return $result;
}

/**
 * Convert 'file' form element into Ext.form.field.File.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.File
 */
function extadmin_file_config($id, $item, $form) {
  $result = extadmin_default_config($id, $item, $form);
  $result['xtype'] = 'filefield';
  return $result;
}

/**
 * Configure 'hidden' form element.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Hidden
 */
function extadmin_hidden_config($id, $item, $form) {
  return array(
    'xtype' => 'hidden',
    'name' => $item['#name'],
    'value' => $item['#value'],
  );
}

/**
 * Represent 'value' form element as Ext.form.field.Hidden.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Hidden
 */
function extadmin_value_config($id, $item, $form) {
  return extadmin_hidden_config($id, $item, $form);
}

/**
 * Represent 'token' form element as Ext.form.field.Hidden.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Hidden
 */
function extadmin_token_config($id, $item, $form) {
  return extadmin_hidden_config($id, $item, $form);
}

/**
 * Convert different #options array variations to one format.
 *
 * @see extadmin_select_config()
 */
function extadmin_convert_options($options) {
  $result = array();
  if (!is_array($options)) {
    return $result;
  }

  foreach ($options as $key => $value) {
    if (is_object($value)) {
      $key = array_pop(array_keys($value->option));
      $value = array_pop(array_values($value->option));
    }
    if (is_array($value)) {
      $result = array_merge($result, extadmin_convert_options($value));
    }
    else {
      $result[$key] = $value;
    }
  }
  return $result;
}

/**
 * Represent 'select' form element as Ext.form.field.ComboBox.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.ComboBox
 */
function extadmin_select_config($id, $item, $form, $save_data = TRUE) {
  $options = extadmin_convert_options($item['#options']);
  $result = extadmin_default_config($id, $item, $form);

  // If it is weight selector we make it into Ext.form.field.Number.
  if ($item['#is_weight']) {
    $result['xtype'] = 'numberfield';
    return $result;
  }

  $result['xtype'] = 'drupalcombo';
  $result['valueField'] = 'id';
  $result['displayField'] = 'text';

  // We need to display current value somehow, since all values are loaded
  // only when you click on combobox.
  // So we use valueNotFoundText option to display current value.
  if (is_array($result['value'])) {
    // If there are multiple values selected, show them all separated by comma.
    if ($result['value']) {
      $values = array();
      foreach ($result['value'] as $value) {
        $values[] = $options[$value];
      }
      $result['valueNotFoundText'] = implode(', ', $values);
    }
    else {
      $result['valueNotFoundText'] = strval($options['']);
    }
    $result['value'] = implode(',', $result['value']);
  }
  else {
    $result['valueNotFoundText'] = strval($options[$result['value']] ? $options[$result['value']] : $result['value']);
  }

  $result['editable'] = FALSE;
  $result['forceSelection'] = TRUE;
  if ($item['#multiple']) {
    // Some fields have #multiple == TRUE, yet they don't have array
    // indicator next to their name, so we fix that.
    if (strpos($result['name'], '[]') === FALSE) {
      $result['name'] .= '[]';
    }
    $result['multiSelect'] = TRUE;
  }

  // Save options, so we can retrieve them later.
  if ($save_data) {
    $data_id = implode('.', $id);
    $result['data_id'] = $data_id;
    extadmin_save_form_data($data_id, $options);
  }

  return $result;
}

/**
 * Configure 'fieldset' form element.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.FieldSet
 */
function extadmin_fieldset_config($id, $item, $form) {
  $items = _extadmin_get_form_items($id, $item, $form);
  // What's the point of empty fieldset?
  if (!sizeof($items)) {
    return;
  }

  return array(
    'xtype' => 'fieldset',
    'itemId' => implode('-', array_slice($id, 1)),
    'title' => $item['#title'],
    'collapsible' => $item['#collapsible'],
    'collapsed' => $item['#collapsed'],
    'items' => $items,
  );
}

/**
 * Show 'password_confirm' element as Ext.form.FieldSet
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.FieldSet
 */
function extadmin_password_confirm_config($id, $item, $form) {
  $result = extadmin_fieldset_config($id, $item, $form);
  $result['style'] = array(
    'border' => 0,
    'padding' => 0,
  );
  return $result;
}

/**
 * Represent 'radios' form element as Ext.form.RadioGroup
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.RadioGroup
 */
function extadmin_radios_config($id, $item, $form) {
  $result = array(
    'xtype' => 'radiogroup',
    'itemId' => implode('-', array_slice($id, 1)),
    'fieldLabel' => $item['#title'],
    'columns' => 1,
    'items' => array(),
  );
  if (is_array($item['#options'])) {
    foreach ($item['#options'] as $value => $text) {
      $result['items'][] = array(
        'name' => $item['#name'],
        'checked' => $value == $item['#default_value'],
        'inputValue' => $value,
        'boxLabel' => $text,
      );
    }
  }
  return $result;
}

/**
 * Represent 'checkboxes' form element as Ext.form.CheckboxGroup
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.CheckboxGroup
 */
function extadmin_checkboxes_config($id, $item, $form) {
  $result = array(
    'xtype' => 'checkboxgroup',
    'itemId' => implode('-', array_slice($id, 1)),
    'fieldLabel' => $item['#title'],
    'columns' => 1,
    'items' => array(),
  );
  if (is_array($item['#options'])) {
    foreach ($item['#options'] as $value => $text) {
      $name = $item[$value] ? $item[$value]['#name'] : $item['#name'];
      $result['items'][] = array(
        'name' => $name,
        'checked' => is_array($item['#default_value']) ? in_array($value, $item['#default_value'], TRUE) : $value == $item['#default_value'],
        'inputValue' => $value,
        'boxLabel' => $text,
      );
    }
  }
  return $result;
}

/**
 * Get ExtJS config for a form item.
 */
function _extadmin_get_form_item($item_id, $item, $form) {
  // Try to get module specific config first.
  $result = extadmin_invoke_api('item_config', FALSE, $item_id, $item, $form);
  if (!$result) {
    // Modules don't know how to convert the item,
    // run built-in function if available.
    $function = 'extadmin_' . $item['#type'] . '_config';
    if (!function_exists($function)) {
      return;
    }

    $result = $function($item_id, $item, $form);
    if (!$result) {
      return;
    }
  }

  // Represent fields with prefix as Ext.form.FieldContainer.
  if ($item['#field_prefix']) {
    $label = $result['fieldLabel'];
    unset($result['fieldLabel']);

    $result = array(
      'fieldLabel' => $label,
      'xtype' => 'fieldcontainer',
      'layout' => 'hbox',
      'items' => array(
        array(
          'xtype' => 'displayfield',
          'value' => $item['#field_prefix'],
        ),
        array(
          'xtype' => 'splitter',
        ),
        $result,
      ),
    );
  }

  // Represent fields with suffix as Ext.form.FieldContainer.
  if ($item['#field_suffix']) {
    $items = array(
      array(
        'xtype' => 'splitter',
      ),
      array(
        'xtype' => 'displayfield',
        'value' => $item['#field_suffix'],
      ),
    );

    // The field has both suffix and prefix.
    if ($item['#field_prefix']) {
      $result['items'] = array_merge($result['items'], $items);
    }
    else {
      $label = $result['fieldLabel'];
      unset($result['fieldLabel']);
      array_unshift($items, $result);
      $result = array(
        'fieldLabel' => $label,
        'xtype' => 'fieldcontainer',
        'layout' => 'hbox',
        'items' => $items,
      );
    }
  }

  return $result;
}

/**
 * Convert form items array into ExtJS presentation.
 */
function _extadmin_get_form_items($id, $element, $form) {
  uasort($element, 'element_sort');
  $items = array();
  foreach ($element as $name => $item) {
    if (!is_array($item)) {
      continue;
    }
    $item_id = array_merge($id, array(str_replace(' ', '_', $name)));
    if ($item['#type']) {
      $result = _extadmin_get_form_item($item_id, $item, $form);
      if ($result && !$result['#extadmin_ignore']) {
        $items[] = $result;
      }
    }
    else {
      $items = array_merge($items, _extadmin_get_form_items($item_id, $item, $form));
    }
  }
  return $items;
}

/**
 * Convert form into ExtJS presentation.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.Panel
 */
function extadmin_get_form_items($form_id) {
  global $user;

  // We need a special case for login form because it generates access denied
  // if called with logged in user.
  // So we fake anonymous call.
  if ($form_id == 'user_login') {
    $uid = $user->uid;
    $user->uid = 0;
  }

  $form_state = array();
  $form = call_user_func_array('extadmin_get_form', func_get_args());
  $form = form_builder($form_id, $form, $form_state);

  // Restore status quo.
  if ($uid) {
    $user->uid = $uid;
  }

  return _extadmin_get_form_items(array(str_replace('-', '_', $form['#id'])), $form, $form);
}

/**
 * Save for data into session to use later.
 */
function extadmin_save_form_data($data_id, $data) {
  $items = array();
  foreach ($data as $id => $item) {
    $items[] = array(
      'id' => $id,
      'text' => htmlspecialchars($item),
    );
  }
  $_SESSION['extadmin']['data'][$data_id] = $items;
}

/**
 * Implements extadmin_schema() hook.
 */
function extadmin_extadmin_schema() {
  module_load_include('php', 'extadmin', 'services/ServiceBase.class');
  module_load_include('php', 'extadmin', 'services/FormService.class');
  module_load_include('php', 'extadmin', 'services/NavigationService.class');
  module_load_include('php', 'extadmin', 'services/SessionService.class');

  return array(
    new FormService(),
    new NavigationService(),
    new SessionService(),
  );
}

/**
 * Implements extadmin_access hook.
 */
function extadmin_extadmin_access($action, $method, $data) {
  // Before we output form presentation,
  // we try to check if user has access to it.
  if ($action == 'FormService') {
    $form_id = $method == 'Process' ? $_POST['form_id'] : $data['form_id'];
    $args = unserialize(db_result(db_query("SELECT access_arguments FROM menu_router WHERE page_callback='drupal_get_form' AND access_callback='user_access' AND page_arguments LIKE '%%%s%%'", $form_id)));
    if ($args) {
      return user_access($args[0]);
    }
  }
  return TRUE;
}

/**
 * Helper function to do ExtAdmin API calls.
 */
function extadmin_invoke_api($op, $by_module) {
  $return = array();
  foreach (module_implements('extadmin_' . $op) as $module) {
    $args = array_slice(func_get_args(), 2);
    $result = call_user_func_array($module . '_extadmin_' . $op, $args);
    if (!isset($result)) {
      continue;
    }

    // The caller requested to get API results by module.
    if ($by_module) {
      if (!is_array($return[$module])) {
        $return[$module] = array();
      }
      if (is_array($result)) {
        $return[$module] = array_merge($return[$module], $result);
      }
      else {
        $return[$module][] = $result;
      }
    }
    else {
      if (is_array($result)) {
        $return = array_merge($return, $result);
      }
      else {
        $return[] = $result;
      }
    }
  }
  return $return;
}

/**
 * Outputs Ext.Direct schema.
 *
 * @see http://www.sencha.com/products/extjs/extdirect
 */
function extadmin_api_schema() {
  $actions = array();
  $schema = extadmin_invoke_api('schema', FALSE);
  foreach ($schema as $key => $item) {

    // Check what kind of implementation we have.
    if (is_object($item)) {
      // It is class implementation so use reflection to build schema.
      $r = new ReflectionClass($item);
      $key = $r->getName();

      $methods = array();
      foreach ($r->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
        $form_handler = FALSE;
        $params = array();
        foreach ($method->getParameters() as $param) {
          if (!$param->isOptional()) {
            $params[] = $param->getName();
          }
          elseif ($param->getName() == 'formHandler') {
            // formHandler is a special flag on Ext.Direct methods,
            // it tells Provider that this method handles form submits.
            // We use function variable with default value as
            // some sort of attribute, since PHP doesn't support attributes.
            $form_handler = $param->getDefaultValue();
          }
        }
        if (sizeof($params)) {
          $method = array(
            'name' => $method->name,
            'params' => $params,
          );
        }
        else {
          $method = array(
            'name' => $method->name,
            'len' => 0,
          );
        }
        if ($form_handler) {
          $method['formHandler'] = TRUE;
        }
        $methods[] = $method;
      }
      $actions[$key] = $methods;
    }
    elseif (is_array($item)) {
      // No need to use reflection, module returned everything we want.
      $actions[$key] = $item;
    }
  }

  print 'Ext.direct.Manager.addProvider(' . json_encode(array(
    'url' => base_path() . (variable_get('clean_url', '0') ? '' : 'index.php?q=') . 'extadmin/api',
    'namespace' => 'ExtAdmin',
    'type' => 'remoting',
    'actions' => $actions,
  )) . ');' . "\n";
}

/**
 * Helper function to call extadmin_access hook.
 */
function extadmin_check_access($module, $action, $method, $data) {
  $function = $module . '_extadmin_access';
  if (function_exists($function) && !call_user_func($function, $action, $method, $data)) {
    throw new Exception(t('Access denied.'));
  }
}

/**
 * Execute RPC call.
 */
function extadmin_execute_call($action, $method, $data) {
  $schema = extadmin_invoke_api('schema', TRUE);

  // Try and locate implementation.
  foreach ($schema as $module => $services) {
    foreach ($services as $key => $item) {
      // Check if it is a class implementation.
      if (is_object($item)) {
        // Current class doesn't have the function client is trying to call.
        if (get_class($item) != $action) {
          continue;
        }

        // Check access before we continue with execution.
        extadmin_check_access($module, $action, $method, $data);
        $method = new ReflectionMethod($action, $method);
        $args = array();
        foreach ($method->getParameters() as $param) {
          if ($param->isOptional()) {
            continue;
          }

          $args[] = $data[$param->getName()];
        }

        return $method->invokeArgs($item, $args);
      }
      else {
        // Check if this hook implementation has the function.
        if ($key != $action) {
          continue;
        }

        // Check access before calling the hook.
        extadmin_check_access($module, $action, $method, $data);
        return call_user_func($module . '_extadmin_call', $action, $method, $data);
      }
    }
  }

  // Didn't find anything, tell that to the client.
  throw new Exception(t('Access denied.'));
}

/**
 * Process RPC call.
 */
function extadmin_process_call($call) {
  try {
    return array(
      'type' => $call->type,
      'tid' => $call->tid,
      'action' => $call->action,
      'method' => $call->method,
      'result' => extadmin_execute_call(
        $call->action,
        $call->method,
        $call->data
      ),
    );
  }
  catch(Exception $e) {
    $call_array = array_map(create_function('$x', 'return check_plain($x);'), (array)$call);
    watchdog('extadmin', $call_array['action'] . '.' . $call_array['method'] . ': ' . $e->getMessage(), $call_array, WATCHDOG_WARNING);
    return array(
      'type' => 'exception',
      'tid' => $call->tid,
      'message' => $e->getMessage(),
      'where' => basename($e->getFile()) . ': ' . $e->getLine(),
      'method' => $call->method,
      'action' => $call->action,
      'result' => array('success' => FALSE),
    );
  }
}

/**
 * Entry point for Ext.Direct calls.
 *
 * @see http://www.sencha.com/products/extjs/extdirect
 */
function extadmin_api_router() {
  // Check if we received a regular POST first.
  if ($_POST['extAction'] && $_POST['extMethod']) {
    $call = new stdClass();
    $call->type = $_POST['extType'];
    $call->tid = $_POST['extTID'];
    $call->action = $_POST['extAction'];
    $call->method = $_POST['extMethod'];
    $call->data = $_POST;
    $result = extadmin_process_call($call);
  }
  else {
    // Read the request from STDIN.
    $request = json_decode(file_get_contents('php://input'));

    if (is_array($request)) {
      // Process multiple calls batched into one request.
      $result = array();
      foreach ($request as $call) {
        $call->data = $call->data ? get_object_vars($call->data) : array();
        $result[] = extadmin_process_call($call);
      }
    }
    else {
      $request->data = $request->data ? get_object_vars($request->data) : array();
      $result = extadmin_process_call($request);
    }
  }

  print json_encode($result);
}
