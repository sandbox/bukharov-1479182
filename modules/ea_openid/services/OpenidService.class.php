<?php

/**
 * @file
 * OpenidService class definition.
 */

/**
 * OpenID authentication service.
 */
class OpenidService extends ServiceBase {

  /**
   * The initial step of OpenID authentication.
   *
   * This function is almost the exact copy of openid_begin.
   * We needed slightly different error handling and no redirects.
   *
   * @see openid_begin()
   */
  public function Authenticate($claimed_id) {
    module_load_include('inc', 'openid');

    $claimed_id = _openid_normalize($claimed_id);

    $services = openid_discovery($claimed_id);
    if (!sizeof($services)) {
      return array(
        'success' => FALSE,
        'error' => t('Sorry, that is not a valid OpenID. Please ensure you have spelled your ID correctly.'),
      );
    }

    $service = $services[0];
    $op_endpoint = $service['uri'];

    // Store discovered information in the users' session
    // so we don't have to rediscover.
    $_SESSION['openid']['service'] = $service;
    $_SESSION['openid']['claimed_id'] = $claimed_id;

    // If bcmath is present, then create an association.
    $assoc_handle = '';
    if (function_exists('bcadd')) {
      $assoc_handle = openid_association($op_endpoint);
    }

    // Now that there is an association created, move on
    // to request authentication from the IdP
    // First check for LocalID. If not found, check for Delegate. Fall
    // back to $claimed_id if neither is found.
    if (!empty($service['localid'])) {
      $identity = $service['localid'];
    }
    elseif (!empty($service['delegate'])) {
      $identity = $service['delegate'];
    }
    else {
      $identity = $claimed_id;
    }

    if (isset($service['types']) && is_array($servic['types']) && in_array(OPENID_NS_2_0 . '/server', $service['types'])) {
      $claimed_id = $identity = 'http://specs.openid.net/auth/2.0/identifier_select';
    }

    return array(
      'success' => TRUE,
      'message' => openid_authentication_request($claimed_id, $identity, url('openid/authenticate', array('absolute' => TRUE, 'query' => array('destination' => 'admin'))), $assoc_handle, $services[0]['version']),
      'service' => $service,
    );
  }
}
