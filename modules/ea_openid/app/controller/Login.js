/**
 * Override standard controller to respect field sets.
 */
Ext.override(ExtAdmin.controller.Login, {
  login: function(button) {
    // Use standard login only when OpenID login is collapsed.
    if(!button.up('form').down('#local').collapsed) {
      this.callOverridden(arguments);
    }
  },

  reset: function(button) {
    var form = button.up('form');
    // Return to standard login form, collapse OpenID login.
    if(form.down('#local').collapsed) {
      form.down('#openid').collapse();
      form.down('#local').expand();
    }
    this.callOverridden(arguments);
  }
});

/**
 * Controller for OpenID login.
 */
Ext.define('ExtAdmin.openid.controller.Login', {
  extend: 'Ext.app.Controller',

  refs: [{
    ref: 'local',
    selector: 'user_login #local'
  }, {
    ref: 'openid',
    selector: 'user_login #openid'
  }, {
    selector: 'user_login #login',
    ref: 'button'
  }],

  init: function() {
    this.control({
      'user_login #local': {
        expand: function() {
          this.getOpenid().collapse();
        },
        collapse: function() {
          this.getOpenid().expand();
        }
      },
      'user_login #openid': {
        expand: function() {
          this.getLocal().collapse();
        },
        collapse: function() {
          this.getLocal().expand();
        }
      },
      'user_login #login': {
        click: this.login
      },
      'user_login textfield': {
        specialkey: function(field, e) {
          if(e.getKey() == e.ENTER) {
            this.login(this.getButton());
          }
        }
      }
    });
  },

  login: function() {
    if(this.getOpenid().collapsed) {
      return;
    }

    var field = this.getOpenid().down('#openid_identifier');
    var openid = field.getValue();
    if(!openid) {
      return;
    }

    ExtAdmin.OpenidService.Authenticate({ claimed_id: openid }, function(result) {
      if(!result.success) {
        field.markInvalid(result.error);
        return;
      }

      if(result.service.version == 2) {
        this.processAuth(result.service, result.message);
      }
      else {
        window.location.href = result.service.uri + '?' + Ext.Object.toQueryString(response.message);
      }
    }, this);
  },

  processAuth: function(service, message) {
    var items = [];
    Ext.Object.each(message, function(key, value) {
      items.push({
        xtype: 'hidden',
        name: key,
        value: value
      });
    });

    var panel = Ext.create('Ext.form.Panel', {
      renderTo: Ext.getBody(),
      hidden: true,
      items: items,
      url: service.uri,
      standardSubmit: true
    });

    panel.getForm().submit({
      success: function() {
        panel.destroy();
      }
    });
  }
});
