<?php

/**
 * @file
 * NodeService class definition.
 */

include_once drupal_get_path('module', 'node') . '/node.pages.inc';
include_once drupal_get_path('module', 'node') . '/node.admin.inc';
include_once drupal_get_path('module', 'node') . '/content_types.inc';

/**
 * Content nodes management service.
 */
class NodeService extends ServiceBase {

  /**
   * Performs operations on content.
   *
   * @see node_admin_nodes_submit()
   */
  public function PerformOperation($operation, $nids) {
    $form_id = 'node_admin_content';
    $data = array(
      'operation' => $operation,
      'op' => $operation == 'delete' ? t('Delete all') : t('Update'),
      'confirm' => TRUE,
      'form_id' => $form_id,
    );
    foreach ($nids as $nid) {
      $data['nodes'][$nid] = $nid;
    }
    $form = extadmin_get_form($form_id, $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    $form['#skip_validation'] = TRUE;
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Returns a list of possible operations and available content types.
   */
  public function GetListOptions() {
    $result = array();
    $result['operations'] = array();
    foreach (module_invoke_all('node_operations') as $operation => $array) {
      $result['operations'][$operation] = $array['label'];
    }

    $result['types'] = array();
    foreach (node_get_types('types', NULL, TRUE) as $type) {
      $result['types'][] = array(
        'type' => $type->type,
        'name' => $type->name,
      );
    }
    return $result;
  }

  /**
   * Returns a list of content nodes.
   *
   * @see node_admin_nodes()
   */
  public function GetList($page, $start, $limit, $sort) {
    if ($sort) {
      switch ($sort[0]->property) {
        case 'type_name':
          $order_by = 'b.name';
          break;

        case 'uid__name':
          $order_by = 'c.name';
          break;

        default:
          $order_by = 'a.' . $sort[0]->property;
          break;
      }
      $order_by = ' ORDER BY ' . $order_by . ' ' . $sort[0]->direction;
    }

    $count = db_result(db_query("SELECT COUNT(*) FROM {node} n WHERE language != ''"));
    $multilanguage = (module_exists('locale') || $count);
    if ($multilanguage) {
      $languages = language_list();
    }

    $nodes = array();
    $count = db_result(db_query('SELECT count(*) FROM {node}'));
    $result = db_query('SELECT a.*, b.name type__name, c.name uid__name FROM {node} a INNER JOIN {node_type} b ON (a.type=b.type) INNER JOIN {users} c ON (a.uid=c.uid)' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($node = db_fetch_object($result)) {
      if ($multilanguage) {
        $node->language = $node->language ? t($languages[$node->language]->name) : '';
      }
      $nodes[] = $node;
    }
    return array(
      'total' => $count,
      'nodes' => $nodes,
    );
  }

  /**
   * Set up node object.
   */
  private function initNode($type, $nid, $params) {
    global $user;

    if ($params) {
      $params = json_decode($params, TRUE);
      foreach ($params as $k => $v) {
        $_GET[$k] = $v;
      }
    }

    if ($nid) {
      $node = node_load($nid);
      $type = $node->type;
    }
    else {
      $node = array(
        'uid' => $user->uid,
        'name' => (isset($user->name) ? $user->name : ''),
        'type' => $type,
        'language' => '',
      );
    }
    return $node;
  }

  /**
   * Saves changes to a content node or creates a new one.
   *
   * @see node_form_submit()
   */
  public function Update($type, $params, $formHandler = TRUE) {
    $node = $this->initNode($type, $_POST['nid'], $params);

    if (!$_POST['teaser_js']) {
      unset($_POST['teaser_js']);
    }

    $form_id = $type . '_node_form';
    $form = extadmin_get_form($form_id, array(), $node);
    $form_state = extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult(array('nid' => $form_state['nid'], 'title' => $form_state['node']['title']));
  }

  /**
   * Generates the node add/edit form.
   *
   * @see node_form()
   */
  public function GetForm($type, $nid, $params) {
    $node = $this->initNode($type, $nid, $params);
    if (is_object($node)) {
      $type = $node->type;
    }

    return extadmin_get_form_items($type . '_node_form', array(), $node);
  }

  /**
   * Rebuild node access permissions.
   */
  public function RebuildPermissions() {
    node_access_rebuild(TRUE);
    $batch =& batch_get();
    return array(
      'success' => TRUE,
      'batch' => $batch ? $batch : NULL,
    );
  }
}
