<?php

/**
 * @file
 * NodeTypeService class definition.
 */

/**
 * Content types management service.
 */
class NodeTypeService extends ServiceBase {

  /**
   * Returns a list of available content types.
   *
   * @see node_overview_types()
   */
  public function GetList($start, $limit, $sort) {
    if ($sort) {
      $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;
    }

    $types = array();
    $count = db_result(db_query('SELECT count(*) FROM {node_type}'));
    $result = db_query('SELECT * FROM {node_type}' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($type = db_fetch_object($result)) {
      $types[] = $type;
    }
    return array(
      'total' => $count,
      'types' => $types,
    );
  }

  /**
   * Saves changes to a content type or creates a new one.
   *
   * @see node_type_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'node_type_form';
    if ($_POST['old_type']) {
      $type = node_get_types('type', $_POST['old_type']);
    }
    $form = extadmin_get_form($form_id, $_POST, $type);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Deletes content type.
   */
  public function Delete($type) {
    $form_id = 'node_type_delete_confirm';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
    );

    $form = extadmin_get_form($form_id, $data, node_get_types('type', $type));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Generates the node type editing form.
   *
   * @see node_type_form()
   */
  public function GetForm($type) {
    $form_id = 'node_type_form';
    return extadmin_get_form_items($form_id, array(), node_get_types('type', $type));
  }
}
