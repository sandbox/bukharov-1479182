/**
 * Controller for content types list.
 *
 * @see ExtAdmin.node.view.TypeList
 */
Ext.define('ExtAdmin.node.controller.Types', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'node_overview_types',
  formSelector: 'node_type_form',

  listButtons: ['#edit', '#delete'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { op: Drupal.t('Save content type') });
        }
      }
    };
  },

  getButtonState: function(button, selected) {
    return this.callParent(arguments) && selected[0] && !selected[0].get('locked');
  },

  add: function() {
    this.getMain().setView(
      'node-type',
      { xtype: 'node_type_form' },
      Drupal.t('Add content type')
    );
  },

  edit: function(record) {
    this.getMain().setView(
      'type-' + record.get('type'),
      { xtype: 'node_type_form', args: [ record.get('type') ] },
      record.get('name')
    );
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the content type {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('name')),
      function() {
        ExtAdmin.NodeTypeService.Delete({ type: record.get('type')});
        this.refreshList();
      }
    );
  }
});
