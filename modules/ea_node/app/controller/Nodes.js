/**
 * Controller for content list.
 *
 * @see ExtAdmin.node.view.List
 */
Ext.define('ExtAdmin.node.controller.Nodes', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'node_admin_content',
  formSelector: 'node_add',

  listButtons: ['#edit', '#delete', '#update-options'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#create menuitem': {
        click: this.create
      },
      '#update-options menuitem': {
        click: function(item) {
          this.performOperation(item.id.replace('node-operation-', ''), this.getList().getSelectionModel().getSelection());
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: function() {
          this.performOperation('delete', this.getList().getSelectionModel().getSelection());
        }
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: this.save
      },
      '#preview': {
        click: this.preview
      }
    };
  },

  initList: function() {
    ExtAdmin.NodeService.GetListOptions(function(result) {
      var menu = this.getListItem('#create').menu;
      Ext.each(result.types, function(type) {
        menu.add({
          id: 'node-type-' + type.type,
          text: type.name
        });
      });
      menu = this.getListItem('#update-options').menu;
      Ext.Object.each(result.operations, function(key, value) {
        menu.add({
          id: 'node-operation-' + key,
          text: value
        });
      });
    }, this);
  },

  create: function(item) {
    var type = item.id.replace('node-type-', '');
    this.getMain().setView(
      'type-' + type,
      { xtype: 'node_add', args: [ type ] },
      item.text
    );
  },

  edit: function(record) {
    this.getMain().setView(
      'nid' + record.get('nid'),
      { xtype: 'node_add', args: [ record.get('type'), record.get('nid') ] },
      record.get('title')
    );
  },

  save: function(button) {
    this.submitForm(button, {
      success: function(form, action) {
        this.getMain().setView(
          action.result.nid,
          { xtype: 'iframe', args: ['/node/' + action.result.nid] },
          action.result.title
        );
      }
    });
  },

  preview: function(button) {
    var form = button.up(this.formSelector).getForm();
    if(!form.isValid()) {
      return;
    }

    var node = form.getValues();
    var view = this.getMain().setView(
      node.nid ? node.nid : 'new',
      { xtype: 'iframe' },
      node.title ? node.title : Drupal.t('Preview')
    );

    form.doAction('standardsubmit', {
      url: '/extadmin/preview',
      target: view.id
    });
  },

  performOperation: function(operation, selection, callback, scope) {
    var doOperation = function(operation) {
      var nids = [];
      Ext.each(selection, function(record) {
        nids.push(record.get('nid'));
      });

      ExtAdmin.NodeService.PerformOperation({ operation: operation, nids: nids});
      this.refreshList();
      if(Ext.isFunction(callback)) {
        callback.call(scope, operation);
      }
    }
    if(operation != 'delete') {
      doOperation.call(this, operation);
    }
    else {
      this.confirm(
        selection.length == 1 ?
          Ext.String.format(Drupal.t('Are you sure you want to delete {0}?') + ' ' + Drupal.t('This action cannot be undone.'), selection[0].get('title')) :
          Drupal.t('Are you sure you want to delete these items?') + ' ' + Drupal.t('This action cannot be undone.'),
        function() {
          doOperation.call(this, operation);
        }
      );
    }
  }
});
