/**
 * Controller for Post settings.
 *
 * @see ExtAdmin.node.view.Settings
 */
Ext.define('ExtAdmin.node.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    node_configure: {
      modal: true
    }
  },

  controlExtra: function() {
    return {
      'node_configure #rebuild-permissions': {
        click: function(button) {
          this.confirm(
            Drupal.t('Are you sure you want to rebuild the permissions on site content?'),
            function() {
              button.up('window').close();
              this.rebuildPermissions();
            }
          );
        }
      }
    };
  },

  rebuildPermissions: function() {
    ExtAdmin.NodeService.RebuildPermissions(function(result) {
      if(result.batch) {
        Ext.create('ExtAdmin.view.BatchWindow', {
          batch: result.batch
        });
      }
    });
  }
});
