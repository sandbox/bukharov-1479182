Ext.define('ExtAdmin.node.view.Type', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.node_type_form',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.NodeTypeService.GetForm,
      directParams: {
        type: config.args ? config.args[0] : null
      },
      api: {
        submit: ExtAdmin.NodeTypeService.Update
      },
      paramsAsHash: true
    })]);
  }
});
