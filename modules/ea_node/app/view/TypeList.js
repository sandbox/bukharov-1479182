Ext.define('ExtAdmin.node.view.TypeList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.node_overview_types',

  columns: [{
    header: Drupal.t('Name'),
    dataIndex: 'name',
    width: 200
  }, {
    header: Drupal.t('Type'),
    dataIndex: 'type'
  }, {
    header: Drupal.t('Description'),
    dataIndex: 'description',
    flex: 1
  }],

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.NodeTypeService.GetList,
        reader: {
          type: 'json',
          idProperty: 'type',
          root: 'types'
        }
      },
      remoteSort: true,
      fields: [
        { name: 'type', type: 'string' },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'custom', type: 'boolean' },
        { name: 'locked', type: 'boolean' }
      ]
    });

    Ext.apply(this, {
      store: store,
      tbar: [{
        itemId: 'add',
        text: Drupal.t('Add content type'),
        iconCls: 'ea-icon-content-type'
      }, '-', {
        itemId: 'edit',
        text: Drupal.t('Edit'),
        iconCls: 'ea-icon-edit',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }],
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying types {0} - {1} of {2}')
      }
    });

    this.callParent(arguments);
  }
});
