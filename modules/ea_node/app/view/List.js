Ext.define('ExtAdmin.node.view.List', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.node_admin_content',
  mixins: {
    extension: 'ExtAdmin.view.Extension'
  },

  multiSelect: true,

  initComponent: function() {
    var columns = [{
      header: Drupal.t('ID'),
      dataIndex: 'nid',
      width: 50
    }, {
      header: Drupal.t('Title'),
      dataIndex: 'title',
      flex: 1
    }, {
      header: Drupal.t('Type'),
      dataIndex: 'type__name'
    }, {
      header: Drupal.t('Author'),
      dataIndex: 'uid__name'
    }, {
      header: Drupal.t('Published'),
      dataIndex: 'status',
      xtype: 'checkcolumn'
    }, {
      header: Drupal.t('Created'),
      dataIndex: 'created',
      xtype: 'datecolumn'
    }, {
      header: Drupal.t('Changed'),
      dataIndex: 'changed',
      xtype: 'datecolumn'
    }, {
      header: Drupal.t('Comment'),
      dataIndex: 'comment',
      xtype: 'checkcolumn',
      hidden: true
    }, {
      header: Drupal.t('Promote'),
      dataIndex: 'promote',
      xtype: 'checkcolumn',
      hidden: true
    }, {
      header: Drupal.t('Moderate'),
      dataIndex: 'moderate',
      xtype: 'checkcolumn',
      hidden: true
    }, {
      header: Drupal.t('Sticky'),
      dataIndex: 'sticky',
      xtype: 'checkcolumn',
      hidden: true
    }];

    if(Drupal.settings.node_multilanguage) {
      columns = Ext.Array.insert(columns, 5, [{
        header: Drupal.t('Language'),
        dataIndex: 'language',
        renderer: function(v) {
          return v ? v : Drupal.t('Language neutral');
        }
      }]);
    }

    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.NodeService.GetList,
        reader: {
          type: 'json',
          idProperty: 'nid',
          root: 'nodes'
        }
      },
      remoteSort: true,
      fields: [
        { name: 'nid', type: 'int' },
        { name: 'vid', type: 'int' },
        { name: 'type', type: 'string' },
        { name: 'type__name', type: 'string' },
        { name: 'title', type: 'string' },
        { name: 'uid__name', type: 'string' },
        { name: 'language', type: 'string' },
        { name: 'status', type: 'int'},
        { name: 'created', type: 'date', dateFormat: 'timestamp' },
        { name: 'changed', type: 'date', dateFormat: 'timestamp' },
        { name: 'comment', type: 'boolean' },
        { name: 'promote', type: 'boolean' },
        { name: 'moderate', type: 'boolean' },
        { name: 'sticky', type: 'boolean' }
      ]
    });

    Ext.apply(this, {
      columns: columns,
      store: store,
      tbar: [{
        itemId: 'create',
        text: Drupal.t('Create content'),
        iconCls: 'ea-icon-create-content',
        menu: {
          items: []
        }
      }, '-', {
        itemId: 'edit',
        text: Drupal.t('Edit'),
        iconCls: 'ea-icon-edit',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }, '->', {
        itemId: 'update-options',
        text: Drupal.t('Update options'),
        iconCls: 'ea-icon-update-options',
        disabled: true,
        menu: {
          items: []
        }
      }],
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying items {0} - {1} of {2}')
      }
    });

    this.mixins.extension.initComponent(this);
    this.callParent(arguments);
  }
});
