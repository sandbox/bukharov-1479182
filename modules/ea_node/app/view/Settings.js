/**
 * View extension to open Post settings window.
 */
Ext.override(ExtAdmin.view.Main, {
  node_configure_view: function(id, config, title) {
    Ext.create('ExtAdmin.node.view.Settings', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.node.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.node_configure',

  modal: true,
  autoShow: true,
  height: 260,
  width: 300,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'node_configure',
      args: []
    },
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        // This window is only 300 pixels wide, but default dupalcombo width is 400.
        // We remove width attribute to use default combobox width.
        delete defaults.width;
      }
      return defaults;
    }
  },

  tbar: [{
    itemId: 'rebuild-permissions',
    iconCls: 'ea-icon-rebuild-permissions',
    text: Drupal.t('Rebuild permissions')
  }],

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]
});
