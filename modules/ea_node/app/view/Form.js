Ext.define('ExtAdmin.node.view.Form', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.node_add',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }, {
    itemId: 'preview',
    text: Drupal.t('Preview'),
    iconCls: 'ea-icon-preview'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.args[1] ? config.title : Ext.String.format(Drupal.t('Create {0}'), config.title),
      autoLoad: true,
      directFn: ExtAdmin.NodeService.GetForm,
      directParams: {
        type: config.args[0],
        nid: config.args[1] || 0,
        params: config.args[2] ? Ext.encode(config.args[2]) : ''
      },
      api: {
        submit: ExtAdmin.NodeService.Update
      },
      paramsAsHash: true
    })]);
  }
});
