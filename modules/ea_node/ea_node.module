<?php

/**
 * @file
 * Integrates Node module with ExtAdmin.
 */

/**
 * Implements hook_menu().
 */
function ea_node_menu() {
  return array(
    'extadmin/preview' => array(
      'page callback' => 'ea_node_preview',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implements extadmin_init().
 */
function ea_node_extadmin_init() {
  if (!user_access('administer nodes') && !user_access('administer content types')) {
    return;
  }

  $path = drupal_get_path('module', 'ea_node');

  drupal_add_js($path . '/app.js');
  if (user_access('administer nodes')) {
    drupal_add_js(array(
      'node_multilanguage' => module_exists('locale') || db_result(db_query("SELECT COUNT(*) FROM {node} n WHERE language != ''")),
    ), 'setting');

    drupal_add_js($path . '/app/view/Form.js');
    drupal_add_js($path . '/app/view/List.js');
    drupal_add_js($path . '/app/view/Settings.js');
  }
  if (user_access('administer content types')) {
    drupal_add_js($path . '/app/view/Type.js');
    drupal_add_js($path . '/app/view/TypeList.js');
  }
  drupal_add_js($path . '/app/controller/Nodes.js');
  drupal_add_js($path . '/app/controller/Types.js');
  drupal_add_js($path . '/app/controller/Settings.js');

  drupal_add_css($path . '/css/Icons.css');
}

/**
 * Implements extadmin_schema().
 */
function ea_node_extadmin_schema() {
  $services = array();

  if (user_access('administer nodes')) {
    module_load_include('php', 'ea_node', 'services/NodeService.class');
    $services[] = new NodeService();
  }
  if (user_access('administer content types')) {
    module_load_include('php', 'ea_node', 'services/NodeTypeService.class');
    $services[] = new NodeTypeService();
  }

  return $services;
}

/**
 * Implements extadmin_item_config.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.RadioGroup
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.CheckboxGroup
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Date
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.Number
 */
function ea_node_extadmin_item_config($item_id, $item, $form) {
  $result = array();
  $id = implode('.', $item_id);
  switch ($id) {
    case 'node_form.body_field.body':
      // Split body from teaser.
      $result = extadmin_textarea_config($item_id, $item, $form);
      $delimiter = strpos($result['value'], '<!--break-->');
      if ($delimiter !== FALSE) {
        $result['value'] = trim(substr($result['value'], $delimiter + 12));
      }
      break;

    case 'node_form.body_field.teaser_js':
      // Split teaser from body.
      $result = extadmin_textarea_config($item_id, $item, $form);
      $result['disabled'] = FALSE;
      $result['fieldLabel'] = t('Teaser');
      $body = $form['body_field']['body']['#value'];
      $delimiter = strpos($body, '<!--break-->');
      if ($delimiter !== FALSE) {
        $result['value'] = trim(substr($body, 0, $delimiter));
      }
      break;

    case 'node_form.body_field.format':
      // By default format selector is displayed
      // as several radio boxes inside a field set.
      // We change it into RadioGroup.
      $result = extadmin_radios_config($item_id, $item, $form);
      foreach (_extadmin_get_form_items($item_id, $item, $form) as $child) {
        $result['items'][] = array(
          'boxLabel' => $child['boxLabel'],
          'checked' => $child['checked'],
          'inputValue' => $child['inputValue'],
          'name' => $child['name'],
        );
      }
      break;

    case 'node_form.options':
      // Change publishing options from fieldset to CheckboxGroup.
      $result = extadmin_fieldset_config($item_id, $item, $form);
      $items = array();
      foreach ($result['items'] as $child) {
        $items[] = array(
          'boxLabel' => $child['boxLabel'],
          'checked' => $child['checked'],
          'inputValue' => $child['inputValue'],
          'name' => $child['name'],
        );
      }
      $result['items'] = array(
        'xtype' => 'checkboxgroup',
        'columns' => 1,
        'items' => $items,
      );
      break;

    case 'node_form.author.date':
      // Change textfield input to datefield.
      $result = extadmin_textfield_config($item_id, $item, $form);
      $result['xtype'] = 'datefield';
      $result['format'] = 'Y-m-d H:i:s O';
      break;

    case 'node_type_form.submission.min_word_count':
    case 'node_configure.default_nodes_main':
      // Turn min_word_count and default_nodes_main into numberfield.
      $result = extadmin_default_config($item_id, $item, $form);
      $result['xtype'] = 'numberfield';
      break;
  }
  return $result;
}

/**
 * Generate node preview.
 */
function ea_node_preview() {
  include_once drupal_get_path('module', 'node') . '/node.pages.inc';
  return node_preview((object) $_POST);
}
