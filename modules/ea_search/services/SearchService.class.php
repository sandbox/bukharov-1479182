<?php

/**
 * @file
 * SearchService class definition.
 */

include_once drupal_get_path('module', 'search') . '/search.admin.inc';

/**
 * Search management service.
 */
class SearchService extends ServiceBase {

  /**
   * Wipe the search index.
   *
   * @see search_wipe()
   */
  public function Reindex() {
    search_wipe();
  }
}
