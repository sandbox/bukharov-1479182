/**
 * Controller for search settings window.
 *
 * @see ExtAdmin.search.view.Settings
 */
Ext.define('ExtAdmin.search.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    search_admin_settings: {
      modal: false
    }
  },

  controlExtra: function() {
    return {
      'search_admin_settings #reindex': {
        click: function(button) {
          this.confirm(
            Drupal.t('Are you sure you want to re-index the site?') + ' ' + Drupal.t('This action cannot be undone.'),
            function() {
              ExtAdmin.SearchService.Reindex();
            }
          );
        }
      }
    };
  }
});
