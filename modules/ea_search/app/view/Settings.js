Ext.define('ExtAdmin.search.view.Settings', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.search_admin_settings',

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }, '-', {
    itemId: 'reindex',
    iconCls: 'ea-icon-reindex',
    text: Drupal.t('Re-index site')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directParams: {
        form_id: 'search_admin_settings',
        args: []
      }
    })]);
  }
});
