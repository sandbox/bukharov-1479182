/**
 * Controller for updates tree.
 *
 * @see ExtAdmin.update.view.Tree
 */
Ext.define('ExtAdmin.update.controller.Updates', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'update_status',

  listButtons: ['#link', '#notes', '#download'],

  controlList: function() {
    return {
      '#check-updates': {
        click: this.checkUpdates
      },
      '#link': {
        click: function() {
          var record = this.getList().getSelectionModel().getSelection()[0];
          if(!record.get('link')) {
            record = record.parentNode;
          }

          this.openPage(record.get('id') + '-link', record.get('link'), record.get('name'));
        }
      },
      '#notes': {
        click: function() {
          var record = this.getList().getSelectionModel().getSelection()[0];
          this.openPage(record.get('id') + '-notes', record.get('release_link'), 'Release notes');
        }
      },
      '#download': {
        click: function() {
          var record = this.getList().getSelectionModel().getSelection()[0];
          window.location.href = record.get('download_link');
        }
      },
      '#settings': {
        click: function() {
          Ext.create('ExtAdmin.update.view.Settings');
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'update_settings #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'update_settings',
            op: Drupal.t('Save configuration'),
            modal: true
          });
        }
      },
      'update_settings #reset': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'update_settings',
            op: Drupal.t('Reset to defaults'),
            modal: true
          });
        }
      }
    };
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);
    switch(button) {
      case '#link':
        return base && (selected[0].get('link') || selected[0].parentNode.get('link'));
      case '#notes':
        return base && selected[0].get('release_link');
      case '#download':
        return base && selected[0].get('download_link');
    }
    return base;
  },

  checkUpdates: function() {
    ExtAdmin.UpdateService.CheckUpdates(function(result) {
      if(!result.success) {
        Ext.MessageBox.alert(
          Drupal.t('Error'),
          Drupal.t('Unable to fetch any information about available new releases and updates.')
        );
      }
      else {
        Drupal.settings.update_last_check = parseInt((new Date()).getTime() / 1000);
        this.getList().updateLastChecked();
      }
    }, this);
    this.refreshList();
  },

  openPage: function(id, url, title) {
    this.getMain().setView(
      id,
      { xtype: 'iframe', args: [ url ] },
      title
    );
  }
});
