Ext.define('ExtAdmin.update.view.Tree', {
  extend: 'Ext.tree.Panel',
  alias: 'widget.update_status',

  rootVisible: false,

  messages: [
    Drupal.t('Security update required!'),
    Drupal.t('Revoked!'),
    Drupal.t('Not supported!'),
    Drupal.t('Update available'),
    Drupal.t('Up to date')
  ],

  icons: [
    'ea-grid-alert',
    'ea-grid-alert',
    'ea-grid-alert',
    'ea-grid-warning',
    'ea-grid-ok'
  ],

  columns: [{
    xtype: 'treecolumn',
    header: Drupal.t('Name'),
    dataIndex: 'name',
    flex: 1
  }, {
    width: 30,
    menuDisabled: true,
    dataIndex: 'status',
    renderer: function(v, meta, record) {
      if(v > 0) {
        meta.tdAttr += ' data-qtip="' + this.messages[v - 1] + '"';
        meta.tdCls += ' ' + this.icons[v - 1];
      }
      else if (v < 0){
        meta.tdAttr += ' data-qtip="' + record.get('reason') + '"';
        meta.tdCls += ' ea-grid-error';
      }
      return '&nbsp;'
    }
  }, {
    header: Drupal.t('Version'),
    dataIndex: 'version'
  }, {
    xtype: 'datecolumn',
    header: Drupal.t('Date'),
    dataIndex: 'date'
  }],

  store: {
    root: {
      expanded: false
    },
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.UpdateService.GetTree,
      api: { destroy: Ext.emptyFn },
      reader: {
        type: 'json'
      }
    },
    fields: [
      { name: 'name', type: 'string' },
      { name: 'status', type: 'int' },
      { name: 'link', type: 'string' },
      { name: 'download_link', type: 'string' },
      { name: 'release_link', type: 'string' },
      { name: 'version', type: 'string' },
      { name: 'reason', type: 'string' },
      { name: 'date', type: 'date', dateFormat: 'timestamp' }
    ]
  },
  tbar: [{
    itemId: 'last-update',
    xtype: 'tbtext',
    text: Drupal.t('N/A')
  }, {
    itemId: 'check-updates',
    text: Drupal.t('Check manually'),
    iconCls: 'ea-icon-check-updates'
  }, '-', {
    itemId: 'link',
    text: Drupal.t('Project page'),
    iconCls: 'ea-icon-link',
    disabled: true
  }, {
    itemId: 'notes',
    text: Drupal.t('Release notes'),
    iconCls: 'ea-icon-notes',
    disabled: true
  }, {
    itemId: 'download',
    text: Drupal.t('Download'),
    iconCls: 'ea-icon-download',
    disabled: true
  }, '->', {
    itemId: 'settings',
    text: Drupal.t('Settings'),
    iconCls: 'ea-icon-settings'
  }],

  initComponent: function() {
    this.callParent(arguments);
    this.updateLastChecked();

    this.updateTask = Ext.TaskManager.start({
      run: this.updateLastChecked,
      interval: 1000,
      scope: this
    });
  },

  updateLastChecked: function() {
    this.down('#last-update').setText(
      Ext.String.format(
        Drupal.t('Last checked: {0} ago'),
        Ext.Date.formatInterval(
          new Date(Drupal.settings.update_last_check * 1000)
        )
      )
    );
  },
  destroy: function() {
    Ext.TaskManager.stop(this.updateTask);
    this.callParent(arguments);
  }
});
