Ext.define('ExtAdmin.update.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.update_settings',

  title: Drupal.t('Settings'),
  modal: true,
  autoShow: true,
  height: 330,
  width: 400,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'update_settings',
      args: []
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
