<?php

/**
 * @file
 * UpdateService class definition.
 */

include_once drupal_get_path('module', 'update') . '/update.settings.inc';

/**
 * Updates management service.
 */
class UpdateService extends ServiceBase {

  /**
   * Generates a tree node representing an update.
   */
  private function getUpdateNode($title, $version) {
    return array(
      'name' => $title,
      'version' => $version['version'],
      'release_link' => $version['release_link'],
      'download_link' => $version['download_link'],
      'date' => $version['date'],
      'iconCls' => 'ea-icon-update',
      'leaf' => TRUE,
    );
  }

  /**
   * Returns a list of nodes representing available updates for given project.
   *
   * @see theme_update_report()
   */
  private function getProjectUpdates($project) {
    $updates = array();

    if (isset($project['recommended'])) {
      if ($project['status'] != UPDATE_CURRENT || $project['existing_version'] !== $project['recommended']) {
        if (empty($project['security updates'])
            || count($project['security updates']) != 1
            || $project['security updates'][0]['version'] !== $project['recommended']) {
          $updates[] = $this->getUpdateNode(
            t('Recommended version'),
            $project['releases'][$project['recommended']]
          );
        }

        if (!empty($project['security updates'])) {
          foreach ($project['security updates'] as $security_update) {
            $updates[] = $this->getUpdateNode(
              t('Security update'),
              $security_update
            );
          }
        }
      }

      if ($project['recommended'] !== $project['latest_version']) {
        $updates[] = $this->getUpdateNode(
          t('Latest version'),
          $project['releases'][$project['latest_version']]
        );
      }

      if ($project['install_type'] == 'dev'
          && $project['status'] != UPDATE_CURRENT
          && isset($project['dev_version'])
          && $project['recommended'] !== $project['dev_version']) {
        $updates[] = $this->getUpdateNode(
          t('Development version'),
          $project['releases'][$project['dev_version']]
        );
      }
    }

    if (isset($project['also'])) {
      foreach ($project['also'] as $also) {
        $updates[] = $this->getUpdateNode(
          t('Optional version'),
          $project['releases'][$also]
        );
      }
    }

    return $updates;
  }

  /**
   * Returns a list of nodes representing projects.
   */
  private function getProjectNodes($type, &$data) {
    $nodes = array();
    foreach ($data as $project) {
      if ($project['project_type'] != $type) {
        continue;
      }

      $updates = $this->getProjectUpdates($project);

      $nodes[] = array(
        'name' => $project['title'] ? $project['title'] : $project['name'],
        'status' => $project['status'],
        'link' => $project['link'],
        'install_type' => $project['install_type'],
        'version' => $project['existing_version'],
        'reason' => $project['reason'],
        'date' => $project['datestamp'],
        'leaf' => !sizeof($updates),
        'expanded' => TRUE,
        'children' => $updates,
        'iconCls' => 'ea-icon-' . $type,
      );
    }
    return $nodes;
  }

  /**
   * Returns a tree representing available updates.
   *
   * @see update_status()
   */
  public function GetTree() {
    if ($available = update_get_available(TRUE)) {
      $data = update_calculate_project_data($available);
    }
    $nodes = array();
    $project_types = array(
      'core' => t('Drupal core'),
      'module' => t('Modules'),
      'theme' => t('Themes'),
      'disabled-module' => t('Disabled modules'),
      'disabled-theme' => t('Disabled themes'),
    );
    foreach ($project_types as $key => $name) {
      $children = $this->getProjectNodes($key, $data);
      if (sizeof($children)) {
        $nodes[] = array(
          'name' => $name,
          'expanded' => TRUE,
          'children' => $children,
        );
      }
    }
    return $nodes;
  }

  /**
   * Checks for available updates.
   *
   * @see update_manual_status()
   */
  public function CheckUpdates() {
    include_once drupal_get_path('module', 'update') . '/update.fetch.inc';
    $available = _update_refresh();
    return array(
      'success' => !empty($available),
    );
  }
}
