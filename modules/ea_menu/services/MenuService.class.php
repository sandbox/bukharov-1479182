<?php

/**
 * @file
 * MenuService class definition.
 */

include_once drupal_get_path('module', 'menu') . '/menu.admin.inc';

/**
 * Menu management.
 */
class MenuService extends ServiceBase {

  /**
   * Generates a list of tree nodes representing menus.
   */
  private function getRootNodes() {
    $nodes = array();
    $result = db_query("SELECT * FROM {menu_custom} ORDER BY title");
    while ($menu = db_fetch_array($result)) {
      $nodes[] = array(
        'id' => $menu['menu_name'],
        'menu_name' => $menu['menu_name'],
        'title' => $menu['title'],
        'iconCls' => 'ea-icon-menu',
        'allowDrag' => FALSE,
        'deletable' => !in_array($menu['menu_name'], menu_list_system_menus()),
      );
    }

    return $nodes;
  }

  /**
   * Generates a list of tree nodes representing menu items.
   */
  private function getItemNodes($menu, $mlid = 0) {

    $result = db_query("
      SELECT m.type, ml.*
      FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
      WHERE ml.menu_name = '%s' AND ml.plid=%d AND hidden >= 0 AND (m.type = 4 OR m.type = 6 OR m.type = 20 OR m.type IS NULL)
      ORDER BY ml.weight
    ", $menu, $mlid);

    $nodes = array();
    while ($link = db_fetch_object($result)) {

      $nodes[] = array(
        'id' => $menu . '.' . $link->mlid,
        'mlid' => $link->mlid,
        'plid' => $link->plid,
        'weight' => $link->weight,
        'menu_name' => $link->menu_name,
        'title' => $link->link_title,
        'iconCls' => 'ea-icon-item',
        'hidden' => $link->hidden,
        'expanded' => $link->expanded,
        'customized' => $link->module == 'system' && $link->customized,
        'deletable' => $link->module == 'menu' || $link->updated,
        'leaf' => !$link->has_children,
      );
    }
    return $nodes;
  }

  /**
   * Returns a list of child nodes for specified node.
   */
  public function GetNode($node) {
    if ($node == 'root') {
      return $this->getRootNodes();
    }

    list($menu, $id) = explode('.', $node);
    if ($id) {
      return $this->getItemNodes($menu, $id);
    }

    return $this->getItemNodes($menu);
  }

  /**
   * Returns the form that handles the adding/editing of a custom menu.
   *
   * @see menu_edit_menu()
   */
  public function GetMenuForm($menu_name) {
    return extadmin_get_form_items(
      'menu_edit_menu',
      array(),
      $menu_name ? 'edit' : 'add',
      $menu_name ? menu_load($menu_name) : NULL
    );
  }

  /**
   * Creates a new menu or changes an existing one.
   *
   * @see menu_edit_menu_submit()
   */
  public function UpdateMenu($formHandler = TRUE) {
    $form_id = 'menu_edit_menu';
    $menu_name = $_POST['menu_name'];

    $form = extadmin_get_form(
      $form_id,
      array(),
      $menu_name ? 'edit' : 'add',
      $menu_name ? menu_load($menu_name) : NULL
    );
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Returns the menu link editing form.
   *
   * @see menu_edit_item()
   */
  public function GetItemForm($menu_name, $mlid) {
    return extadmin_get_form_items(
      'menu_edit_item',
      array(),
      $mlid ? 'edit' : 'add',
      $mlid ? menu_link_load($mlid) : NULL,
      $mlid ? NULL : menu_load($menu_name)
    );
  }

  /**
   * Process menu and menu item add/edit form submissions.
   *
   * @see menu_edit_item_submit()
   */
  public function UpdateItem($formHandler = TRUE) {
    $form_id = 'menu_edit_item';
    $mlid = $_POST['menu']['mlid'];
    $form = extadmin_get_form(
      $form_id,
      array(),
      $mlid ? 'edit' : 'add',
      $mlid ? menu_link_load($mlid) : NULL,
      $mlid ? NULL : menu_load($_POST['menu_name'])
    );

    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates menu_delete_menu_confirm form submit to delete a custom menu.
   *
   * @see menu_delete_menu_confirm_submit()
   */
  public function DeleteMenu($menu_name) {
    $form_id = 'menu_delete_menu_confirm';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
    );

    $form = extadmin_get_form($form_id, $data, menu_load($menu_name));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Emulates menu_item_delete_form form submit to delete a menu item.
   *
   * @see menu_item_delete_form_submit()
   */
  public function DeleteItem($mlid) {
    $form_id = 'menu_item_delete_form';
    $data = array(
      'op' => t('Confirm'),
      'confirm' => TRUE,
    );

    $form = extadmin_get_form($form_id, $data, menu_link_load($mlid));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Emulates menu_reset_item_confirm form submit
   * to reset a single modified item.
   *
   * @see menu_reset_item_confirm_submit()
   */
  public function ResetItem($mlid) {
    $form_id = 'menu_reset_item_confirm';
    $data = array(
      'op' => t('Reset'),
      'confirm' => TRUE,
    );

    $form = extadmin_get_form($form_id, $data, menu_link_load($mlid));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Saves changes to menu hierarchy.
   */
  public function UpdateTree($links) {

    foreach ($links as $link) {
      $item = menu_link_load($link->mlid);
      $item['customized'] = 1;
      $item['menu_name'] = $link->menu_name;
      $item['weight'] = $link->weight;
      $item['plid'] = $link->plid;
      menu_link_save($item);
    }

    return array(
      'success' => TRUE,
    );
  }
}
