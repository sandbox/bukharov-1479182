Ext.define('ExtAdmin.menu.view.Item', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.menu_edit_item',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.menu_name = config.args[0];
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.MenuService.GetItemForm,
      directParams: {
        menu_name: config.args[0],
        mlid: config.args[1] ? config.args[1] : null
      },
      api: {
        submit: ExtAdmin.MenuService.UpdateItem
      },
      paramsAsHash: true
    })]);
  }
});
