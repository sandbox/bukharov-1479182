Ext.define('ExtAdmin.menu.view.Window', {
  extend: 'Ext.window.Window',
  alias: 'widget.menu_edit_menu',

  modal: true,
  width: 450,
  layout: 'fit',
  resizable: false,
  autoShow: true,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  initComponent: function() {
    Ext.apply(this, {
      height: this.record ? 230 : 275,
      title: this.record ? Drupal.t('Edit menu') : Drupal.t('Add menu'),
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directFn: ExtAdmin.MenuService.GetMenuForm,
        directParams: {
          menu_name: this.record ? this.record.get('menu_name') : ''
        },
        api: {
          submit: ExtAdmin.MenuService.UpdateMenu
        }
      }
    });
    this.callParent();
  }

});
