Ext.define('ExtAdmin.menu.view.Tree', {
  extend: 'Ext.tree.Panel',
  alias: 'widget.menu_overview_page',

  rootVisible: false,

  viewConfig: {
    plugins: {
      ptype: 'treeviewdragdrop',
      appendOnly: true
    }
  },

  columns: [{
    xtype: 'treecolumn',
    header: Drupal.t('Title'),
    dataIndex: 'title',
    flex: 1
  }, {
    header: Drupal.t('Disabled'),
    xtype: 'checkcolumn',
    dataIndex: 'hidden'
  }, {
    header: Drupal.t('Expanded'),
    xtype: 'checkcolumn',
    dataIndex: 'expanded'
  }],

  store: {
    root: {
      expanded: true
    },
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.MenuService.GetNode,
      api: { destroy: Ext.emptyFn },
      reader: {
        type: 'json'
      }
    },
    remoteSort: true,
    fields: [
      { name: 'mlid', type: 'int' },
      { name: 'plid', type: 'int' },
      { name: 'weight', type: 'int' },
      { name: 'menu_name', type: 'string' },
      { name: 'title', type: 'string' },
      { name: 'hidden', type: 'boolean' },
      { name: 'expanded', type: 'boolean' },
      { name: 'customized', type: 'boolean' },
      { name: 'deletable', type: 'boolean' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add menu'),
    iconCls: 'ea-icon-menu'
  }, {
    itemId: 'add-item',
    text: Drupal.t('Add item'),
    iconCls: 'ea-icon-item',
    disabled: true
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, {
    itemId: 'reset',
    text: Drupal.t('Reset'),
    iconCls: 'ea-icon-reset',
    disabled: true
  }, '-', {
    itemId: 'up',
    text: Drupal.t('Move up'),
    iconCls: 'ea-icon-up',
    disabled: true
  }, {
    itemId: 'down',
    text: Drupal.t('Move down'),
    iconCls: 'ea-icon-down',
    disabled: true
  }, '->', {
    itemId: 'settings',
    text: Drupal.t('Settings'),
    iconCls: 'ea-icon-settings'
  }]
});
