/**
 * Controller for menu tree.
 *
 * @see ExtAdmin.menu.view.Tree
 */
Ext.define('ExtAdmin.menu.controller.Menus', {
  extend: 'ExtAdmin.controller.TreeBase',

  listSelector: 'menu_overview_page',
  formSelector: 'menu_edit_menu',

  listButtons: ['#add-item', '#edit', '#delete', '#reset', '#up', '#down'],

  controlList: function() {
    return Ext.apply({
      '#add': {
        click: function() {
          Ext.create('ExtAdmin.menu.view.Window');
        }
      },
      '#add-item': {
        click: this.addItem
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: function() {
          var record = this.getList().getSelectionModel().getSelection()[0];

          if(record.get('mlid')) {
            this.deleteItem(record);
          }
          else {
            this.doDelete(record);
          }
        }
      },
      '#reset': {
        click: this.reset
      },
      '#settings': {
        click: function() {
          Ext.create('ExtAdmin.menu.view.Settings');
        }
      }
    }, this.callParent());
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { modal: true });
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'menu_edit_item #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'menu_edit_item',
            params: {
              menu_name: button.up('menu_edit_item').menu_name
            }
          });
        }
      },
      'menu_configure #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'menu_configure',
            op: Drupal.t('Save configuration'),
            modal: true
          });
        }
      },
      'menu_configure #reset': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'menu_configure',
            op: Drupal.t('Reset to defaults'),
            modal: true
          });
        }
      }
    };
  },

  getButtonState: function(button, selected) {
    var record = selected[0];
    var base = this.callParent(arguments);

    switch(button) {
      case '#delete':
        return base && record.get('deletable');
      case '#reset':
        return base && record.get('customized');
      case '#up':
      case '#down':
        return base && record.get('mlid');
    }

    return base;
  },

  addItem: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    this.getMain().setView(
      record.get('menu_name') + '-item',
      { xtype: 'menu_edit_item', args: [ record.get('menu_name') ]},
      Drupal.t('Add item')
    );
  },

  edit: function(record) {
    var record = this.getList().getSelectionModel().getSelection()[0];

    if(record.get('mlid')) {
      this.editItem(record);
    }
    else {
      Ext.create('ExtAdmin.menu.view.Window', { record: record });
    }
  },

  editItem: function(record) {
    this.getMain().setView(
      'mlid' + record.get('mlid') + '-' + record.get('menu_name') + '-item',
      { xtype: 'menu_edit_item', args: [ record.get('menu_name'), record.get('mlid') ]},
      Drupal.t('Edit menu item')
    );
  },

  doDelete: function(record) {
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the custom menu {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.MenuService.DeleteMenu({ menu_name: record.get('menu_name') });
        this.refreshList();
      }
    );
  },

  deleteItem: function(record) {
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the custom menu item {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.MenuService.DeleteItem({ mlid: record.get('mlid') });
        this.refreshList();
      }
    );
  },

  reset: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to reset the item {0} to its default values?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.MenuService.ResetItem({ mlid: record.get('mlid') });
        this.refreshList();
      }
    );
  },

  saveChanges: function(node) {
    var updated = [];

    node.eachChild(function(child) {
      child.set('plid', node.get('mlid') ? node.get('mlid') : 0);
      child.set('menu_name', node.get('menu_name'));
      child.set('weight', node.indexOf(child));

      if(child.dirty) {
        updated.push(child.data);
        child.commit();
      }
    });

    if(updated.length) {
      ExtAdmin.MenuService.UpdateTree({ links: updated });
    }
  }
});
