Ext.define('ExtAdmin.block.view.Form', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.block_admin_configure',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.BlockService.GetForm,
      directParams: {
        module: config.args ? config.args[0] : null,
        delta: config.args ? config.args[1] : null
      },
      api: {
        submit: ExtAdmin.BlockService.Update
      },
      paramsAsHash: true
    })]);
  }
});
