Ext.define('ExtAdmin.block.view.Tree', {
  extend: 'Ext.tree.Panel',
  alias: 'widget.block_admin_display',

  rootVisible: false,
  theme: '',

  viewConfig: {
    plugins: {
      ptype: 'treeviewdragdrop',
      appendOnly: true
    }
  },

  store: {
    root: {
      expanded: false
    },
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.BlockService.GetTree,
      extraParams: { theme: '' },
      api: { destroy: Ext.emptyFn },
      reader: {
        type: 'json'
      }
    },
    fields: [
      { name: 'text', type: 'string' },
      { name: 'title', type: 'string' },
      { name: 'module', type: 'string' },
      { name: 'delta', type: 'string' },
      { name: 'region', type: 'string' },
      { name: 'weight', type: 'int' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add block'),
    iconCls: 'ea-icon-block'
  }, '-', {
    itemId: 'configure',
    text: Drupal.t('Configure'),
    iconCls: 'ea-icon-configure',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '-', {
    itemId: 'up',
    text: Drupal.t('Move up'),
    iconCls: 'ea-icon-up',
    disabled: true
  }, {
    itemId: 'down',
    text: Drupal.t('Move down'),
    iconCls: 'ea-icon-down',
    disabled: true
  }, '->', Drupal.t('Theme') + ': ', {
    itemId: 'theme',
    iconCls: 'ea-icon-theme',
    menu: {
      item: []
    }
  }]
});
