/**
 * Controller for blocks tree.
 *
 * @see ExtAdmin.block.view.Tree
 */
Ext.define('ExtAdmin.block.controller.Blocks', {
  extend: 'ExtAdmin.controller.TreeBase',

  listSelector: 'block_admin_display',
  formSelector: 'block_admin_configure',

  listButtons: ['#configure', '#delete', '#up', '#down'],

  controlList: function() {
    return Ext.apply({
      '#add': {
        click: this.add
      },
      '#configure': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#theme menuitem': {
        click: this.theme
      }
    }, this.callParent());
  },

  controlForm: function() {
    return {
      '#save': {
        click: this.save
      }
    };
  },

  getButtonState: function(button, selected) {
    var record = selected[0];
    var base = this.callParent(arguments) && record.isLeaf();

    switch(button) {
      case '#delete':
        return base && record.get('module') == 'block';
    }

    return base;
  },

  refreshList: function() {
    var list = this.getList();
    list.getStore().load({
      params: {
        theme: list.theme
      }
    });
  },

  /**
   * Populate theme selector.
   */
  initList: function() {
    ExtAdmin.BlockService.GetListOptions(function(result) {
      var button = this.getListItem('#theme');
      Ext.each(result.themes, function(theme) {
        if(theme.id == result['default']) {
          button.setText(theme.title);
        }

        button.menu.add({
          id: 'theme-' + theme.id,
            text: theme.title
        });
      });
    }, this);
  },

  add: function() {
    this.getMain().setView(
      'block',
      { xtype: 'block_admin_configure' },
      Drupal.t('Add block')
    );
  },

  edit: function(record) {
    this.getMain().setView(
      'bid' + record.get('id'),
      { xtype: 'block_admin_configure', args: [ record.get('module'), record.get('delta')] },
      record.get('text')
    );
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the block {0}?'), record.get('text')),
      function() {
        ExtAdmin.BlockService.Delete({ bid: record.get('delta') });
        this.refreshList();
      }
    );
  },

  theme: function(item) {
    var theme = item.id.replace('theme-', '');
    var button = item.parentMenu.floatParent;

    this.getList().theme = theme;
    this.refreshList();
    button.setText(item.text);
  },

  save: function(button) {
    this.submitForm(button, {
      op: Drupal.t('Save block')
    });
  },

  saveChanges: function() {
    var nodes = [];
    var updated = [];

    this.getList().getRootNode().eachChild(function(node) {
      nodes = nodes.concat(node.childNodes);
    });

    Ext.each(nodes, function(node, index) {
      node.set('region', node.parentNode.get('id'));
      node.set('weight', index);

      if(node.dirty) {
        updated.push(node.data);
        node.commit();
      }
    });

    if(updated.length) {
      ExtAdmin.BlockService.UpdateTree({ blocks: updated, theme: ''});
    }
  }
});
