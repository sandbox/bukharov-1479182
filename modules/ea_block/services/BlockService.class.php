<?php

/**
 * @file
 * BlockService class definition.
 */

include_once drupal_get_path('module', 'block') . '/block.admin.inc';

/**
 * Blocks management service.
 */
class BlockService extends ServiceBase {

  /**
   * Generates a list of tree nodes representing blocks for a certain region.
   */
  private function getNodes($blocks) {
    $nodes = array();
    foreach ($blocks as $block) {
      $nodes[] = array(
        'id' => $block['bid'],
        'text' => $block['info'],
        'iconCls' => 'ea-icon-block',
        'title' => $block['title'],
        'module' => $block['module'],
        'delta' => $block['delta'],
        'region' => $block['region'],
        'weight' => intval($block['weight']),
        'leaf' => TRUE,
      );
    }
    return $nodes;
  }

  /**
   * Returns block tree.
   *
   * Block regions are represented by root nodes
   * and blocks are represented by leaves.
   *
   * @see block_admin_display()
   */
  public function GetTree($theme) {
    global $theme_key, $custom_theme;

    $custom_theme = $theme ? $theme : variable_get('theme_default', 'garland');

    $blocks = _block_rehash();
    $regions = system_region_list($theme_key);
    $regions[-1] = t('Disabled');

    $nodes = array();
    foreach ($regions as $region => $name) {
      $nodes[] = array(
        'id' => $region,
        'text' => $name,
        'iconCls' => 'ea-icon-region',
        'expanded' => TRUE,
        'allowDrag' => FALSE,
        'children' => $this->getNodes(array_filter($blocks, create_function('$block', 'return $block["region"] == "' . $region . '";'))),
      );
    }
    return $nodes;
  }

  /**
   * Returns the block configuration form.
   *
   * @see block_admin_configure()
   */
  public function GetForm($module, $delta) {
    return extadmin_get_form_items(
      $module ? 'block_admin_configure' : 'block_add_block_form',
      array(),
      $module,
      $delta
    );
  }

  /**
   * Updates block configuration.
   *
   * @see block_admin_configure_submit()
   */
  public function Update($formHandler = TRUE) {
    $form = extadmin_get_form(
      $_POST['form_id'],
      $_POST,
      $_POST['module'],
      $_POST['delta']
    );
    extadmin_process_form($_POST['form_id'], $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates block_box_delete form submit to delete a custom block.
   *
   * @see block_box_delete_submit()
   */
  public function Delete($bid) {
    $form_id = 'block_box_delete';
    $data = array(
      'form_id' => $form_id,
      'confirm' => TRUE,
      'op' => t('Delete'),
    );

    $form = extadmin_get_form($form_id, $data, $bid);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }


  /**
   * Saves changes to block arrangement.
   */
  public function UpdateTree($blocks, $theme) {
    $theme = $theme ? $theme : variable_get('theme_default', 'garland');
    foreach ($blocks as $block) {
      $status = $block->region != BLOCK_REGION_NONE;
      db_query(
        "UPDATE {blocks} SET status = %d, weight = %d, region = '%s' WHERE module = '%s' AND delta = '%s' AND theme = '%s'",
        $status, $block->weight, $status ? $block->region : '', $block->module, $block->delta, $theme
      );
    }
    cache_clear_all();
  }

  /**
   * Returns a list of active themes for blocks tree.
   */
  public function GetListOptions() {
    $themes = array();
    foreach (list_themes() as $theme) {
      if (!$theme->status && $theme->name != variable_get('admin_theme', '0')) {
        continue;
      }

      $themes[] = array(
        'id' => $theme->name,
        'title' => $theme->info['name'],
      );
    }
    return array(
      'default' => variable_get('theme_default', 'garland'),
      'themes' => $themes,
    );
  }
}
