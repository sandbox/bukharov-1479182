/**
 * Controller for themes list.
 *
 * @see ExtAdmin.system.view.ThemeList
 */
Ext.define('ExtAdmin.system.controller.Themes', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'system_themes_form',
  formSelector: 'system_theme_settings',

  listButtons: ['#toggle', '#default', '#configure'],

  controlList: function() {
    return {
      '#toggle': {
        click: this.toggle
      },
      '#default': {
        click: this.setDefault
      },
      '#configure': {
        click: this.editSelected
      },
      '#settings': {
        click: function() {
          this.edit();
        }
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, {
            op: Drupal.t('Save configuration'),
            params: {
              theme: button.up(this.formSelector).theme
            }
          });
        }
      },
      '#reset': {
        click: function(button) {
          this.submitForm(button, {
            op: Drupal.t('Reset to defaults'),
            params: {
              theme: button.up(this.formSelector).theme
            }
          });
        }
      }
    };
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);

    switch(button) {
      case '#toggle':
        if(base) {
          var item = this.getListItem(button);
          // Update toggle button based on theme status.
          if(selected[0].get('status')) {
            item.setIconCls('ea-icon-disable-theme');
            item.setText(Drupal.t('Disable theme'));
          }
          else {
            item.setIconCls('ea-icon-enable-theme');
            item.setText(Drupal.t('Enable theme'));
          }
        }
        return base && !selected[0].get('disabled');
      case '#default':
        return base && !selected[0].get('default');
      case '#configure':
        return base && selected[0].get('status');
    }

    return base;
  },

  toggle: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    ExtAdmin.ThemeService.Update({
      name: record.get('id'),
      status: !record.get('status'),
      'default': record.get('default')
    });
    this.refreshList();
  },

  setDefault: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    ExtAdmin.ThemeService.Update({
      name: record.get('id'),
      status: !record.get('status') ? true : record.get('status'),
      'default': true
    });
    this.refreshList();
  },

  edit: function(theme) {
    this.getMain().setView(
      'theme-' + (theme ? theme.get('id') + '-' : '') + 'settings',
      { xtype: 'system_theme_settings', args: [ theme ? theme.get('id') : '' ]},
      theme ? Ext.String.format(Drupal.t('Configure {0}'), theme.get('name')) : Drupal.t('Global settings')
    );
  }
});
