/**
 * Controller for system settings.
 *
 * @see ExtAdmin.system.view.AdminThemeSettings
 * @see ExtAdmin.system.view.CleanUrlSettings
 * @see ExtAdmin.system.view.DateTimeSettings
 * @see ExtAdmin.system.view.ErrorReportingSettings
 * @see ExtAdmin.system.view.FileSystemSettings
 * @see ExtAdmin.system.view.ImageToolkitSettings
 * @see ExtAdmin.system.view.PerformanceSettings
 * @see ExtAdmin.system.view.RssSettings
 * @see ExtAdmin.system.view.SiteInformation
 * @see ExtAdmin.system.view.SiteMaintenance
 */
Ext.define('ExtAdmin.system.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    system_rss_feeds_settings: {
      modal: true
    },
    system_admin_theme_settings: {
      modal: true,
      success: function() {
        this.reloadPage(Drupal.settings.clean_url)
      }
    },
    system_clean_url_settings: {
      modal: true,
      success: function(form) {
        this.reloadPage(!form.findField('clean_url').getValue());
      }
    },
    system_date_time_settings: {
      modal: false
    },
    system_error_reporting_settings: {
      modal: true
    },
    system_file_system_settings: {
      modal: true
    },
    system_image_toolkit_settings: {
      modal: true
    },
    system_performance_settings: {
      modal: false
    },
    system_site_information_settings: {
      modal: false
    },
    system_site_maintenance_settings: {
      modal: true
    }
  },

  controlExtra: function() {
    return {
      'system_performance_settings #clear-cache': {
        click: function(button) {
          var msg = Ext.MessageBox.wait(Drupal.t('Clearing cached data...'), Drupal.t('Please wait'));
          ExtAdmin.SystemService.ClearCache(function() {
            msg.close();
          });
        }
      }
    };
  },

  reloadPage: function(clean_url) {
    if(clean_url) {
      window.location.href = Drupal.settings.basePath + 'admin?' + Math.random();
    }
    else {
      window.location.href = Drupal.settings.basePath + 'index.php?q=admin&' + Math.random();
    }
  }
});
