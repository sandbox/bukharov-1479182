/**
 * Controller for modules list.
 *
 * @see ExtAdmin.system.controller.ModuleList
 */
Ext.define('ExtAdmin.system.controller.Modules', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'system_modules',

  listButtons: ['#toggle', '#uninstall'],

  controlList: function() {
    return {
      '#toggle': {
        click: this.toggle
      },
      '#uninstall': {
        click: this.uninstall
      }
    };
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);

    switch(button) {
      case '#toggle':
        if(base) {
          var item = this.getListItem(button);
          // Update toggle button based on module status.
          if(selected[0].get('status')) {
            item.setIconCls('ea-icon-disable-module');
            item.setText(Drupal.t('Disable module'));
          }
          else {
            item.setIconCls('ea-icon-enable-module');
            item.setText(Drupal.t('Enable module'));
          }
        }
        return base && !selected[0].get('disabled');
      case '#uninstall':
        return base && selected[0].get('uninstall');
    }
    return base;
  },

  toggle: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    var doUpdate = function() {
      ExtAdmin.ModuleService.Update({
        name: record.get('id'),
        status: !record.get('status')
      });
      this.refreshList();
    }

    if(record.get('dependencies').length) {
      var dependencies = [];
      // Collect a list of dependencies
      Ext.each(record.get('dependencies'), function(id) {
        var dependency = this.getList().getStore().getById(id);
        if(!dependency.get('status')) {
          dependencies.push(dependency.get('name'));
        }
      }, this);

      if(dependencies.length && !record.get('status')) {
        this.confirm(
          Ext.String.format(Drupal.t('{0}<br>Would you like to continue with enabling the above?'), dependencies.join(', ')),
          doUpdate,
          Drupal.t('Some required modules must be enabled')
        );
        return;
      }
    }
    doUpdate.call(this);
  },

  uninstal: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    this.confirm(
      Drupal.t('Selected module will be completely uninstalled from your site, and all data from this module will be lost! Would you like to continue?'),
      function() {
        ExtAdmin.ModuleService.Uninstall({ name: record.get('id') });
        this.refreshList();
      },
      Drupal.t('Confirm uninstall')
    );
  }
});
