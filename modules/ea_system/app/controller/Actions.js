/**
 * Controller for actions list.
 *
 * @see ExtAdmin.system.view.ActionList
 */
Ext.define('ExtAdmin.system.controller.Actions', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'system_actions_manage',
  formSelector: 'system_actions_configure',

  listButtons: ['#edit', '#delete'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#create menuitem': {
        click: function(item) {
          Ext.create('ExtAdmin.system.view.Action', { action: item.id.replace('action-', '') });
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { modal: true });
        }
      }
    };
  },

  getButtonState: function(button, selected) {
    return this.callParent(arguments) && !selected[0].get('locked');
  },

  /**
   * Populate Create action menu.
   */
  initList: function() {
    ExtAdmin.ActionService.GetListOptions(function(actions) {
      if(!actions.length) {
        return;
      }

      var button = this.getListItem('#create');
      button.enable();
      Ext.each(actions, function(action) {
        button.menu.add({
          id: 'action-' + action.id,
          text: action.description
        });
      });
    }, this);
  },

  edit: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    Ext.create('ExtAdmin.system.view.Action', { action: record.get('aid') });
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the action {0}?') + ' ' + Drupal.t('This cannot be undone.'), record.get('description')),
      function() {
        ExtAdmin.ActionService.Delete({ aid: record.get('aid') });
        this.refreshList();
      }
    );
  }
});
