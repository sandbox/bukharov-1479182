Ext.define('ExtAdmin.system.view.SiteInformation', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.system_site_information_settings',

  defaults: function(item) {
    var defaults = this.callParent(arguments);

    return Ext.apply(defaults, {
      labelAlign: 'top'
    });
  },

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directParams: {
        form_id: 'system_site_information_settings',
        args: []
      }
    })]);
  }
});
