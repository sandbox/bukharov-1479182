Ext.define('ExtAdmin.system.view.ModuleList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.system_modules',

  initComponent: function() {
    if(!this.rowBodyTpl) {
      // Template for displaying module details.
      ExtAdmin.system.view.ModuleList.prototype.rowBodyTpl = new Ext.XTemplate(
        '<table>',
        '<tpl if="dependenciesData.length">',
          '<tr>',
            '<th>' + Drupal.t('Depends on') + ':</th>',
            '<td>',
              '<tpl for="dependenciesData">',
                '<span class="ea-module-status-{[ values.status ? "enabled" : "disabled"]}">{name}</span>',
              '</tpl>',
            '</td>',
          '</tr>',
        '</tpl>',
        '<tpl if="dependentsData.length">',
          '<tr>',
            '<th>' + Drupal.t('Required by') + ':</th>',
            '<td>',
              '<tpl for="dependentsData">',
                '<span class="ea-module-status-{[ values.status ? "enabled" : "disabled"]}">{name}</span>',
              '</tpl>',
            '</td>',
          '</tr>',
        '</tpl>',
        '</table>', {
          compiled: true
        }
      );
    }

    var columns = [{
      header: Drupal.t('Name'),
      dataIndex: 'name',
      flex: 1
    }, {
      xtype: 'checkcolumn',
      header: Drupal.t('Enabled'),
      dataIndex: 'status'
    }, {
      header: Drupal.t('Version'),
      dataIndex: 'version'
    }, {
      header: Drupal.t('Package'),
      dataIndex: 'package'
    }, {
      header: Drupal.t('Description'),
      dataIndex: 'description',
      flex: 2
    }];


    if(Drupal.settings.throttle_enabled) {
      columns = Ext.Array.insert(columns, 2, [{
        xtype: 'checkcolumn',
        header: Drupal.t('Throttle'),
        dataIndex: 'throttle'
      }]);
    }

    Ext.apply(this, {
      columns: columns,
      features: [{
        ftype: 'rowbody',
        getAdditionalData: function(data, idx, record, orig) {
          var headerCt = this.view.headerCt,
            colspan  = headerCt.getColumnCount();

          data.dependenciesData = [];
          Ext.each(data.dependencies, function(id) {
            data.dependenciesData.push(this.view.getStore().getById(id).data);
          }, this);

          data.dependentsData = [];
          Ext.each(data.dependents, function(id) {
            data.dependentsData.push(this.view.getStore().getById(id).data);
          }, this);

          return {
            rowBody: this.view.up('gridpanel').rowBodyTpl.apply(data),
            rowBodyCls: this.rowBodyCls,
            rowBodyColspan: colspan
          };
        }
      }, {
        ftype: 'rowwrap'
      }, {
        ftype: 'grouping',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true
      }],
      store: {
        autoLoad: true,
        proxy: {
          type: 'direct',
          directFn: ExtAdmin.ModuleService.GetList,
          reader: {
            type: 'json',
            idProperty: 'id'
          }
        },
        groupField: 'package',
        fields: [
          { name: 'id', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'version', type: 'string' },
          { name: 'package', type: 'string' },
          { name: 'description', type: 'string' },
          { name: 'status', type: 'boolean' },
          { name: 'disabled', type: 'boolean' },
          { name: 'throttle', type: 'boolean' },
          { name: 'uninstall', type: 'boolean' },
          { name: 'dependencies' },
          { name: 'dependents' }
        ]
      },
      tbar: [{
        itemId: 'toggle',
        text: Drupal.t('Enable module'),
        iconCls: 'ea-icon-enable-module',
        disabled: true
      }, '-', {
        itemId: 'uninstall',
        text: Drupal.t('Uninstall'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }]
    });

    this.callParent(arguments);
  }
});
