Ext.override(ExtAdmin.view.Main, {
  system_site_maintenance_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.system.view.SiteMaintenanceWindow', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.system.view.SiteMaintenanceWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_site_maintenance_settings',

  modal: true,
  autoShow: true,
  height: 260,
  width: 480,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'system_site_maintenance_settings',
      args: []
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
