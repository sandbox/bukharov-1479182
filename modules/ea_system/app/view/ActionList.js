Ext.define('ExtAdmin.system.view.ActionList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.system_actions_manage',

  columns: [{
    header: Drupal.t('Action type'),
    dataIndex: 'type',
    width: 150
  }, {
    header: Drupal.t('Description'),
    dataIndex: 'description',
    flex: 1
  }],

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      remoteSort: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.ActionService.GetList,
        reader: {
          type: 'json',
          idProperty: 'aid',
          root: 'actions'
        }
      },
      fields: [
        { name: 'aid', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'locked', type: 'boolean' }
      ]
    });

    Ext.apply(this, {
      store: store,
      tbar: [{
        itemId: 'create',
        text: Drupal.t('Create action'),
        iconCls: 'ea-icon-action',
        menu: {
          items: []
        },
        disabled: true
      }, '-', {
        itemId: 'edit',
        text: Drupal.t('Edit'),
        iconCls: 'ea-icon-edit',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }],
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying actions {0} - {1} of {2}')
      }
    });

    this.callParent(arguments);
  }
});
