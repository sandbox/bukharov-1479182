Ext.override(ExtAdmin.view.Main, {
  system_admin_theme_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.system.view.AdminThemeSettingsWindow', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.system.view.AdminThemeSettingsWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_admin_theme_settings',

  modal: true,
  autoShow: true,
  height: 150,
  width: 300,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'system_admin_theme_settings',
      args: []
    },
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        delete defaults.width;
      }
      return defaults;
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
