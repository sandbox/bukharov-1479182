Ext.define('ExtAdmin.system.view.ThemeSettings', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.system_theme_settings',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save configuration'),
    iconCls: 'ea-icon-ok'
  }, {
    itemId: 'reset',
    text: Drupal.t('Reset to defaults'),
    iconCls: 'ea-icon-reset'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.theme = config.args ? config.args[0] : null;
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.ThemeService.GetSettingsForm,
      directParams: {
        theme: this.theme
      },
      api: {
        submit: ExtAdmin.ThemeService.UpdateSettings
      },
      paramsAsHash: true
    })]);
  }
});
