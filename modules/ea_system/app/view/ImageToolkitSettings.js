Ext.override(ExtAdmin.view.Main, {
  system_image_toolkit_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.system.view.ImageToolkitSettingsWindow', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.system.view.ImageToolkitSettingsWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_image_toolkit_settings',

  modal: true,
  autoShow: true,
  autoHeight: true,
  width: 270,
  layout: 'fit',
  resizable: false,


  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directParams: {
          form_id: 'system_image_toolkit_settings',
          args: []
        },
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  }
});
