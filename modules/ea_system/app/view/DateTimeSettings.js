Ext.define('ExtAdmin.system.view.DateTimeSettings', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.system_date_time_settings',

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directParams: {
        form_id: 'system_date_time_settings',
        args: []
      }
    })]);
  }
});
