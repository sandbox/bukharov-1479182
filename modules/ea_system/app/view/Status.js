Ext.define('ExtAdmin.system.view.Status', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.system_status',

  columns: [{
    width: 30,
    menuDisabled: true,
    draggable: false,
    dataIndex: 'severity',
    renderer: function(v, meta) {
      if(v == 1) {
        meta.tdCls += ' ea-grid-warning';
      }
      if(v == 2) {
        meta.tdCls += ' ea-grid-alert';
      }

      return '&nbsp';
    }
  }, {
    header: Drupal.t('Feature'),
    dataIndex: 'title',
    flex: 1
  }, {
    header: Drupal.t('Status'),
    dataIndex: 'value',
    flex: 2
  }],

  initComponent: function() {
    Ext.apply(this, {
      features: [{
        ftype: 'rowbody',
        getAdditionalData: function(data, idx, record, orig) {
          var headerCt = this.view.headerCt,
            colspan  = headerCt.getColumnCount();

          return {
            rowBody: data.description,
            rowBodyCls: this.rowBodyCls,
            rowBodyColspan: colspan
          };
        }
      }, {
        ftype: 'rowwrap'
      }],
      store: {
        autoLoad: true,
        proxy: {
          type: 'direct',
          directFn: ExtAdmin.SystemService.GetStatus,
          reader: {
            type: 'json'
          }
        },
        fields: [
          { name: 'title', type: 'string' },
          { name: 'value', type: 'string' },
          { name: 'severity', type: 'int' },
          { name: 'description', type: 'string' }
        ]
      }
    });

    this.callParent(arguments);
  }
});
