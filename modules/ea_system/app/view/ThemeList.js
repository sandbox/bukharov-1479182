Ext.define('ExtAdmin.system.view.ThemeList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.system_themes_form',

  columns: [{
    header: Drupal.t('Screenshot'),
    dataIndex: 'screenshot',
    width: 170
  }, {
    header: Drupal.t('Name'),
    dataIndex: 'name',
    width: 150
  }, {
    header: Drupal.t('Version'),
    dataIndex: 'version',
    width: 60
  }, {
    xtype: 'checkcolumn',
    header: Drupal.t('Enabled'),
    dataIndex: 'status',
    width: 60
  }, {
    xtype: 'checkcolumn',
    header: Drupal.t('Default'),
    dataIndex: 'default',
    width: 60
  }, {
    header: Drupal.t('Description'),
    dataIndex: 'description',
    flex: 1
  }],

  initComponent: function() {
    Ext.apply(this, {
      store: {
        autoLoad: true,
        proxy: {
          type: 'direct',
          directFn: ExtAdmin.ThemeService.GetList,
          reader: {
            type: 'json',
            idProperty: 'id'
          }
        },
        fields: [
          { name: 'id', type: 'string' },
          { name: 'screenshot', type: 'string' },
          { name: 'name', type: 'string' },
          { name: 'version', type: 'string' },
          { name: 'description', type: 'string' },
          { name: 'status', type: 'boolean' },
          { name: 'default', type: 'boolean' }
        ]
      },
      tbar: [{
        itemId: 'toggle',
        text: Drupal.t('Enable theme'),
        iconCls: 'ea-icon-enable-theme',
        disabled: true
      }, {
        itemId: 'default',
        text: Drupal.t('Set as default'),
        iconCls: 'ea-icon-default-theme',
        disabled: true
      }, '-', {
        itemId: 'configure',
        text: Drupal.t('Configure'),
        iconCls: 'ea-icon-configure',
        disabled: true
      }, '->', {
        itemId: 'settings',
        text: Drupal.t('Global settings'),
        iconCls: 'ea-icon-settings'
      }]
    });

    this.callParent(arguments);
  }
});
