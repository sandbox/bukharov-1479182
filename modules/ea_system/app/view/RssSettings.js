Ext.override(ExtAdmin.view.Main, {
  system_rss_feeds_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.system.view.RssSettingsWindow', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.system.view.RssSettingsWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_rss_feeds_settings',

  modal: true,
  autoShow: true,
  height: 170,
  width: 300,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'system_rss_feeds_settings',
      args: []
    },
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        delete defaults.width;
      }
      return defaults;
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
