Ext.define('ExtAdmin.system.view.Action', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_actions_configure',

  title: Drupal.t('Configure an advanced action'),

  modal: true,
  autoHeight: true,
  width: 500,
  layout: 'fit',
  resizable: false,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directFn: ExtAdmin.ActionService.GetForm,
        directParams: {
          action: config.action
        },
        api: {
          submit: ExtAdmin.ActionService.Update
        },
        paramsAsHash: true,
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  }
});
