Ext.override(ExtAdmin.view.Main, {
  system_file_system_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.system.view.FileSystemSettingsWindow', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.system.view.FileSystemSettingsWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_file_system_settings',

  modal: true,
  autoShow: true,
  height: 230,
  width: 480,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'system_file_system_settings',
      args: []
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
