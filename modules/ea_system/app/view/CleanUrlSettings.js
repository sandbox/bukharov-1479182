Ext.override(ExtAdmin.view.Main, {
  system_clean_url_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.system.view.CleanUrlSettingsWindow', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.system.view.CleanUrlSettingsWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.system_clean_url_settings',

  modal: true,
  autoShow: true,
  height: 140,
  width: 270,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'system_clean_url_settings',
      args: []
    },
    api: {
      submit: ExtAdmin.FormService.Process
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
