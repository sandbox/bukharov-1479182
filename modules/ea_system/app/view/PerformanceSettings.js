Ext.define('ExtAdmin.system.view.PerformanceSettings', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.system_performance_settings',

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }, '-', {
    itemId: 'clear-cache',
    iconCls: 'ea-icon-clear-cache',
    text: Drupal.t('Clear cached data')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directParams: {
        form_id: 'system_performance_settings',
        args: []
      }
    })]);
  }
});
