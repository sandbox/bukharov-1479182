<?php

/**
 * @file
 * ActionService class definition.
 */

include_once drupal_get_path('module', 'system') . '/system.admin.inc';

/**
 * Actions management service.
 */
class ActionService extends ServiceBase {

  /**
   * Returns a list of configured actions.
   *
   * @see system_actions_manage()
   */
  public function GetList($start, $limit, $sort) {
    if ($sort) {
      $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;
    }

    $actions = array();
    $count = db_result(db_query('SELECT count(*) FROM {actions}'));
    $result = db_query('SELECT * FROM {actions}' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($action = db_fetch_object($result)) {
      $actions[] = array(
        'aid' => $action->aid,
        'type' => $action->type,
        'description' => $action->description,
        'locked' => !$action->parameters,
      );
    }
    return array(
      'total' => $count,
      'actions' => $actions,
    );
  }

  /**
   * Returns a list of available actions.
   *
   * @see system_actions_manage()
   */
  public function GetListOptions() {
    $result = array();
    $actions = actions_list();
    actions_synchronize($actions);
    foreach (actions_actions_map($actions) as $id => $action) {
      if (!$action['configurable']) {
        continue;
      }

      $result[] = array(
        'id' => $id,
        'description' => $action['description'],
      );
    }
    return $result;
  }

  /**
   * Creates the form for configuration of a single action.
   *
   * @see system_actions_configure()
   */
  public function GetForm($action) {
    return extadmin_get_form_items(
      'system_actions_configure',
      array(),
      $action
    );
  }

  /**
   * Process system_actions_configure form submissions.
   *
   * @see system_actions_configure_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'system_actions_configure';
    $form = extadmin_get_form(
      $form_id,
      array(),
      $_POST['actions_aid'] ? $_POST['actions_aid'] : $_POST['actions_action']
    );
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulate system_actions_delete form submission.
   *
   * @see system_actions_delete_form_submit()
   */
  public function Delete($aid) {
    $form_id = 'system_actions_delete_form';
    $data = array(
      'op' => 'Delete',
      'form_id' => $form_id,
      'confirm' => TRUE,
    );
    $form = extadmin_get_form($form_id, $data, actions_load($aid));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }
}
