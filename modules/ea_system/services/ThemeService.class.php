<?php

/**
 * @file
 * ThemeService class definition.
 */

/**
 * Themes management service.
 */
class ThemeService extends ServiceBase {

  /**
   * Returns a listing of all themes.
   *
   * @see system_themes_form()
   */
  public function GetList() {
    $form = extadmin_get_form('system_themes_form');
    $themes = array();
    foreach ($form as $id => $value) {
      if (!is_array($value) || !$value['info']) {
        continue;
      }

      $themes[] = array(
        'id' => $id,
        'screenshot' => str_replace(t('no screenshot'), '', $value['screenshot']['#value']),
        'name' => $value['info']['#value']['name'],
        'description' => $value['info']['#value']['description'],
        'version' => $value['info']['#value']['version'],
        'status' => in_array($id, $form['status']['#default_value']),
        'default' => $form['theme_default']['#default_value'] == $id,
      );
    }
    return $themes;
  }

  /**
   * Updates theme status/default flag.
   *
   * @see system_themes_form_submit()
   */
  public function Update($name, $status, $default) {
    $form_id = 'system_themes_form';
    $data = array(
      'status' => array(),
      'op' => t('Save configuration'),
    );
    $form = extadmin_get_form($form_id);
    // Collect all enabled themes.
    foreach ($form as $id => $value) {
      if (!is_array($value) || !$value['info']) {
        continue;
      }

      if (in_array($id, $form['status']['#default_value'])) {
        $data['status'][$id] = $id;
      }
    }

    if ($status) {
      // Add theme to the enabled list.
      $data['status'][$name] = $name;
    }
    else {
      // Remove theme from the enabled list.
      unset($data['status'][$name]);
    }

    if ($default) {
      // Make this theme default.
      $data['theme_default'] = $name;
      $form['theme_default']['#default_value'] = $name;
    }

    $form['status']['#default_value'] = array_values($data['status']);
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Returns a form for theme configuration
   * for entire site and individual themes.
   *
   * @see system_theme_settings()
   */
  public function GetSettingsForm($theme) {
    $q = $_GET['q'];
    // system_theme_settings relies on arg() function
    // to determine what form to generate.
    // We fake the uri because for this function it is extadmin/api.
    $_GET['q'] = 'admin/build/themes/settings/' . $theme;
    $items = extadmin_get_form_items(
      'system_theme_settings',
      array(),
      $theme
    );
    $_GET['q'] = $q;
    return $items;
  }

  /**
   * Process system_theme_settings form submissions
   *
   * @see system_theme_settings_submit()
   */
  public function UpdateSettings($formHandler = TRUE) {
    $theme = $_POST['theme'];
    $q = $_GET['q'];
    // system_theme_settings relies on arg() function
    // to determine what form to generate.
    // We fake the uri because for this function it is extadmin/api.
    $_GET['q'] = 'admin/build/themes/settings/' . $theme;
    $form_id = 'system_theme_settings';
    $form = extadmin_get_form($form_id, array(), $theme);
    extadmin_process_form($form_id, $form, $_POST);
    $_GET['q'] = $q;
    return $this->formResult();
  }
}
