<?php

/**
 * @file
 * ModuleService class definition.
 */

include_once drupal_get_path('module', 'system') . '/system.admin.inc';

/**
 * Module management service.
 */
class ModuleService extends ServiceBase {

  /**
   * Returns a list of available modules.
   *
   * @see system_modules()
   * @see system_modules_uninstall()
   */
  public function GetList() {
    $ea_required = array(
      'ea_block',
      'ea_filter',
      'ea_node',
      'ea_system',
      'ea_user',
    );
    $form = extadmin_get_form('system_modules');
    $uninstall = extadmin_get_form('system_modules_uninstall');
    $modules = array();
    foreach ($form['validation_modules']['#value'] as $module) {
      $modules[] = array(
        'id' => $module->name,
        'status' => intval($module->status),
        'throttle' => intval($module->throttle),
        'disabled' => in_array($module->name, $form['status']['#disabled_modules']) || in_array($module->name, $ea_required),
        'uninstall' => is_array($uninstall['modules'][$module->name]),
        'name' => $module->info['name'],
        'package' => $module->info['package'],
        'description' => $module->info['description'],
        'version' => $module->info['version'],
        'dependencies' => $module->info['dependencies'],
        'dependents' => array_values($module->info['dependents']),
      );
    }
    return $modules;
  }

  /**
   * Updates module status.
   *
   * @see system_modules_submit()
   */
  public function Update($name, $status) {
    $data = array(
      'status' => array(),
      'op' => t('Save configuration'),
    );
    $form_id = 'system_modules';
    $form = extadmin_get_form($form_id);

    // Collect current module statuses and dependencies.
    foreach ($form['validation_modules']['#value'] as $module) {
      if ($module->status) {
        $data['status'][$module->name] = $module->name;
      }

      if ($module->name == $name) {
        $dependencies = $module->info['dependencies'];
      }
    }

    if ($status) {
      // Add module to enabled modules and enable all its dependencies.
      $data['status'][$name] = $name;
      if ($dependencies) {
        foreach ($dependencies as $module) {
          $data['status'][$module] = $module;
        }
      }
    }
    else {
      // Remove module from enabled modules.
      unset($data['status'][$name]);
    }

    $form['status']['#default_value'] = array_values($data['status']);
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Emulates submitted uninstall form.
   *
   * @see system_modules_uninstall_submit()
   */
  public function Uninstall($name) {
    $form_id = 'system_modules_uninstall';
    $data = array(
      'uninstall' => array(),
      'confirm' => TRUE,
      'op' => t('Uninstall'),
      'form_id' => $form_id,
    );
    $data['uninstall'][$name] = TRUE;

    $form = extadmin_get_form($form_id, array('#storage' => $data));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }
}
