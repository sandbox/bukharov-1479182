<?php

/**
 * @file
 * SystemService class definition.
 */

include_once drupal_get_path('module', 'system') . '/system.admin.inc';

/**
 * System management service.
 */
class SystemService extends ServiceBase {

  /**
   * Generates status report.
   *
   * @see system_status()
   */
  public function GetStatus() {
    include_once './includes/install.inc';
    drupal_load_updates();

    $requirements = module_invoke_all('requirements', 'runtime');
    usort($requirements, '_system_sort_requirements');
    return array_map(create_function('$v', '$v["value"] = strip_tags($v["value"]); $v["description"] = strip_tags($v["description"]); return $v;'), $requirements);
  }

  /**
   * Clear system caches.
   *
   * @see drupal_flush_all_caches()
   */
  public function ClearCache() {
    drupal_flush_all_caches();
  }
}
