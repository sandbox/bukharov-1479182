ExtAdmin.Application.registerController([
  'ExtAdmin.system.controller.Actions',
  'ExtAdmin.system.controller.Modules',
  'ExtAdmin.system.controller.Settings',
  'ExtAdmin.system.controller.Themes'
]);
