/**
 * Controller for Blog API settings window.
 *
 * @see ExtAdmin.blogapi.view.Settings
 */
Ext.define('ExtAdmin.blogapi.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    blogapi_admin_settings: {
      modal: true
    }
  }
});
