/**
 * View extension to open Blog API settings window.
 */
Ext.override(ExtAdmin.view.Main, {
  blogapi_admin_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.blogapi.view.Settings', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.blogapi.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.blogapi_admin_settings',

  modal: true,
  autoHeight: true,
  width: 350,
  layout: 'fit',
  resizable: false,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directParams: {
          form_id: 'blogapi_admin_settings',
          args: []
        },
        paramsAsHash: true,
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
