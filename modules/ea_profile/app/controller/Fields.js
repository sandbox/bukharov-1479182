/**
 * Controller for profile fields list.
 *
 * @see ExtAdmin.profile.view.FieldList
 */
Ext.define('ExtAdmin.profile.controller.Fields', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'profile_admin_overview',
  formSelector: 'profile_field_form',

  listButtons: ['#edit', '#delete'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#add menuitem': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { op: Drupal.t('Save field') });
        }
      }
    };
  },

  add: function(item) {
    var type = item.id.replace('profile-', '');
    this.getMain().setView(
      'field-type-' + type,
      { xtype: 'profile_field_form', args: [ type ] },
      item.text
    );
  },

  edit: function(record) {
    this.getMain().setView(
      'fid' + record.get('fid'),
      { xtype: 'profile_field_form', args: [ record.get('type'), record.get('fid') ] },
      record.get('title')
    );
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the field {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.ProfileFieldService.Delete({ fid: record.get('fid') });
        this.refreshList();
      }
    );
  }
});
