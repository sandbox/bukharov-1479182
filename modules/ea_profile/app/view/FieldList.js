Ext.define('ExtAdmin.profile.view.FieldList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.profile_admin_overview',

  columns: [{
    header: Drupal.t('Title'),
    dataIndex: 'title',
    flex: 1
  }, {
    header: Drupal.t('Name'),
    dataIndex: 'name'
  }, {
    header: Drupal.t('Type'),
    dataIndex: 'type'
  }, {
    header: Drupal.t('Category'),
    dataIndex: 'category'
  }, {
    xtype: 'checkcolumn',
    header: Drupal.t('Required'),
    dataIndex: 'required'
  }, {
    xtype: 'checkcolumn',
    header: Drupal.t('Registration'),
    dataIndex: 'register'
  }],

  features: {
    ftype: 'grouping',
    groupHeaderTpl: '{name}',
    hideGroupedHeader: true
  },

  store: {
    autoLoad: true,
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.ProfileFieldService.GetList,
      reader: {
        type: 'json',
        idProperty: 'fid'
      }
    },
    groupField: 'category',
    fields: [
      { name: 'fid', type: 'int' },
      { name: 'title', type: 'string' },
      { name: 'name', type: 'string' },
      { name: 'category', type: 'string' },
      { name: 'type', type: 'string' },
      { name: 'required', type: 'boolean' },
      { name: 'register', type: 'boolean' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add new field'),
    iconCls: 'ea-icon-profile-field',
    menu: {
      items: [{
        id: 'profile-textfield',
        text: Drupal.t('single-line textfield')
      }, {
        id: 'profile-textarea',
        text: Drupal.t('multi-line textfield')
      }, {
        id: 'profile-checkbox',
        text: Drupal.t('checkbox')
      }, {
        id: 'profile-selection',
        text: Drupal.t('list selection')
      }, {
        id: 'profile-list',
        text: Drupal.t('freeform list')
      }, {
        id: 'profile-url',
        text: Drupal.t('URL')
      }, {
        id: 'profile-date',
        text: Drupal.t('date')
      }]
    }
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }]
});
