Ext.define('ExtAdmin.profile.view.Field', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.profile_field_form',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.args[1] ? Ext.String.format(Drupal.t('edit {0}'), config.title)  : Ext.String.format(Drupal.t('add new {0}'), config.title),
      autoLoad: true,
      directFn: ExtAdmin.ProfileFieldService.GetForm,
      directParams: {
        type: config.args[0],
        fid: config.args[1] || 0
      },
      api: {
        submit: ExtAdmin.ProfileFieldService.Update
      },
      paramsAsHash: true
    })]);
  }
});
