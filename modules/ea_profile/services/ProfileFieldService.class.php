<?php

/**
 * @file
 * ProfileFieldService class definition.
 */

include_once drupal_get_path('module', 'profile') . '/profile.admin.inc';

/**
 * Profile fields management service.
 */
class ProfileFieldService extends ServiceBase {

  /**
   * Returns a list of all editable profile fields.
   *
   * @see profile_admin_overview()
   */
  public function GetList() {
    $fields = array();
    $result = db_query('SELECT title, name, type, category, fid, required, register FROM {profile_fields} ORDER BY category, weight');
    while ($field = db_fetch_object($result)) {
      $fields[] = $field;
    }
    return $fields;
  }

  /**
   * Generates a form to add/edit a user profile field.
   *
   * @see profile_field_form()
   */
  public function GetForm($type, $fid) {
    $q = $_GET['q'];
    if ($fid) {
      // profile_field_form relies on arg() function to determine
      // what form to generate.
      // We fake the uri because for this function it is extadmin/api.
      $_GET['q'] = 'admin/user/profile/edit';
    }
    $items = extadmin_get_form_items('profile_field_form', array(), $fid ? $fid : $type);
    $_GET['q'] = $q;
    return $items;
  }

  /**
   * Process profile_field_form submissions.
   *
   * @see profile_field_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $q = $_GET['q'];
    if ($_POST['fid']) {
      // profile_field_form relies on arg() function to determine
      // what form to generate.
      // We fake the uri because for this function it is extadmin/api.
      $_GET['q'] = 'admin/user/profile/edit';
    }
    $form = extadmin_get_form($_POST['form_id'], array(), $_POST['fid'] ? $_POST['fid'] : $_POST['type']);
    extadmin_process_form($_POST['form_id'], $form, $_POST);
    $_GET['q'] = $q;
    return $this->formResult();
  }

  /**
   * Deletes a field from all user profiles.
   *
   * @see profile_field_delete()
   */
  public function Delete($fid) {
    $form_id = 'profile_field_delete';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
      'form_id' => $form_id,
    );

    $form = extadmin_get_form($form_id, $data, $fid);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }
}
