<?php

/**
 * @file
 * Integrates Poll module with ExtAdmin.
 */

/**
 * Implements extadmin_init().
 */
function ea_poll_extadmin_init() {
  if (!user_access('administer nodes')) {
    return;
  }

  $path = drupal_get_path('module', 'ea_poll');

  drupal_add_js($path . '/app/ux/PollChoices.js');
}

/**
 * Generates a list of fields for editing poll choices.
 */
function ea_poll_get_text($element) {
  $result = array();
  foreach ($element as $k => $v) {
    if (is_numeric($k) && is_array($v)) {
      array_unshift($v['chtext']['#parents'], 'node_form');
      $field = extadmin_textfield_config($v['chtext']['#parents'], $v['chtext'], array());
      unset($field['fieldLabel']);
      $result[] = $field;
    }
  }
  return $result;
}

/**
 * Generates a list of fields for editing poll vote numbers.
 */
function ea_poll_get_votes($element) {
  $result = array();
  foreach ($element as $k => $v) {
    if (is_numeric($k) && is_array($v)) {
      array_unshift($v['chvotes']['#parents'], 'node_form');
      $field = extadmin_default_config($v['chvotes']['#parents'], $v['chvotes'], array());
      unset($field['fieldLabel']);
      $field['xtype'] = 'numberfield';
      $result[] = $field;
    }
  }
  return $result;
}

/**
 * Implements extadmin_item_config().
 *
 * @see ExtAdmin.poll.ux.PollChoices
 */
function ea_poll_extadmin_item_config($item_id, $item, $form) {
  static $ignore;

  $result = array();
  $id = implode('.', $item_id);
  if (strpos($id, 'choice_wrapper') !== FALSE) {
    if ($ignore) {
      // Since our component contains all poll choices,
      // we need to process only the first one.
      $result['#extadmin_ignore'] = TRUE;
    }
    else {
      // Create one PollChoices component to include all poll choices.
      $ignore = TRUE;
      $result = array(
        'xtype' => 'pollchoices',
        'chtext' => ea_poll_get_text($form['choice_wrapper']['choice']),
        'chvotes' => ea_poll_get_votes($form['choice_wrapper']['choice']),
      );
    }
  }
  return $result;
}
