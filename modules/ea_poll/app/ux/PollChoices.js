Ext.define('ExtAdmin.poll.ux.PollChoices', {
  extend: 'Ext.form.FieldSet',
  alias: 'widget.pollchoices',

  title: Drupal.t('Poll choices'),
  padding: 5,

  initComponent: function() {
    var chtext = [{
      xtype: 'label',
      text: Drupal.t('Choice')
    }];

    var chvotes = [{
      xtype: 'label',
      text: Drupal.t('Vote count')
    }];

    Ext.apply(this, {
      items: [{
        xtype: 'container',
        layout: 'column',
        items: [{
          xtype: 'container',
          itemId: 'chtext',
          layout: 'anchor',
          columnWidth: 0.8,
          style: 'padding: 0 5px 0 0',
          defaults: {
            anchor: '100%'
          },
          items: chtext.concat(this.chtext)
        }, {
          xtype: 'container',
          itemId: 'chvotes',
          layout: 'anchor',
          columnWidth: 0.2,
          defaults: {
            anchor: '100%'
          },
          items: chvotes.concat(this.chvotes)
        }]
      }, {
        xtype: 'button',
        text: Drupal.t('Add another choice'),
        anchor: null,
        margin: { top: 5 },
        handler: this.addChoice,
        scope: this
      }, {
        xtype: 'hidden',
        itemId: 'choice_count',
        name: '#extadmin_form_state[choice_count]',
        value: this.chtext.length
      }]
    });

    this.callParent(arguments);
  },

  addChoice: function() {
    var chtext = this.down('#chtext');
    var chvotes = this.down('#chvotes');
    var index = chtext.items.getCount() - 1;

    chtext.add({
      xtype: 'textfield',
      itemId: 'choice-' + index + '-chtext',
      name: 'choice[' + index + '][chtext]',
      allowBlank: true
    });
    chvotes.add({
      xtype: 'numberfield',
      itemId: 'choice-' + index + '-chvotes',
      name: 'choice[' + index + '][chvotes]',
      allowBlank: true,
      value: 0
    });

    var form = this.up('form').getForm();
    form.findField('choice_count').setValue(index + 1);
  }
});
