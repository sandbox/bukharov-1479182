<?php

/**
 * @file
 * DblogService class definition.
 */

include_once drupal_get_path('module', 'dblog') . '/dblog.admin.inc';

/**
 * Database logs service.
 */
class DblogService extends ServiceBase {

  /**
   * Returns a list of watchdog entries.
   */
  public function GetList($start, $limit, $sort) {
    if ($sort) {
      switch ($sort[0]->property) {
        case 'uid__name':
          $order_by = 'b.name';
          break;

        default:
          $order_by = 'a.' . $sort[0]->property;
          break;
      }
      $order_by = ' ORDER BY ' . $order_by . ' ' . $sort[0]->direction;
    }

    $entries = array();
    $count = db_result(db_query('SELECT count(*) FROM {watchdog}'));
    $result = db_query('SELECT a.wid, a.uid, a.type, a.message, a.variables, a.severity, a.hostname, a.timestamp, b.name uid__name FROM {watchdog} a INNER JOIN {users} b ON (a.uid=b.uid)' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($entry = db_fetch_object($result)) {
      $entry->message = _dblog_format_message($entry);
      $entry->variables = '';
      $entries[] = $entry;
    }
    return array(
      'total' => $count,
      'entries' => $entries,
    );
  }

  /**
   * Returns a top of watchdog entries of certain type.
   */
  public function GetTop($type, $start, $limit, $sort) {
    $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;

    $entries = array();
    $count = db_result(db_query("SELECT count(distinct(message)) FROM {watchdog} WHERE type='%s'", $type));
    $result = db_query("SELECT count(wid) count, message, variables FROM {watchdog} WHERE type='%s' GROUP BY message, variables" . $order_by . ' LIMIT %d, %d', $type, $start, $limit);
    while ($entry = db_fetch_object($result)) {
      $entry->message = _dblog_format_message($entry);
      $entry->variables = '';
      $entries[] = $entry;
    }
    return array(
      'total' => $count,
      'entries' => $entries,
    );
  }
}
