/**
 * Controller for Database logging settings window.
 *
 * @see ExtAdmin.dblog.view.Settings
 */
Ext.define('ExtAdmin.dblog.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    dblog_admin_settings: {
      modal: true
    }
  }

});
