/**
 * View extension to open Database logging lists.
 */
Ext.override(ExtAdmin.view.Main, {
  dblog_top_view: function(id, config, title) {
    Ext.create('ExtAdmin.dblog.view.TopEntries', {
      title: title,
      type: config.args[0]
    });
  }
});

Ext.define('ExtAdmin.dblog.view.TopEntries', {
  extend: 'Ext.window.Window',
  alias: 'widget.dblog_top',

  modal: true,
  autoShow: true,
  height: 300,
  width: 450,
  layout: 'fit',
  resizable: false,

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.DblogService.GetTop,
        extraParams: {
          type: this.type
        },
        reader: {
          type: 'json',
          root: 'entries'
        }
      },
      remoteSort: true,
      sorters: [{
        property: 'count',
        direction: 'DESC'
      }],
      fields: [
        { name: 'count', type: 'int' },
        { name: 'message', type: 'string' }
      ]
    });

    Ext.apply(this, {
      items: {
        xtype: 'gridpanel',
        border: false,
        columns: [{
          xtype: 'numbercolumn',
          header: Drupal.t('Count'),
          dataIndex: 'count',
          format: '0',
          align: 'right'
        }, {
          header: Drupal.t('Message'),
          dataIndex: 'message',
          flex: 1
        }],
        store: store,
        bbar: {
          xtype: 'pagingtoolbar',
          store: store,
          displayInfo: true,
          displayMsg: Drupal.t('Displaying entries {0} - {1} of {2}')
        }
      }
    });

    this.callParent(arguments);
  }
});
