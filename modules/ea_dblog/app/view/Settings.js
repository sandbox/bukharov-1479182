/**
 * View extension to open Database logging settings window.
 */
Ext.override(ExtAdmin.view.Main, {
  dblog_admin_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.dblog.view.Settings', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.dblog.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.dblog_admin_settings',

  modal: true,
  autoShow: true,
  height: 125,
  width: 300,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        // This window is only 300 pixels wide, but default dupalcombo width is 400.
        // We remove width attribute to use default combobox width.
        delete defaults.width;
      }
      return defaults;
    },
    directParams: {
      form_id: 'dblog_admin_settings',
      args: []
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
