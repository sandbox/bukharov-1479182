Ext.define('ExtAdmin.dblog.view.EntryList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.dblog_overview',

  columns: [{
    dataIndex: 'severity',
    width: 30,
    menuDisabled: true,
    // Renders icons in severity column.
    renderer: function(v, meta) {
      switch(v) {
        case 0: // WATCHDOG_EMERG
        case 1: // WATCHDOG_ALERT
          meta.tdCls += ' ea-grid-alert';
          break;

        case 2: // WATCHDOG_CRITICAL
        case 3: // WATCHDOG_ERROR
          meta.tdCls += ' ea-grid-error';
          break;

        case 4: // WATCHDOG_WARNING
          meta.tdCls += ' ea-grid-warning';
          break;

        case 5: // WATCHDOG_NOTICE
        case 6: // WATCHDOG_INFO
        case 7: // WATCHDOG_DEBUG
          break;
      }
      return '&nbsp;';
    }
  }, {
    header: Drupal.t('Type'),
    dataIndex: 'type',
    width: 150
  }, {
    xtype: 'datecolumn',
    format: 'Y-m-d H:i:s',
    header: Drupal.t('Date'),
    dataIndex: 'timestamp',
    width: 150
  }, {
    header: Drupal.t('Message'),
    dataIndex: 'message',
    flex: 1
  }, {
    header: Drupal.t('User'),
    dataIndex: 'uid__name',
    renderer: function(v) {
      return v ? v : Drupal.t('Anonymous');
    }
  }, {
    header: Drupal.t('Hostname'),
    dataIndex: 'hostname',
    hidden: true
  }],

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.DblogService.GetList,
        reader: {
          type: 'json',
          idProperty: 'wid',
          root: 'entries'
        }
      },
      remoteSort: true,
      sorters: [{
        property: 'timestamp',
        direction: 'DESC'
      }],
      fields: [
        { name: 'wid', type: 'int' },
        { name: 'type', type: 'string' },
        { name: 'message', type: 'string' },
        { name: 'uid__name', type: 'string' },
        { name: 'severity', type: 'int' },
        { name: 'hostname', type: 'string' },
        { name: 'timestamp', type: 'date', dateFormat: 'timestamp' }
      ]
    });

    Ext.apply(this, {
      store: store,
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying entries {0} - {1} of {2}')
      }
    });

    this.callParent(arguments);
  }
});
