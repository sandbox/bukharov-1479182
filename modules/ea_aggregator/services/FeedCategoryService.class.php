<?php

/**
 * @file
 * FeedCategoryService class definition.
 */

include_once drupal_get_path('module', 'aggregator') . '/aggregator.admin.inc';

/**
 * Feed categories management service.
 */
class FeedCategoryService extends ServiceBase {

  /**
   * Returns a list of feed categories.
   *
   * @see aggregator_view()
   */
  public function GetList() {
    $categories = array();

    $result = db_query('SELECT * FROM {aggregator_category}');
    while ($category = db_fetch_object($result)) {
      $category->items = db_result(db_query('SELECT count(*) FROM {aggregator_category_item} WHERE cid=%d', $category->cid));
      $category->refresh += $category->checked;
      $categories[] = $category;
    }

    return $categories;
  }

  /**
   * Generates a form to add/edit/delete aggregator categories.
   *
   * @see aggregator_form_category()
   */
  public function GetForm($cid) {
    $form_id = 'aggregator_form_category';

    if ($cid) {
      return extadmin_get_form_items($form_id, array(), aggregator_category_load($cid));
    }

    return extadmin_get_form_items($form_id);
  }

  /**
   * Process aggregator_form_category form submissions.
   *
   * @see aggregator_form_category_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'aggregator_form_category';
    $form = extadmin_get_form($form_id, array(), $_POST);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates aggregator_form_category form submit to delete a feed category.
   *
   * @see aggregator_form_category_submit()
   */
  public function Delete($cid) {
    $form_id = 'aggregator_form_category';
    $data = aggregator_category_load($cid);
    $data['form_id'] = $form_id;
    $data['op'] = t('Delete');

    $form = extadmin_get_form($form_id, array(), $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }
}
