<?php

/**
 * @file
 * FeedService class definition.
 */

 include_once drupal_get_path('module', 'aggregator') . '/aggregator.admin.inc';

/**
 * Feeds management service.
 */
class FeedService extends ServiceBase {

  /**
   * Returns a list of feeds.
   *
   * @see aggregator_view()
   */
  public function GetList() {
    $feeds = array();

    $result = db_query('SELECT * FROM {aggregator_feed}');
    while ($feed = db_fetch_object($result)) {
      $feed->items = db_result(db_query('SELECT count(*) FROM {aggregator_item} WHERE fid=%d', $feed->fid));
      $feed->refresh += $feed->checked;
      $feeds[] = $feed;
    }

    return $feeds;
  }

  /**
   * Generates a form to add/edit feed sources.
   *
   * @see aggregator_form_feed()
   */
  public function GetForm($fid) {
    $form_id = 'aggregator_form_feed';

    if ($fid) {
      return extadmin_get_form_items($form_id, array(), aggregator_feed_load($fid));
    }

    return extadmin_get_form_items($form_id);
  }

  /**
   * Process aggregator_form_feed form submissions.
   *
   * @see aggregator_form_feed_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'aggregator_form_feed';
    $form = extadmin_get_form($form_id, array(), $_POST);

    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates aggregator_form_feed_submit form submit to delete a feed source.
   *
   * @see aggregator_form_feed_submit()
   */
  public function Delete($fid) {
    $form_id = 'aggregator_form_feed';
    $data = aggregator_feed_load($fid);
    $data['form_id'] = $form_id;
    $data['op'] = t('Delete');

    $form = extadmin_get_form($form_id, array(), $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Removes all items from a feed.
   */
  public function RemoveItems($fid) {
    aggregator_remove(aggregator_feed_load($fid));
  }

  /**
   * Refreshes a feed.
   */
  public function UpdateItems($fid) {
    aggregator_refresh(aggregator_feed_load($fid));
  }
}
