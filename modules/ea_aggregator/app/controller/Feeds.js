/**
 * Controller for feed list.
 *
 * @see ExtAdmin.aggregator.view.FeedList
 */
Ext.define('ExtAdmin.aggregator.controller.Feeds', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'aggregator_feeds',
  formSelector: 'aggregator_form_feed',

  listButtons: ['#edit', '#delete', '#remove-items', '#update-items'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: function() {
          Ext.create('ExtAdmin.aggregator.view.Feed');
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#remove-items': {
        click: this.removeItems
      },
      '#update-items': {
        click: this.updateItems
      },
      '#settings': {
        click: this.settings
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { modal: true });
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'aggregator_admin_settings #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'aggregator_admin_settings',
            op: Drupal.t('Save configuration'),
            modal: true
          });
        }
      },
      'aggregator_admin_settings #reset': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'aggregator_admin_settings',
            op: Drupal.t('Reset to defaults'),
            modal: true
          });
        }
      }
    };
  },

  edit: function(record) {
    Ext.create('ExtAdmin.aggregator.view.Feed', { fid: record.get('fid') });
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the feed {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.FeedService.Delete({ fid: record.get('fid') });
        this.refreshList();
      }
    );
  },

  removeItems: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to remove all items from the feed {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.FeedService.RemoveItems({ fid: record.get('fid') });
        this.refreshList();
      }
    );
  },

  updateItems: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    ExtAdmin.FeedService.UpdateItems({ fid: record.get('fid') });
    this.refreshList();
  },

  settings: function() {
    Ext.create('ExtAdmin.aggregator.view.Settings');
  }
});
