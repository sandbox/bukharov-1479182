/**
 * Controller for categories list.
 *
 * @see ExtAdmin.aggregator.view.CategoryList
 */
Ext.define('ExtAdmin.aggregator.controller.Categories', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'aggregator_categories',
  formSelector: 'aggregator_form_category',

  listButtons: ['#edit', '#delete'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: function() {
          Ext.create('ExtAdmin.aggregator.view.Category');
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { modal: true });
        }
      }
    };
  },

  edit: function(record) {
    Ext.create('ExtAdmin.aggregator.view.Category', { cid: record.get('cid') });
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the category {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('title')),
      function() {
        ExtAdmin.FeedCategoryService.Delete({ cid: record.get('cid') });
        this.refreshList();
      }
    );
  }
});
