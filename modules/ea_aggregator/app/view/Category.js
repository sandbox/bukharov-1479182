Ext.define('ExtAdmin.aggregator.view.Category', {
  extend: 'Ext.window.Window',
  alias: 'widget.aggregator_form_category',

  modal: true,
  autoShow: true,
  width: 350,
  height: 230,
  layout: 'fit',
  resizable: false,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config || {}, {
      title: config && config.cid ? Drupal.t('Edit category') : Drupal.t('Add category'),
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directFn: ExtAdmin.FeedCategoryService.GetForm,
        directParams: {
          cid: config ? config.cid : null
        },
        api: {
          submit: ExtAdmin.FeedCategoryService.Update
        },
        paramsAsHash: true
      }
    })]);
  }
});
