Ext.define('ExtAdmin.aggregator.view.Feed', {
  extend: 'Ext.window.Window',
  alias: 'widget.aggregator_form_feed',

  modal: true,
  width: 350,
  autoHeight: true,
  layout: 'fit',
  resizable: false,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config || {}, {
      title: config && config.fid ? Drupal.t('Edit feed') : Drupal.t('Add feed'),
      items: {
        xtype: 'drupalform',
        border: false,
        defaults: function(item) {
          var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
          if(item.xtype == 'drupalcombo') {
            delete defaults.width;
          }
          return defaults;
        },
        autoLoad: true,
        directFn: ExtAdmin.FeedService.GetForm,
        directParams: {
          fid: config ? config.fid : null
        },
        api: {
          submit: ExtAdmin.FeedService.Update
        },
        paramsAsHash: true,
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  }
});
