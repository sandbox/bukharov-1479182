Ext.define('ExtAdmin.aggregator.view.FeedList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.aggregator_feeds',

  columns: [{
    header: Drupal.t('Title'),
    dataIndex: 'title',
    flex: 1
  }, {
    header: Drupal.t('Items'),
    dataIndex: 'items',
    align: 'right'
  }, {
    header: Drupal.t('Last update'),
    dataIndex: 'checked',
    renderer: function(v, meta, record) {
      if(!parseInt(record.raw.checked)) {
        return Drupal.t('never');
      }

      return Ext.String.format(Drupal.t('{0} ago'), Ext.Date.formatInterval(v));
    },
    width: 150
  }, {
    header: Drupal.t('Next update'),
    dataIndex: 'refresh',
    renderer: function(v, meta, record) {
      if(!parseInt(record.raw.checked)) {
        return Drupal.t('never');
      }

      return Ext.String.format(Drupal.t('{0} left'), Ext.Date.formatDelta(v - new Date()));
    },
    width: 150
  }],

  store: {
    autoLoad: true,
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.FeedService.GetList,
      reader: {
        type: 'json',
        idProperty: 'fid'
      }
    },
    fields: [
      { name: 'fid', type: 'int' },
      { name: 'refresh', type: 'int' },
      { name: 'items', type: 'int' },
      { name: 'title', type: 'string' },
      { name: 'url', type: 'string' },
      { name: 'link', type: 'string' },
      { name: 'description', type: 'string' },
      { name: 'image', type: 'string' },
      { name: 'refresh', type: 'date', dateFormat: 'timestamp' },
      { name: 'checked', type: 'date', dateFormat: 'timestamp' },
      { name: 'modified', type: 'date', dateFormat: 'timestamp' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add feed'),
    iconCls: 'ea-icon-feed'
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '-', {
    itemId: 'remove-items',
    text: Drupal.t('Remove items'),
    iconCls: 'ea-icon-remove-feed-items',
    disabled: true
  }, {
    itemId: 'update-items',
    text: Drupal.t('Update items'),
    iconCls: 'ea-icon-update-feed-items',
    disabled: true
  }, '->', {
    itemId: 'settings',
    text: Drupal.t('Settings'),
    iconCls: 'ea-icon-settings'
  }]
});
