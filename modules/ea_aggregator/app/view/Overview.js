Ext.define('ExtAdmin.aggregator.view.Overview', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.aggregator_admin_overview',

  layout: 'border',

  items: [{
    xtype: 'aggregator_feeds',
    region: 'center',
    border: false,
    flex: 1
  }, {
    xtype: 'aggregator_categories',
    region: 'south',
    border: false,
    split: true,
    flex: 1
  }]
});
