Ext.define('ExtAdmin.aggregator.view.CategoryList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.aggregator_categories',

  title: Drupal.t('Categories'),

  columns: [{
    header: Drupal.t('Title'),
    dataIndex: 'title',
    flex: 1
  }, {
    header: Drupal.t('Items'),
    dataIndex: 'items',
    align: 'right'
  }],

  store: {
    autoLoad: true,
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.FeedCategoryService.GetList,
      reader: {
        type: 'json',
        idProperty: 'cid'
      }
    },
    fields: [
      { name: 'cid', type: 'int' },
      { name: 'items', type: 'int' },
      { name: 'title', type: 'string' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add category'),
    iconCls: 'ea-icon-feed-category'
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }]
});
