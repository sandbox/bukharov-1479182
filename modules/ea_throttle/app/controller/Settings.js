/**
 * Controller for Throttle settings window.
 */
Ext.define('ExtAdmin.throttle.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    throttle_admin_settings: {
      modal: true
    }
  }

});
