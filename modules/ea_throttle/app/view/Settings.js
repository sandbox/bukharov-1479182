/**
 * View extension to open Database logging settings window.
 */
Ext.override(ExtAdmin.view.Main, {
  throttle_admin_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.throttle.view.Settings', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.throttle.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.throttle_admin_settings',

  modal: true,
  autoShow: true,
  height: 220,
  width: 300,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'throttle_admin_settings',
      args: []
    },
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        // This window is only 300 pixels wide, but default dupalcombo width is 400.
        // We remove width attribute to use default combobox width.
        delete defaults.width;
      }
      return defaults;
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
