<?php

/**
 * @file
 * NodeTranslationService class definition.
 */

/**
 * Translation management service.
 */
class NodeTranslationService extends ServiceBase {

  /**
   * Returns content types that allow translation.
   */
  public function GetListOptions() {
    $result = array();
    foreach (node_get_types('types') as $type) {
      if (translation_supported_type($type->type)) {
        $result[] = $type->type;
      }
    }
    return $result;
  }

  /**
   * Returns a list of node's translations.
   *
   * @see translation_node_overview()
   */
  public function GetList($nid) {
    $node = node_load($nid);
    if ($node->tnid) {
      $tnid = $node->tnid;
      $translations = translation_node_get_translations($node->tnid);
    }
    else {
      $tnid = $node->nid;
      $translations = array($node->language => $node);
    }

    $result = array();
    foreach (language_list() as $language) {
      if ($translations[$language->language]) {
        $tnode = node_load($translations[$language->language]->nid);
        $result[] = array(
          'nid' => $tnode->nid,
          'title' => $tnode->title,
          'status' => $tnode->status,
          'language__code' => $language->language,
          'language__name' => $language->name,
          'source' => $tnode->nid == $tnid,
          'outdated' => $tnode->translate,
        );
      }
      else {
        $result[] = array(
          'nid' => $node->nid,
          'title' => t('n/a'),
          'status' => 2,
          'language__code' => $language->language,
          'language__name' => $language->name,
        );
      }
    }
    return $result;
  }
}
