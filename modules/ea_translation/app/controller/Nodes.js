/**
 * Controller for content list.
 *
 * @see ExtAdmin.node.view.List
 */
Ext.define('ExtAdmin.translation.controller.Nodes', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'node_admin_content',

  listButtons: ['#translate'],

  controlList: function() {
    return {
      '#translate': {
        click: this.translate
      }
    };
  },

  /**
   * Load content types available for translation.
   */
  initList: function() {
    ExtAdmin.NodeTranslationService.GetListOptions(function(result) {
      this.types = result;
    }, this);
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);

    switch(button) {
      case '#translate':
        return base && selected[0].get('language') && Ext.Array.indexOf(this.types, selected[0].get('type')) >= 0;
    }

    return base;
  },

  translate: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    Ext.create('ExtAdmin.translation.view.List', {
      nid: record.get('nid'),
      type: record.get('type'),
      type__name: record.get('type__name'),
      title: Ext.String.format(Drupal.t('Translations of {0}'), record.get('title'))
    });
  }
});
