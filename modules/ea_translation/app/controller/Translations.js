/**
 * Controller for translations list.
 *
 * @see ExtAdmin.translation.view.List
 */
Ext.define('ExtAdmin.translation.controller.Translations', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'translation_node_overview gridpanel',

  listButtons: ['#add', '#edit'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      }
    };
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);

    switch(button) {
      case '#add':
        return base && selected[0].get('status') == 2;
      case '#edit':
        return base && selected[0].get('status') != 2;
    }

    return base;
  },

  add: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    var wnd = this.getList().up('window');
    this.getMain().setView(
      'type-' + wnd.type,
      { xtype: 'node_add', args: [ wnd.type, 0, { translation: record.get('nid'), language: record.get('language__code') }]},
      wnd.type__name
    );
    wnd.close();
  },

  edit: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    var wnd = this.getList().up('window');
    this.getMain().setView(
      'nid' + record.get('nid'),
      { xtype: 'node_add', args: [ wnd.type, record.get('nid') ] },
      record.get('title')
    );
    wnd.close();
  }
});
