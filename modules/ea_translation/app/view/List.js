Ext.define('ExtAdmin.translation.view.List', {
  extend: 'Ext.window.Window',
  alias: 'widget.translation_node_overview',

  modal: true,
  autoShow: true,
  height: 225,
  width: 450,
  layout: 'fit',
  resizable: false,

  initComponent: function() {
    Ext.apply(this, {
      items: {
        xtype: 'gridpanel',
        border: false,
        tbar: [{
          itemId: 'add',
          iconCls: 'ea-icon-translation',
          text: Drupal.t('Add translation'),
          disabled: true
        }, '-', {
          itemId: 'edit',
          iconCls: 'ea-icon-edit',
          text: Drupal.t('Edit'),
          disabled: true
        }],
        columns: [{
          header: Drupal.t('Language'),
          dataIndex: 'language__name',
          renderer: function(v, meta, record) {
            return record.get('source') ? '<b>' + v + '</b>': v;
          }
        }, {
          header: Drupal.t('Title'),
          dataIndex: 'title',
          flex: 1
        }, {
          header: Drupal.t('Status'),
          dataIndex: 'status',
          renderer: function(v) {
            switch(v) {
              case 0:
                return Drupal.t('Not published');
              case 1:
                return Drupal.t('Published');
              case 2:
                return Drupal.t('Not translated');
            }
          }
        }],
        store: {
          autoLoad: true,
          proxy: {
            type: 'direct',
            extraParams: {
              nid: this.nid
            },
            directFn: ExtAdmin.NodeTranslationService.GetList,
            reader: {
              type: 'json'
            }
          },
          fields: [
            { name: 'nid', type: 'int' },
            { name: 'title', type: 'string' },
            { name: 'language__code', type: 'string' },
            { name: 'language__name', type: 'string' },
            { name: 'source', type: 'boolean' },
            { name: 'status', type: 'int' }
          ]
        }
      }
    });

    this.callParent(arguments);
  }
});
