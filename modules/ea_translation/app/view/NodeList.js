/**
 * View extension to add Translation button to Content list toolbar.
 */
ExtAdmin.view.Extension.register('node_admin_content', function() {
  Ext.Array.insert(this.tbar, 4, [{
    itemId: 'translate',
    text: Drupal.t('Translate'),
    iconCls: 'ea-icon-translate',
    disabled: true
  }]);
});
