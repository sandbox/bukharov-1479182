<?php

/**
 * @file
 * TaxonomyService class definition.
 */

include_once drupal_get_path('module', 'taxonomy') . '/taxonomy.admin.inc';

/**
 * Taxonomy management service
 */
class TaxonomyService extends ServiceBase {

  /**
   * Generates a list of tree nodes representing vocabularies.
   */
  private function getRootNodes() {
    $nodes = array();
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $vid => $v) {
      $nodes[] = array(
        'id' => 'vocabulary.' . $v->vid,
        'vid' => $v->vid,
        'name' => $v->name,
        'types' => implode(
          ', ',
          array_map(
            create_function('$type', 'return node_get_types("name", $type);'),
            $v->nodes
          )
        ),
        'tags' => $v->tags,
        'multiple' => $v->multiple,
        'required' => $v->required,
        'allowDrag' => FALSE,
        'iconCls' => 'ea-icon-vocabulary',
      );
    }
    return $nodes;
  }

  /**
   * Checks if term has child terms.
   */
  private function checkForChildren($terms, $tid) {
    foreach ($terms as $t) {
      if (in_array($tid, $t->parents)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Generates a list of tree nodes representing terms.
   */
  private function getTermNodes($tid, $vid = 0) {
    if ($tid) {
      $term = taxonomy_get_term($tid);
    }
    $vocabulary = taxonomy_vocabulary_load($vid ? $vid : $term->vid);

    $nodes = array();
    if (!$vocabulary->tags) {
      $terms = taxonomy_get_tree($vocabulary->vid, $tid, -1, 2);
      foreach ($terms as $t) {
        if (!in_array($tid, $t->parents)) {
          continue;
        }

        $nodes[] = array(
          'id' => 'term.' . $tid . '.' . $t->tid,
          'tid' => $t->tid,
          'ptid' => $tid,
          'vid' => $t->vid,
          'parents' => array_map('intval', $t->parents),
          'name' => $t->name,
          'iconCls' => 'ea-icon-term',
          'leaf' => !$this->checkForChildren($terms, $t->tid),
        );
      }
    }
    return $nodes;
  }

  /**
   * Returns a list of child nodes for specified node.
   */
  public function GetNode($node) {
    if ($node == 'root') {
      return $this->getRootNodes();
    }

    list($type, $id, $tid) = explode('.', $node);
    if ($type == 'vocabulary') {
      return $this->getTermNodes(0, $id);
    }

    if ($type == 'term') {
      return $this->getTermNodes($tid);
    }

    return array();
  }

  /**
   * Accept the form submission for a vocabulary and save the results.
   *
   * @see taxonomy_form_vocabulary_submit()
   */
  public function UpdateVocabulary($formHandler = TRUE) {
    $form_id = 'taxonomy_form_vocabulary';
    $form = extadmin_get_form($form_id, array(), $_POST);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Returns form for adding and editing vocabularies.
   *
   * @see taxonomy_form_vocabulary()
   */
  public function GetVocabularyForm($vid) {
    $form_id = 'taxonomy_form_vocabulary';
    if ($vid) {
      $vocabulary = taxonomy_vocabulary_load($vid);
    }
    return extadmin_get_form_items($form_id, array(), (array) $vocabulary);
  }

  /**
   * Deletes vocabulary.
   *
   * @see taxonomy_vocabulary_confirm_delete_submit()
   */
  public function DeleteVocabulary($vid) {
    $data = array(
      'values' => (array) taxonomy_vocabulary_load($vid),
    );
    taxonomy_vocabulary_confirm_delete_submit(array(), $data);
    return $this->formResult();
  }

  /**
   * Insert or update a term.
   *
   * taxonomy_form_term_submit()
   */
  public function UpdateTerm($formHandler = TRUE) {
    $form_id = 'taxonomy_form_term';
    $form = extadmin_get_form($form_id, array(), taxonomy_vocabulary_load($_POST['vid']), $_POST);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Returns term edit form.
   *
   * @see taxonomy_form_term()
   */
  public function GetTermForm($vid, $tid) {
    $form_id = 'taxonomy_form_term';
    if ($tid) {
      $term = taxonomy_get_term($tid);
    }
    return extadmin_get_form_items($form_id, array(), taxonomy_vocabulary_load($vid), (array) $term);
  }

  /**
   * Deletes term.
   *
   * @see taxonomy_term_confirm_delete_submit()
   */
  public function DeleteTerm($tid) {
    $term = taxonomy_get_term($tid);
    $data = array(
      'values' => (array) $term,
    );
    taxonomy_term_confirm_delete_submit(
      array(
        '#vocabulary' => (array) taxonomy_vocabulary_load($term->vid),
      ),
      $data
    );
    return $this->formResult();
  }

  /**
   * Saves changes to taxonomy hierarchy.
   */
  public function UpdateTree($terms) {
    foreach ($terms as $t) {
      $term = (array) taxonomy_get_term($t->tid);
      $term['vid'] = $t->vid;
      $term['parent'] = $t->parents;
      $term['weight'] = $t->weight;
      taxonomy_save_term($term);
    }
  }
}
