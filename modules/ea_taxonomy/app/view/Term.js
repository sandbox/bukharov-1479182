Ext.define('ExtAdmin.taxonomy.view.Term', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.taxonomy_form_term',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      autoLoad: true,
      directFn: ExtAdmin.TaxonomyService.GetTermForm,
      directParams: {
        vid: config.args[0],
        tid: config.args[1] ? config.args[1] : null
      },
      api: {
        submit: ExtAdmin.TaxonomyService.UpdateTerm
      },
      paramsAsHash: true
    })]);
  }

});
