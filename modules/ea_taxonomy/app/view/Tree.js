Ext.define('ExtAdmin.taxonomy.view.Tree', {
  extend: 'Ext.tree.Panel',
  alias: 'widget.taxonomy_overview_vocabularies',

  rootVisible: false,

  viewConfig: {
    plugins: {
      ptype: 'treeviewdragdrop',
      appendOnly: true
    }
  },

  columns: [{
    xtype: 'treecolumn',
    header: Drupal.t('Name'),
    dataIndex: 'name',
    flex: 1
  }, {
    header: Drupal.t('Types'),
    dataIndex: 'types',
    flex: 1
  }, {
    header: Drupal.t('Tags'),
    xtype: 'checkcolumn',
    dataIndex: 'tags'
  }, {
    header: Drupal.t('Multiple select'),
    xtype: 'checkcolumn',
    dataIndex: 'multiple'
  }, {
    header: Drupal.t('Required'),
    xtype: 'checkcolumn',
    dataIndex: 'required'
  }],

  store: {
    root: {
      expanded: true
    },
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.TaxonomyService.GetNode,
      api: { destroy: Ext.emptyFn },
      reader: {
        type: 'json'
      }
    },
    remoteSort: true,
    fields: [
      { name: 'vid', type: 'int' },
      { name: 'tid', type: 'int' },
      { name: 'ptid', type: 'int' },
      { name: 'name', type: 'string' },
      { name: 'types', type: 'string' },
      { name: 'required', type: 'boolean' },
      { name: 'multiple', type: 'boolean' },
      { name: 'tags', type: 'boolean' },
      { name: 'parents' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add vocabulary'),
    iconCls: 'ea-icon-vocabulary'
  }, {
    itemId: 'add-term',
    text: Drupal.t('Add term'),
    iconCls: 'ea-icon-term',
    disabled: true
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '-', {
    itemId: 'up',
    text: Drupal.t('Move up'),
    iconCls: 'ea-icon-up',
    disabled: true
  }, {
    itemId: 'down',
    text: Drupal.t('Move down'),
    iconCls: 'ea-icon-down',
    disabled: true
  }]

});
