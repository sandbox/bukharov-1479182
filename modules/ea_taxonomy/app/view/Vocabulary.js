Ext.define('ExtAdmin.taxonomy.view.Vocabulary', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.taxonomy_form_vocabulary',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.TaxonomyService.GetVocabularyForm,
      directParams: {
        vid: config.args ? config.args[0] : null
      },
      api: {
        submit: ExtAdmin.TaxonomyService.UpdateVocabulary
      },
      paramsAsHash: true
    })]);
  }
});
