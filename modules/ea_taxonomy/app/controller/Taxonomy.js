/**
 * Controller for taxonomy tree.
 *
 * @see ExtAdmin.taxonomy.view.Tree
 */
Ext.define('ExtAdmin.taxonomy.controller.Taxonomy', {
  extend: 'ExtAdmin.controller.TreeBase',

  listSelector: 'taxonomy_overview_vocabularies',
  formSelector: 'taxonomy_form_vocabulary',

  listButtons: ['#add-term', '#edit', '#delete', '#up', '#down'],

  controlList: function() {
    return Ext.apply({
      '#add': {
        click: this.add
      },
      '#add-term': {
        click: this.addTerm
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: function() {
          var record = this.getList().getSelectionModel().getSelection()[0];

          if(record.get('tid')) {
            this.deleteTerm(record);
          }
          else {
            this.doDelete(record);
          }
        }
      }
    }, this.callParent());
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button);
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'taxonomy_form_term #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'taxonomy_form_term'
          });
        }
      }
    };
  },

  add: function() {
    this.getMain().setView(
      'vocabulary',
      { xtype: 'taxonomy_form_vocabulary' },
      Drupal.t('Add vocabulary')
    );
  },

  addTerm: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.getMain().setView(
      'vid' + record.get('vid') + '-term',
      { xtype: 'taxonomy_form_term', args: [ record.get('vid') ] },
      Drupal.t('Add term')
    );
  },

  edit: function(record) {
    if(record.get('tid')) {
      this.editTerm(record);
    }
    else {
      this.editMenu(record);
    }
  },

  editMenu: function(record) {
    this.getMain().setView(
      'vid' + record.get('vid'),
      { xtype: 'taxonomy_form_vocabulary', args: [ record.get('vid') ] },
      Drupal.t('Edit vocabulary')
    );
  },

  editTerm: function(record) {
    this.getMain().setView(
      'tid' + record.get('tid'),
      { xtype: 'taxonomy_form_term', args: [ record.get('vid'), record.get('tid') ] },
      Drupal.t('Edit term')
    );
  },

  doDelete: function(record) {
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the vocabulary {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('name')),
      function() {
        ExtAdmin.TaxonomyService.DeleteVocabulary({ vid: record.get('vid') });
        this.refreshList();
      }
    );
  },

  deleteTerm: function(record) {
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the term {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('name')),
      function() {
        ExtAdmin.TaxonomyService.DeleteTerm({ tid: record.get('tid') });
        this.refreshList();
      }
    );
  },

  saveChanges: function(node) {
    var updated = [];

    node.eachChild(function(child) {
      child.set('ptid', node.get('tid') ? node.get('tid') : 0);
      child.set('vid', node.get('vid'));
      child.set('weight', node.indexOf(child));

      if(child.dirty) {
        if(child.isModified('ptid')) {
          var index = child.data.parents.indexOf(child.modified.ptid);
          if(index >= 0) {
            child.data.parents[index] = child.get('ptid');
          }
          else {
            child.data.parents.push(child.get('ptid'));
          }
        }
        updated.push(child.data);
        child.commit();
      }
    });

    if(updated.length) {
      ExtAdmin.TaxonomyService.UpdateTree({ terms: updated });
    }
  }
});
