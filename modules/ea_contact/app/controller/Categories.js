/**
 * Controller for contact categories list.
 *
 * @see ExtAdmin.contact.view.List
 */
Ext.define('ExtAdmin.contact.controller.Categories', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'contact_admin_categories',
  formSelector: 'contact_admin_edit',

  listButtons: ['#edit', '#delete'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#settings': {
        click: function() {
          Ext.create('ExtAdmin.contact.view.Settings');
        }
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button);
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'contact_admin_settings #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'contact_admin_settings',
            op: Drupal.t('Save configuration'),
            modal: true
          });
        }
      },
      'contact_admin_settings #reset': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'contact_admin_settings',
            op: Drupal.t('Reset to defaults'),
            modal: true
          });
        }
      }
    };
  },

  add: function() {
    this.getMain().setView(
      'contact-category',
      { xtype: 'contact_admin_edit' },
      Drupal.t('Add category')
    );
  },

  edit: function(record) {
    this.getMain().setView(
      'cid' + record.get('cid'),
      { xtype: 'contact_admin_edit', args: [ record.get('cid') ] },
      Drupal.t('Edit contact category')
    );
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('category')),
      function() {
        ExtAdmin.ContactCategoryService.Delete({ cid: record.get('cid') });
        this.refreshList();
      }
    );
  }
});
