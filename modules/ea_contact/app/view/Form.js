Ext.define('ExtAdmin.contact.view.Form', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.contact_admin_edit',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      autoLoad: true,
      directFn: ExtAdmin.ContactCategoryService.GetForm,
      directParams: {
        cid: config.args ? config.args[0] : null
      },
      api: {
        submit: ExtAdmin.ContactCategoryService.Update
      },
      paramsAsHash: true
    })]);
  }

});
