Ext.define('ExtAdmin.contact.view.List', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.contact_admin_categories',

  columns: [{
    header: Drupal.t('Category'),
    dataIndex: 'category',
    flex: 1
  }, {
    header: Drupal.t('Recipients'),
    dataIndex: 'recipients',
    flex: 1
  }, {
    header: Drupal.t('Selected'),
    dataIndex: 'selected',
    xtype: 'checkcolumn'
  }],

  store: {
    autoLoad: true,
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.ContactCategoryService.GetList,
      reader: {
        type: 'json',
        idProperty: 'cid'
      }
    },
    fields: [
      { name: 'cid', type: 'int' },
      { name: 'recipients', type: 'string' },
      { name: 'category', type: 'string' },
      { name: 'selected', type: 'boolean' }
    ]
  },

  tbar: [{
    itemId: 'add',
    text: Drupal.t('Add category'),
    iconCls: 'ea-icon-contact-category'
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '->', {
    itemId: 'settings',
    text: Drupal.t('Settings'),
    iconCls: 'ea-icon-settings'
  }]
});
