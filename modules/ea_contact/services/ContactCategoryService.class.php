<?php

/**
 * @file
 * ContactCategoryService class definition.
 */

include_once drupal_get_path('module', 'contact') . '/contact.admin.inc';

/**
 * Contact categories management service.
 */
class ContactCategoryService extends ServiceBase {

  /**
   * Return a list of configured contact categories.
   *
   * @see contact_admin_categories()
   */
  public function GetList() {
    $result = db_query('SELECT cid, category, recipients, selected FROM {contact} ORDER BY weight, category');
    $categories = array();
    while ($category = db_fetch_object($result)) {
      $categories[] = $category;
    }
    return $categories;
  }

  /**
   * Generates category edit form.
   *
   * @see contact_admin_edit()
   */
  public function GetForm($cid) {
    $form_id = 'contact_admin_edit';
    if (!$cid) {
      return extadmin_get_form_items($form_id, array(), 'add');
    }

    return extadmin_get_form_items($form_id, array(), 'edit', contact_load($cid));
  }

  /**
   * Process the contact category edit form submission.
   *
   * @see contact_admin_edit_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'contact_admin_edit';
    if ($_POST['cid']) {
      $form = extadmin_get_form($form_id, array(), 'edit', contact_load($_POST['cid']));
    }
    else {
      $form = extadmin_get_form($form_id, array(), 'add');
    }
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates contact_admin_delete form submit to delete a contact category.
   *
   * @see contact_admin_delete_submit()
   */
  public function Delete($cid) {
    $form_id = 'contact_admin_delete';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
    );

    $form = extadmin_get_form($form_id, $data, contact_load($cid));
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }
}
