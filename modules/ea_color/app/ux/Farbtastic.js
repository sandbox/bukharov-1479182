/**
 * Farbtastik color picker implementation for ExtJS.
 */
Ext.define('ExtAdmin.color.ux.Farbtastic', {
  extend: 'Ext.form.field.Picker',
  alias: 'widget.farbtastic',

  isFormField: false,
  allowBlank: false,
  width: 200,
  maxLength: 7,
  minLength: 4,

  initComponent: function() {
    this.picker = Ext.create('Ext.Component', {
      ownerCt: this.ownerCt,
      renderTo: Ext.getBody(),
      floating: true,
      hidden: true,
      minHeight: 200,
      style: {
        backgroundColor: '#FFF'
      }
    });
    this.farbtastic = $.farbtastic('#' + this.picker.id);
    this.farbtastic.linkTo(Ext.Function.bind(this.syncValue, this));

    this.callParent(arguments);
  },

  setValue: function(value) {
    this.callParent(arguments);
    if(value) {
      this.farbtastic.setColor(value);
    }
  },

  syncValue: function(value) {
    this.value = value;
    this.setRawValue(value);
    this.validateValue(value);
    this.fireEvent('select', this, value);
  },

  getErrors: function(value) {
    var errors = this.callParent(arguments);

    if(!errors.length && value.charAt(0) != '#') {
      errors.push(Drupal.t('Value must start with #'));
    }

    if(!errors.length && value.length != 4 && value.length != 7) {
      errors.push(Drupal.t('Color value must be 3 or 6 characters long'));
    }

    if(!errors.length) {
      var re = new RegExp('^[0-9a-f]{3,6}$', 'i')
      if(!re.test(value.substr(1))) {
        errors.push(Drupal.t('Color value must be in hex'));
      }
    }

    return errors;
  }

});
