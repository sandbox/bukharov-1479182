/**
 * Farbtastik color picker with color display.
 */
Ext.define('ExtAdmin.color.ux.ColorPicker', {
  extend: 'Ext.form.FieldContainer',
  alias: 'widget.farbtasticpicker',

  mixins: {
      field: 'Ext.form.field.Field'
  },

  layout: 'hbox',
  height: 22,

  initComponent: function() {
    Ext.apply(this, {
      items: [{
        xtype: 'component',
        itemId: 'color',
        width: 20,
        height: 22
      }, {
        xtype: 'splitter'
      }, {
        xtype: 'farbtastic',
        itemId: 'picker',
        listeners: {
          select: function(farb, value) {
            this.updateColor(value);
            this.mixins.field.setValue.call(this, value);
          },
          scope: this
        }
      }]
    });

    this.callParent(arguments);
    this.initField();
  },

  setValue: function(value) {
    this.mixins.field.setValue.call(this, value);

    this.down('#picker').setValue(value);
    this.updateColor(value);

    return this;
  },

  updateColor: function(value) {
    if(this.rendered) {
      this.down('#color').getEl().setStyle('background-color', value || this.value);
    }
  },

  afterRender: function() {
    this.callParent(arguments);
    this.updateColor();
  }
});
