/**
 * Color scheme control.
 */
Ext.define('ExtAdmin.color.ux.Palette', {
  extend: 'Ext.container.Container',
  alias: 'widget.color_scheme_form',

  cssLoaded: true,

  initComponent: function() {
    this.loadCss();

    Ext.apply(this, {
      items: [{
        xtype: 'fieldset',
        title: Drupal.t('Color scheme'),
        collapsible: false,
        anchor: '100%',
        defaults: function(item) {
          if(item.xtype == 'drupalcombo') {
            return {
              width: 400
            };
          }
        },
        items: this.items
      }, {
        xtype: 'fieldset',
        title: Drupal.t('Preview'),
        collapsible: false,
        anchor: '100%',
        html: '<div id="preview" style="position: relative;"><div id="text"><h2>Lorem ipsum dolor</h2><p>Sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud <a href="#">exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div><div id="img" style="background-image: url(' + this.path + '/' + this.info.preview_image + ');"></div><div id="gradient"></div></div>'
      }]
    });
    this.callParent(arguments);

    this.down('#color-scheme').on('select', this.changeScheme, this);
  },

  loadCss: function() {
    if(!this.info.preview_css) {
      return;
    }

    this.cssLoaded = false;
    Ext.Ajax.request({
      url: this.path + '/' + this.info.preview_css,
      success: function(response) {
        Ext.util.CSS.createStyleSheet(response.responseText, this.id + '-css');
        this.cssLoaded = true;
        if(this.rendered) {
          this.updatePreview();
        }
      },
      scope: this
    });
  },

  afterRender: function() {
    this.callParent(arguments);
    this.previewEl = Ext.get('preview');
    this.textEl = Ext.get('text');
    this.gradientEl = Ext.get('gradient');

    this.updatePreview();
  },

  changeScheme: function(combo, records, opts) {
    var colors = records[0].get('id').split(',');
    if(colors.length < 5) {
      return;
    }

    Ext.each(colors, function(color, index) {
      this.getComponent(0).getComponent(index + 1).setValue(color);
    }, this);
    this.updatePreview();
  },

  updatePreview: function() {
    if(!this.cssLoaded) {
      return;
    }

    this.previewEl.setStyle('backgroundColor', this.down('#color-palette-base').getValue());
    this.textEl.setStyle('color', this.down('#color-palette-text').getValue());
    this.textEl.down('a').setStyle('color', this.down('#color-palette-link').getValue());
    this.textEl.down('h2').setStyle('color', this.down('#color-palette-link').getValue());

    var top = Ext.draw.Color.fromString(this.down('#color-palette-top').getValue());
    var bottom = Ext.draw.Color.fromString(this.down('#color-palette-bottom').getValue());
    if(!top || !bottom) {
      return;
    }

    var html = '';
    var height = this.gradientEl.getHeight() / 10;
    var delta = {
      r: (bottom.r - top.r) / height,
      g: (bottom.g - top.g) / height,
      b: (bottom.b - top.b) / height
    };
    for(var i=0; i<height; ++i) {
      top.r += delta.r;
      top.g += delta.g;
      top.b += delta.b;
      html += '<div class="gradient-line" style="background-color: ' + top.toString() + ';"></div>';
    }
    this.gradientEl.update(html);
  },

  destroy: function() {
    Ext.util.CSS.removeStyleSheet(this.id + '-css');
    this.callParent(arguments);
  }
});
