<?php

/**
 * @file
 * Integrates Color module with ExtAdmin.
 */

/**
 * Implements extadmin_init().
 */
function ea_color_extadmin_init() {
  $path = drupal_get_path('module', 'ea_color');

  drupal_add_js('misc/farbtastic/farbtastic.js');
  drupal_add_js($path . '/app/ux/Palette.js');
  drupal_add_js($path . '/app/ux/Farbtastic.js');
  drupal_add_js($path . '/app/ux/ColorPicker.js');

  drupal_add_css('misc/farbtastic/farbtastic.css', 'module', 'all', FALSE);
}


/**
 * Implements extadmin_item_config().
 *
 * @see ExtAdmin.color.ux.Palette
 * @see ExtAdmin.color.ux.ColorPicker
 * @see ExtAdmin.color.ux.Farbtastic
 */
function ea_color_extadmin_item_config($item_id, $item, $form) {
  $result = array();
  $id = implode('.', $item_id);
  switch ($id) {
    case 'system_theme_settings.color.scheme':
      // Fix #default_value bug when its not set to color scheme.
      $result = extadmin_select_config($item_id, $item, $form);
      $keys = array_keys($form['color']['scheme']['#options']);
      if (!in_array($result['value'], $keys)) {
        $result['value'] = $keys[0];
      }
      break;

    case 'system_theme_settings.color':
      // Switch from regular select to color scheme selector.
      $result = extadmin_fieldset_config($item_id, $item, $form);
      $result['xtype'] = 'color_scheme_form';
      $result['info'] = $item['info']['#value'];
      $result['path'] = base_path() . drupal_get_path('theme', $form['color']['theme']['#value']);
      break;

    case 'system_theme_settings.color.info':
      // We don't need it since we embedded it into scheme selector. See above.
      $result['#extadmin_ignore'] = TRUE;
      break;

    case 'system_theme_settings.color.palette.base':
    case 'system_theme_settings.color.palette.link':
    case 'system_theme_settings.color.palette.top':
    case 'system_theme_settings.color.palette.bottom':
    case 'system_theme_settings.color.palette.text':
      // Change textfields into color picker.
      $result = extadmin_textfield_config($item_id, $item, $form);
      $result['xtype'] = 'farbtasticpicker';
      break;
  }
  return $result;

}
