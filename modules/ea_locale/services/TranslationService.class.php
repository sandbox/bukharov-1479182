<?php

/**
 * @file
 * TranslationService class definition.
 */

include_once './includes/locale.inc';

/**
 * Translation strings management service.
 */
class TranslationService extends ServiceBase {

  /**
   * Returns a list of translations.
   *
   * @see locale_translate_overview_screen()
   */
  public function GetList($start, $limit, $sort) {
    $groups = module_invoke_all('locale', 'groups');
    $languages = language_list();
    unset($languages['en']);

    $strings = array();
    $count = db_result(db_query('SELECT count(*) FROM {locales_source}'));
    $r1 = db_query('SELECT * FROM {locales_source}' . $this->getOrderBy($sort) . ' LIMIT %d, %d', $start, $limit);
    while ($string = db_fetch_array($r1)) {
      $r2 = db_query('SELECT DISTINCT language FROM {locales_target} WHERE lid=%d', $string['lid']);
      $translations = array();
      while ($translation = db_fetch_object($r2)) {
        $translations[] = $translation->language;
      }

      foreach ($languages as $langcode => $language) {
        $string['language_' . $langcode] = in_array($langcode, $translations);
      }

      $string['textgroup'] = $groups[$string['textgroup']];
      $strings[] = $string;
    }

    return array(
      'total' => $count,
      'strings' => $strings,
    );
  }

  /**
   * Returns a list of locale groups and enabled languages.
   */
  public function GetListOptions() {
    $groups = array();
    foreach (module_invoke_all('locale', 'groups') as $id => $name) {
      $groups[] = array(
        'id' => $id,
        'name' => $name,
      );
    }

    $languages = array();
    foreach (language_list('language', TRUE) as $language) {
      if ($language->language == 'en') {
        continue;
      }
      $languages[] = array(
        'id' => $language->language,
        'name' => $language->name,
      );
    }

    return array(
      'languages' => $languages,
      'groups' => $groups,
    );
  }

  /**
   * Returns form for editing a translation.
   *
   * @see locale_translate_edit_form()
   */
  public function GetForm($lid) {
    return extadmin_get_form_items('locale_translate_edit_form', array(), $lid);
  }

  /**
   * Process string editing form submissions.
   *
   * @see locale_translate_edit_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'locale_translate_edit_form';
    $form = extadmin_get_form($form_id, array(), $_POST['lid']);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates locale_translate_delete_form form submit to delete translation.
   *
   * @see locale_translate_delete_form_submit()
   */
  public function Delete($lid) {
    $form_id = 'locale_translate_delete_form';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
      'form_id' => $form_id,
    );

    $source = db_fetch_object(db_query('SELECT * FROM {locales_source} WHERE lid = %d', $lid));
    $form = extadmin_get_form($form_id, $data, $source);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Process the locale import form submission.
   *
   * @see locale_translate_import_form_submit()
   */
  public function Import($formHandler = TRUE) {
    $form = extadmin_get_form($_POST['form_id'], $_POST);
    $errors = array();

    // Import errors are not set as form errors,
    // we can't retrieve them using form_get_errors.
    // So we get error messages before we process the import,
    // after we process the import and then compare.
    $errors1 = drupal_get_messages('error', FALSE);
    $errors1 = $errors1['error'];
    extadmin_process_form($_POST['form_id'], $form, $_POST);
    $errors2 = drupal_get_messages('error', FALSE);
    $errors2 = $errors2['error'];

    // We try to figure out if there were any errors during import.
    if ($errors1 || $errors2) {
      $errors1 = array_merge(array_diff(is_array($errors2) ? $errors2 : array(), is_array($errors1) ? $errors1 : array()));
      if ($errors1) {
        $errors['files[file]'] = $errors1;
      }
    }

    return $this->formResult(array(), $errors);
  }
}
