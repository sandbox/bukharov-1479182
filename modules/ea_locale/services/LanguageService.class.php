<?php

/**
 * @file
 * LanguageService class definition.
 */

include_once './includes/locale.inc';

/**
 * Languages management service.
 */
class LanguageService extends ServiceBase {

  /**
   * Returns a list of enabled languages.
   *
   * @see locale_languages_overview_form()
   */
  public function GetList() {
    $languages = array();
    foreach (language_list('language', TRUE) as $language) {
      $language->default = $language->language == language_default('language');
      $languages[] = $language;
    }
    return $languages;
  }

  /**
   * Returns a list of all possible languages and language negotiation options.
   */
  public function GetListOptions() {
    $languages = array();
    foreach (_locale_prepare_predefined_list() as $key => $name) {
      $languages[] = array(
        'id' => $key,
        'name' => $name,
      );
    }
    return array(
      'languages' => $languages,
      'language_negotiation' => intval(variable_get('language_negotiation', LANGUAGE_NEGOTIATION_NONE)),
    );
  }

  /**
   * Process the language addition form submission.
   *
   * @see locale_languages_predefined_form_submit()
   */
  public function Add($code) {
    $form_id = 'locale_languages_predefined_form';
    $data = array(
      'langcode' => $code,
      'op' => t('Add language'),
      'form_id' => $form_id,
    );
    $form = extadmin_get_form($form_id, $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Returns a form for editing language or adding custom language.
   *
   * @see locale_languages_edit_form()
   * @see locale_languages_custom_form()
   */
  public function GetForm($code) {
    $form_id = $code ? 'locale_languages_edit_form' : 'locale_languages_custom_form';
    return extadmin_get_form_items($form_id, array(), $code);
  }

  /**
   * Process the language editing form submission.
   *
   * @see locale_languages_edit_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form = extadmin_get_form($_POST['form_id'], array(), $_POST['langcode']);
    extadmin_process_form($_POST['form_id'], $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates locale_languages_delete_form form submit to delete language.
   *
   * @see locale_languages_delete_form_submit()
   */
  public function Delete($code) {
    $form_id = 'locale_languages_delete_form';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
      'form_id' => $form_id,
    );

    $form = extadmin_get_form($form_id, $data, $code);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Emulates locale_languages_overview_form submit to change language status.
   *
   * @see locale_languages_overview_form_submit()
   */
  public function UpdateStatus($code, $enabled, $default) {
    $form_id = 'locale_languages_overview_form';
    $data = array(
      'enabled' => array(),
      'op' => t('Save configuration'),
      'form_id' => $form_id,
    );
    $form = extadmin_get_form($form_id);

    // Collect currently enabled languages.
    foreach ($form['enabled']['#default_value'] as $id) {
      $data['enabled'][$id] = $id;
    }

    // Add language to the list or remove it from the list.
    if ($enabled) {
      $data['enabled'][$code] = $code;
    }
    else {
      unset($data['enabled'][$code]);
    }

    // Update default radio button if necessary.
    if ($default) {
      $data['site_default'] = $code;
      $form['site_default']['#default_value'] = $code;
    }

    $data['form_token'] = $form['form_token']['#default_value'];
    $form['enabled']['#default_value'] = array_values($data['enabled']);
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Emulates locale_languages_overview_form submit
   * to save changes to language weights.
   *
   * @see locale_languages_overview_form_submit()
   */
  public function UpdateWeights($languages) {
    $form_id = 'locale_languages_overview_form';
    $data = array(
      'op' => t('Save configuration'),
      'form_id' => $form_id,
    );

    // Populate weight values.
    foreach ($languages as $language) {
      $data['weight'][$language->code] = $language->weight;
    }

    $form = extadmin_get_form($form_id);

    // Preserve enabled languages and default language.
    foreach ($form['enabled']['#default_value'] as $id) {
      $data['enabled'][$id] = $id;
    }
    $data['form_token'] = $form['form_token']['#default_value'];

    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Saves language negotiation into a variable.
   */
  public function UpdateNegotiation($language_negotiation) {
    variable_set('language_negotiation', $language_negotiation);
  }
}
