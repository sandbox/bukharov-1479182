Ext.define('ExtAdmin.locale.view.LanguageList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.locale_languages_overview_form',

  columns: [{
    header: Drupal.t('Enabled'),
    dataIndex: 'enabled',
    xtype: 'checkcolumn',
    sortable: false
  }, {
    header: Drupal.t('Code'),
    dataIndex: 'language',
    sortable: false
  }, {
    header: Drupal.t('English name'),
    dataIndex: 'name',
    sortable: false,
    flex: 1
  }, {
    header: Drupal.t('Native name'),
    dataIndex: 'native',
    sortable: false,
    flex: 1
  }, {
    header: Drupal.t('Direction'),
    dataIndex: 'direction',
    sortable: false,
    flex: 1,
    renderer: function(v) {
      return v ? Drupal.t('Right to left') : Drupal.t('Left to right')
    }
  }, {
    header: Drupal.t('Default'),
    dataIndex: 'default',
    xtype: 'checkcolumn',
    sortable: false
  }],

  store: {
    autoLoad: true,
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.LanguageService.GetList,
      reader: {
        type: 'json',
        idProperty: 'language'
      }
    },
    sorters: [{
        property: 'weight',
        direction: 'ASC'
    }],
    fields: [
      { name: 'language', type: 'string' },
      { name: 'name', type: 'string' },
      { name: 'native', type: 'string' },
      { name: 'direction', type: 'int' },
      { name: 'weight', type: 'int' },
      { name: 'enabled', type: 'boolean' },
      { name: 'default', type: 'boolean' }
    ]
  },

  tbar: [{
    xtype: 'splitbutton',
    itemId: 'add',
    text: Drupal.t('Add language'),
    iconCls: 'ea-icon-language',
    menu: {
      items: []
    }
  }, '-', {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '-', {
    itemId: 'toggle',
    text: Drupal.t('Enable language'),
    iconCls: 'ea-icon-enable-language',
    disabled: true
  }, {
    itemId: 'default',
    text: Drupal.t('Set as default'),
    iconCls: 'ea-icon-default-language',
    disabled: true
  }, '-', {
    itemId: 'up',
    text: Drupal.t('Move up'),
    iconCls: 'ea-icon-up',
    disabled: true
  }, {
    itemId: 'down',
    text: Drupal.t('Move down'),
    iconCls: 'ea-icon-down',
    disabled: true
  }, '->', {
    itemId: 'configure',
    text: Drupal.t('Language negotiation'),
    iconCls: 'ea-icon-configure',
    menu: {
      items: [{
        id: 'language_negotiation-0',
        xtype: 'menucheckitem',
        text: Drupal.t('None.'),
        checked: false,
        group: 'language_negotiation'
      }, {
        id: 'language_negotiation-1',
        xtype: 'menucheckitem',
        text: Drupal.t('Path prefix only.'),
        checked: false,
        group: 'language_negotiation'
      }, {
        id: 'language_negotiation-2',
        xtype: 'menucheckitem',
        text: Drupal.t('Path prefix with language fallback.'),
        checked: false,
        group: 'language_negotiation'
      }, {
        id: 'language_negotiation-3',
        xtype: 'menucheckitem',
        text: Drupal.t('Domain name only.'),
        checked: false,
        group: 'language_negotiation'
      }]
    }
  }]
});
