Ext.define('ExtAdmin.locale.view.Import', {
  extend: 'Ext.window.Window',
  alias: 'widget.locale_translate_import_form',

  title: Drupal.t('Import translation'),
  modal: true,
  width: 450,
  autoHeight: true,
  layout: 'fit',
  resizable: false,

  buttons: [{
    itemId: 'import',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Import')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config || {}, {
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directParams: {
          form_id: 'locale_translate_import_form',
          args: []
        },
        api: {
          submit: ExtAdmin.TranslationService.Import
        },
        paramsAsHash: true,
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  }
});
