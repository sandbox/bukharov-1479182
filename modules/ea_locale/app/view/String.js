Ext.define('ExtAdmin.locale.view.String', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.locale_translate_edit_form',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      autoLoad: true,
      directFn: ExtAdmin.TranslationService.GetForm,
      directParams: {
        lid: config.args[0]
      },
      api: {
        submit: ExtAdmin.TranslationService.Update
      },
      paramsAsHash: true
    })]);
  }
});
