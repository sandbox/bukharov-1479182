Ext.define('ExtAdmin.locale.view.StringList', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.locale_translate_overview_screen',
  layout: 'fit',

  loadLanguages: function() {
    ExtAdmin.LanguageService.GetList(function(result) {
      this.languages = result;
      this.createGrid();
    }, this);
  },

  onAdded: function() {
    this.callParent(arguments);
    this.loadLanguages();
  },

  afterRender: function() {
    this.callParent(arguments);
    this.createGrid();
  },

  refresh: function() {
    this.removeAll(true);
    this.loadLanguages();
  },

  createGrid: function() {
    if(!this.rendered || !this.languages || this.items.getCount()) {
      return;
    }

    var fields = [
      { name: 'lid', type: 'int' },
      { name: 'location', type: 'string' },
      { name: 'source', type: 'string' },
      { name: 'textgroup', type: 'string' }
    ];

    var columns = [{
      header: Drupal.t('Group'),
      dataIndex: 'textgroup'
    }, {
      header: Drupal.t('String'),
      dataIndex: 'source',
      flex: 1
    }, {
      header: Drupal.t('Location'),
      dataIndex: 'location',
      hidden: this.languages.length > 4
    }];

    Ext.each(this.languages, function(language) {
      if(language.language == 'en') {
        return;
      }

      fields.push({
        name: 'language_' + language.language,
        type: 'boolean'
      });

      columns.push({
        xtype: 'checkcolumn',
        header: language.name,
        dataIndex: 'language_' + language.language,
        sortable: false
      });
    });

    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.TranslationService.GetList,
        reader: {
          type: 'json',
          idProperty: 'lid',
          root: 'strings'
        }
      },
      remoteSort: true,
      groupField: 'textgroup',
      fields: fields
    });

    this.add({
      xtype: 'gridpanel',
      border: false,
      features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true
      }],
      store: store,
      columns: columns,
      tbar: [{
        itemId: 'edit',
        iconCls: 'ea-icon-edit',
        text: Drupal.t('Edit'),
        disabled: true
      }, {
        itemId: 'delete',
        iconCls: 'ea-icon-delete',
        text: Drupal.t('Delete'),
        disabled: true
      }, '-', {
        itemId: 'import',
        iconCls: 'ea-icon-import',
        text: Drupal.t('Import')
      }, {
        iconCls: 'ea-icon-export',
        text: Drupal.t('Export'),
        menu: {
          items: [{
            itemId: 'export-translation',
            text: Drupal.t('Export translation'),
            menu: {
              items: []
            }
          }, {
            itemId: 'export-template',
            text: Drupal.t('Export template'),
            menu: {
              items: []
            }
          }]
        }
      }],
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying strings {0} - {1} of {2}')
      }
    });
  }
});
