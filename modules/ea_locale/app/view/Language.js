Ext.define('ExtAdmin.locale.view.Language', {
  extend: 'Ext.window.Window',
  alias: 'widget.locale_languages_edit_form',

  modal: true,
  width: 400,
  height: 380,
  layout: 'fit',
  resizable: false,
  autoShow: true,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  initComponent: function() {
    Ext.apply(this, {
      title: this.code ? Drupal.t('Edit language') : Drupal.t('Add custom language'),
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directFn: ExtAdmin.LanguageService.GetForm,
        directParams: {
          code: this.code
        },
        api: {
          submit: ExtAdmin.LanguageService.Update
        }
      },
      paramsAsHash: true
    });
    this.callParent();
  }
});
