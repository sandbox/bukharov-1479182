/**
 * Controller for languages list.
 *
 * @see ExtAdmin.locale.view.LanguageList
 */
Ext.define('ExtAdmin.locale.controller.Languages', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'locale_languages_overview_form',
  formSelector: 'locale_languages_edit_form',

  listButtons: ['#edit', '#delete', '#toggle', '#default', '#up', '#down'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: this.addCustom
      },
      '#add menuitem': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#toggle': {
        click: this.toggle
      },
      '#default': {
        click: this.setDefault
      },
      '#up': {
        click: function(button) {
          this.updateSelectedWeight(-1);
        }
      },
      '#down': {
        click: function(button) {
          this.updateSelectedWeight(1);
        }
      },
      '#configure menuitem': {
        click: this.setNegotiation
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, {
            op: Drupal.t('Save language'),
            modal: true
          });
        }
      }
    };
  },

  /**
   * Populates languages selector and language negotiation options.
   */
  initList: function() {
    ExtAdmin.LanguageService.GetListOptions(function(result) {
      var menu = this.getListItem('#add').menu;
      Ext.each(result.languages, function(language) {
        menu.add({
          id: 'language-' + language.id,
          text: language.name
        });
      });
      menu = this.getListItem('#configure').menu;
      menu.items.get(result.language_negotiation).setChecked(true);
    }, this);
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);
    var record = selected[0];

    switch(button) {
      case '#delete':
        // Allow to delete only if language is not default one and not English.
        return base && !record.get('default') && record.get('language') != 'en';
      case '#toggle':
        if(base) {
          var item = this.getListItem(button);
          // Update toggle button based on language status.
          if(record.get('enabled')) {
            item.setIconCls('ea-icon-disable-language');
            item.setText(Drupal.t('Disable language'));
          }
          else {
            item.setIconCls('ea-icon-enable-language');
            item.setText(Drupal.t('Enable language'));
          }
        }
        return base && record.get('language') != 'en';
      case '#default':
        return base && !record.get('default');
      case '#up':
        return base && record.store.first().id != record.id;
      case '#down':
        return base && record.store.last().id != record.id;
    }

    return base;
  },

  addCustom: function() {
    Ext.create('ExtAdmin.locale.view.Language');
  },

  add: function(item) {
    ExtAdmin.LanguageService.Add({ code: item.id.replace('language-', '') });
    this.refreshList();
  },

  edit: function(record) {
    Ext.create('ExtAdmin.locale.view.Language', {
      code: record.get('language')
    });
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the language {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('name')),
      function() {
        ExtAdmin.LanguageService.Delete({ code: record.get('language') });
        this.refreshList();
      }
    );
  },

  toggle: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    ExtAdmin.LanguageService.UpdateStatus({
      code: record.get('language'),
      enabled: !record.get('enabled'),
      'default': record.get('default')
    });
    this.refreshList();
  },

  setDefault: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    ExtAdmin.LanguageService.UpdateStatus({
      code: record.get('language'),
      enabled: record.get('enabled'),
      'default': true
    });
    this.refreshList();
  },

  updateWeights: function() {
    this.getList().getStore().each(function(record) {
      record.set('weight', record.store.indexOf(record));
    });
  },

  updateSelectedWeight: function(diff) {
    var store = this.getList().getStore();
    var record = this.getList().getSelectionModel().getSelection()[0];

    store.sort('weight', 'ASC');
    this.updateWeights();
    var weight = record.get('weight') + diff;
    store.getAt(weight).set('weight', record.get('weight'));
    record.set('weight', weight);
    store.sort('weight', 'ASC');

    this.updateButtons([ record ]);
    this.saveChanges();
  },

  saveChanges: function() {
    var updated = [];


    Ext.each(this.getList().getStore().getUpdatedRecords(), function(record) {
      updated.push({
        code: record.get('language'),
        weight: record.get('weight')
      });
      record.commit();
    });

    if(updated.length) {
      ExtAdmin.LanguageService.UpdateWeights({ languages: updated });
    }
  },

  setNegotiation: function(item) {
    ExtAdmin.LanguageService.UpdateNegotiation({ language_negotiation: item.id.replace('language_negotiation-', '') })
  }
});
