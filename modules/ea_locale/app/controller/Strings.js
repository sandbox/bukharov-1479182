/**
 * Controller for translatable strings list.
 *
 * @see ExtAdmin.locale.view.StringList
 */
Ext.define('ExtAdmin.locale.controller.Strings', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'locale_translate_overview_screen gridpanel',
  formSelector: 'locale_translate_edit_form',

  listButtons: ['#edit', '#delete'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#import': {
        click: function() {
          Ext.create('ExtAdmin.locale.view.Import');
        }
      },
      '#export-template menuitem': {
        click: this.exportTemplate
      },
      '#export-translation menuitem': {
        click: this.exportTranslation
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { op: Drupal.t('Save translations') });
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'locale_translate_import_form #import': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'locale_translate_import_form',
            op: Drupal.t('Import'),
            modal: true
          });
        }
      }
    };
  },

  /**
   * Populates export menu.
   */
  initList: function() {
    ExtAdmin.TranslationService.GetListOptions(function(result) {
      var menu = this.getListItem('#export-translation').menu;
      Ext.each(result.languages, function(language) {
        var item = menu.add({
          id: 'translation-language-' + language.id,
          text: language.name,
          menu: {
            items: []
          }
        });
        Ext.each(result.groups, function(group) {
          item.menu.add({
            id: 'translation-group-' + language.id + '-' + group.id,
            text: group.name
          });
        });
      });
      menu = this.getListItem('#export-template').menu;
      Ext.each(result.groups, function(group) {
        menu.add({
          id: 'template-group-' + group.id,
          text: group.name
        });
      });
    }, this);
  },

  edit: function(record) {
    this.getMain().setView(
      'lid' + record.get('lid'),
      { xtype: 'locale_translate_edit_form', args: [ record.get('lid') ] },
      Drupal.t('Edit string')
    );
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Drupal.t('Are you sure you want to delete this string?') + ' ' + Drupal.t('This action cannot be undone.'),
      function() {
        ExtAdmin.TranslationService.Delete({ lid: record.get('lid') });
        this.refreshList();
      }
    );
  },

  exportTemplate: function(item) {
    window.location.href = '/extadmin/ea_locale/export/' + item.id.replace('template-group-', '');
  },

  exportTranslation: function(item) {
    if(item.id.indexOf('translation-group-') < 0) {
      return;
    }

    var id = item.id.replace('translation-group-', '').split('-');
    window.location.href = '/extadmin/ea_locale/export/' + id[1] + '/' + id[0];
  }
});
