<?php

/**
 * @file
 * InputFormatService class definition.
 */

include_once drupal_get_path('module', 'filter') . '/filter.admin.inc';

/**
 * Input formats management service.
 */
class InputFormatService extends ServiceBase {

  /**
   * Returns a list of all input formats.
   *
   * @see filter_admin_overview()
   */
  public function GetList() {
    $result = array();
    $formats = filter_formats();
    foreach ($formats as $id => $format) {
      $roles = array();
      foreach (user_roles() as $rid => $name) {
        if (strstr($format->roles, ",$rid,")) {
          $roles[] = $name;
        }
      }

      $result[] = array(
        'format' => $id,
        'name' => $format->name,
        'default' => ($id == variable_get('filter_default_format', 1)),
        'roles' => $roles,
      );
    }
    return $result;
  }

  /**
   * Generates a filter format form.
   *
   * @see filter_admin_format_form()
   */
  public function GetForm($format) {
    return extadmin_get_form_items(
      'filter_admin_format_form',
      array(),
      $format ? filter_format_load($format) : (object) array('name' => '', 'roles' => '', 'format' => '')
    );
  }

  /**
   * Processes filter format form submissions.
   *
   * @see filter_admin_format_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = $_POST['form_id'];
    $form = extadmin_get_form($form_id, array(), filter_format_load($_POST['format']));
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Changes the default input format.
   */
  public function SetDefault($format) {
    $form_id = 'filter_admin_overview';
    $data = array(
      'default' => $format,
      'op' => t('Set default format'),
      'form_id' => $form_id,
    );
    $form = extadmin_get_form($form_id, $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Emulates filter_admin_delete form submit to delete a format.
   *
   * @see filter_admin_delete_submit()
   */
  public function Delete($format) {
    $form_id = 'filter_admin_delete';
    $data = array(
      'format' => $format,
      'op' => t('Delete'),
      'confirm' => TRUE,
      'form_id' => $form_id,
    );
    // filter_admin_delete form relies on arg() function to get format id.
    // Uri inside this function is extadmin/api, so we need to change that.
    $q = $_GET['q'];
    $_GET['q'] = 'admin/settings/filters/delete/' . $format;
    $form = extadmin_get_form($form_id, $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    $_GET['q'] = $q;
    return $this->formResult();
  }
}
