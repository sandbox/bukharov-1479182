Ext.define('ExtAdmin.filter.view.InputFormat', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.filter_admin_format_form',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save configuration'),
    iconCls: 'ea-icon-save'
  }],

  autoScroll: true,

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.InputFormatService.GetForm,
      directParams: {
        format: config.args ? config.args[0] : null
      },
      api: {
        submit: ExtAdmin.InputFormatService.Update
      },
      paramsAsHash: true
    })]);
  }
});
