Ext.define('ExtAdmin.filter.view.InputFormatList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.filter_admin_overview',

  columns: [{
    xtype: 'checkcolumn',
    header: Drupal.t('Default'),
    dataIndex: 'default',
    width: 60
  }, {
    header: Drupal.t('Name'),
    dataIndex: 'name',
    flex: 1
  }, {
    header: Drupal.t('Roles'),
    dataIndex: 'roles',
    flex: 1,
    renderer: function(v, meta, record) {
      if(record.get('default')) {
        return Drupal.t('All roles may use default format');
      }

      if(!v.length) {
        return Drupal.t('No roles may use this format');
      }

      return v;
    }
  }],

  initComponent: function() {
    Ext.apply(this, {
      store: {
        autoLoad: true,
        proxy: {
          type: 'direct',
          directFn: ExtAdmin.InputFormatService.GetList,
          reader: {
            type: 'json',
            idProperty: 'format'
          }
        },
        fields: [
          { name: 'format', type: 'int' },
          { name: 'name', type: 'string' },
          { name: 'default', type: 'boolean' },
          { name: 'roles' }
        ]
      },
      tbar: [{
        itemId: 'add',
        text: Drupal.t('Add input format'),
        iconCls: 'ea-icon-input-format'
      }, {
        itemId: 'default',
        text: Drupal.t('Set as default'),
        iconCls: 'ea-icon-default-format',
        disabled: true
      }, '-', {
        itemId: 'configure',
        text: Drupal.t('Configure'),
        iconCls: 'ea-icon-configure',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }]
    });

    this.callParent(arguments);
  }
});
