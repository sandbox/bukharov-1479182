/**
 * Controller for input formats list.
 *
 * @see ExtAdmin.filter.view.InputFormatList
 */
Ext.define('ExtAdmin.filter.controller.InputFormats', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'filter_admin_overview',
  formSelector: 'filter_admin_format_form',

  listButtons: ['#default', '#configure', '#delete'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: this.add
      },
      '#default': {
        click: this.setDefault
      },
      '#configure': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { op: Drupal.t('Save configuration') });
        }
      }
    };
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);
    switch(button) {
      case '#default':
      case '#delete':
        return base && !selected[0].get('default');
    }
    return base;
  },

  add: function() {
    this.getMain().setView(
      'input-format',
      { xtype: 'filter_admin_format_form' },
      Drupal.t('Add input format')
    );
  },

  setDefault: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];

    ExtAdmin.InputFormatService.SetDefault({ format: record.get('format') });
    this.refreshList();
  },

  edit: function(record) {
    this.getMain().setView(
      'format-' + record.get('format'),
      { xtype: 'filter_admin_format_form', args: [ record.get('format') ] },
      record.get('name')
    );
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the input format {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('name')),
      function() {
        ExtAdmin.InputFormatService.Delete({ format: record.get('format')});
        this.refreshList();
      }
    );
  }
});
