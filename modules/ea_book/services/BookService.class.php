<?php

/**
 * @file
 * BookService class definition.
 */

include_once drupal_get_path('module', 'book') . '/book.admin.inc';

/**
 * Books management service.
 */
class BookService extends ServiceBase {

  /**
   * Generates a list of tree nodes representing books.
   */
  private function getRootNodes() {
    $nodes = array();
    foreach (book_get_books() as $book) {
      $nodes[] = array(
        'id' => $book['mlid'],
        'iconCls' => 'ea-icon-book',
        'allowDrag' => FALSE,
        'nid' => $book['nid'],
        'text' => $book['title'],
        'weight' => intval($book['weight']),
      );
    }
    return $nodes;
  }

  /**
   * Generates a list of tree nodes representing book pages.
   */
  private function getBookNodes($tree) {
    $nodes = array();
    foreach ($tree as $data) {
      $nodes[] = array(
        'id' => $data['link']['mlid'],
        'plid' => $data['link']['plid'],
        'iconCls' => 'ea-icon-page',
        'nid' => $data['link']['nid'],
        'text' => $data['link']['link_title'],
        'title' => $data['link']['link_title'],
        'weight' => $data['link']['weight'],
        'children' => $data['below'] ? $this->getBookNodes($data['below']) : array(),
      );
    }
    return $nodes;
  }

  /**
   * Returns a list of child nodes for specified node.
   */
  public function GetNode($node) {
    if ($node == 'root') {
      return $this->getRootNodes();
    }

    $tree = book_menu_subtree_data(menu_link_load($node));
    $tree = array_shift($tree);
    return $this->getBookNodes($tree['below']);
  }

  /**
   * Returns a list of content types.
   */
  public function GetListOptions() {
    $result = array();
    foreach (node_get_types('types', NULL, TRUE) as $type) {
      $result[] = array(
        'type' => $type->type,
        'name' => $type->name,
      );
    }
    return $result;
  }

  /**
   * Returns table of contents for a book.
   */
  public function GetOptions($bid) {
    $options = array();
    $book = db_fetch_array(db_query('SELECT * FROM {book} a INNER JOIN {menu_links} b ON (a.mlid=b.mlid AND a.nid=%d)', $bid));
    $toc = book_toc($book['bid'], array(), _book_parent_depth_limit($book));
    foreach ($toc as $mlid => $title) {
      $options[] = array(
        'id' => intval($mlid),
        'text' => $title,
      );
    }
    return $options;
  }

  /**
   * Saves changes to book hierarchy.
   */
  public function UpdateTree($links) {
    foreach ($links as $link) {
      $item = menu_link_load($link->id);
      $item['weight'] = $link->weight;
      $item['plid'] = $link->plid;
      menu_link_save($item);
    }

    return array(
      'success' => TRUE,
    );
  }
}
