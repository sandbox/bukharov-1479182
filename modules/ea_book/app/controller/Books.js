/**
 * Controller for books tree.
 *
 * @see ExtAdmin.book.view.Tree
 */
Ext.define('ExtAdmin.book.controller.Books', {
  extend: 'ExtAdmin.controller.TreeBase',

  listSelector: 'book_admin_overview',
  formSelector: 'node_add',

  listButtons: ['#view', '#edit', '#delete', '#up', '#down'],

  controlList: function() {
    return Ext.apply({
      '#create menuitem': {
        click: this.create
      },
      '#view': {
        click: this.view
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#settings': {
        click: function() {
          Ext.create('ExtAdmin.book.view.Settings');
        }
      }
    }, this.callParent());
  },

  controlForm: function() {
    return {
      '.': {
        submitsuccess: this.refreshList
      },
      '#book-bid': {
        select: this.onParentSelect
      }
    };
  },

  controlExtra: function() {
    return {
      'book_admin_settings #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'book_admin_settings',
            op: Drupal.t('Save configuration'),
            modal: true
          });
        }
      },
      'book_admin_settings #reset': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'book_admin_settings',
            op: Drupal.t('Reset to defaults'),
            modal: true
          });
        }
      }
    };
  },

  create: function(item) {
    var type = item.id.replace('node-type-', '');
    this.getMain().setView(
      'type-' + type,
      { xtype: 'node_add', args: [ type ] },
      item.text
    );
  },

  view: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.getMain().setView(
      'book' + record.get('nid'),
      { xtype: 'iframe', args: [ '/node/' + record.get('nid') ] },
      record.get('text')
    );
  },

  edit: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.getMain().setView(
      'nid' + record.get('nid'),
      { xtype: 'node_add', args: [ record.get('type'), record.get('nid') ] },
      record.get('text')
    );
  },

  doDelete: function() {
    var controller = ExtAdmin.AppInstance.getController('ExtAdmin.node.controller.Nodes');
    controller.performOperation('delete', this.getList().getSelectionModel().getSelection(), function() {
      this.refreshList();
    }, this);
  },

  /**
   * Set up page combobox based on selected book.
   */
  onParentSelect: function(combo, records, opts) {
    var parent = combo.up(this.formSelector).down('#book-plid');
    if(!parent) {
      return;
    }

    parent.show();
    if(!parent.store.proxy.type != 'direct') {
      parent.store = new Ext.data.Store({
        fields: ['id', 'text'],
        proxy: {
          type: 'direct',
          directFn: ExtAdmin.BookService.GetOptions,
          reader: {
            type: 'json'
          }
        }
      });
    }
    parent.store.getProxy().extraParams = {
      bid: records[0].get('id')
    };
    parent.setDisplayValue(records[0].get('text'));
    parent.setValue(records[0].get('mlid'));
  },

  /**
   * Populate Create content menu.
   */
  initList: function() {
    ExtAdmin.BookService.GetListOptions(function(result) {
      var menu = this.getListItem('#create').menu;
      Ext.each(result, function(type) {
        menu.add({
          id: 'node-type-' + type.type,
          text: type.name
        });
      });
    }, this);
  },

  saveChanges: function(node) {
    var updated = [];

    node.eachChild(function(child) {
      child.set('plid', node.get('id') ? node.get('id') : 0);
      child.set('weight', node.indexOf(child));

      if(child.dirty) {
        updated.push(child.data);
        child.commit();
      }
    });

    if(updated.length) {
      ExtAdmin.BookService.UpdateTree({ links: updated });
    }
  }
});
