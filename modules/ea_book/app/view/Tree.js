Ext.define('ExtAdmin.book.view.Tree', {
  extend: 'Ext.tree.Panel',
  alias: 'widget.book_admin_overview',

  rootVisible: false,
  theme: '',

  viewConfig: {
    plugins: {
      ptype: 'treeviewdragdrop',
      appendOnly: true
    }
  },

  store: {
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.BookService.GetNode,
      extraParams: { theme: '' },
      api: { destroy: Ext.emptyFn },
      reader: {
        type: 'json'
      }
    },
    fields: [
      { name: 'nid', type: 'int' },
      { name: 'plid', type: 'int' },
      { name: 'text', type: 'string' },
      { name: 'title', type: 'string' },
      { name: 'weight', type: 'int' }
    ]
  },

  tbar: [{
      itemId: 'create',
      text: Drupal.t('Create content'),
      iconCls: 'ea-icon-create-content',
      menu: {
        items: []
      }
  }, '-', {
    itemId: 'view',
    text: Drupal.t('View'),
    iconCls: 'ea-icon-preview',
    disabled: true
  }, {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '-', {
    itemId: 'up',
    text: Drupal.t('Move up'),
    iconCls: 'ea-icon-up',
    disabled: true
  }, {
    itemId: 'down',
    text: Drupal.t('Move down'),
    iconCls: 'ea-icon-down',
    disabled: true
  }, '->', {
    itemId: 'settings',
    text: Drupal.t('Settings'),
    iconCls: 'ea-icon-settings'
  }]
});
