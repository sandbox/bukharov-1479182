Ext.define('ExtAdmin.book.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.book_admin_settings',

  title: Drupal.t('Settings'),
  modal: true,
  width: 350,
  autoHeight: true,
  layout: 'fit',
  resizable: false,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config || {}, {
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directParams: {
          form_id: 'book_admin_settings',
          args: []
        },
        paramsAsHash: true,
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  }
});
