<?php

/**
 * @file
 * CommentService class definition.
 */

include_once drupal_get_path('module', 'comment') . '/comment.admin.inc';

/**
 * Comments management service.
 */
class CommentService extends ServiceBase {

  /**
   * Performs operations on comments.
   *
   * @see comment_admin_overview_submit()
   */
  public function PerformOperation($operation, $cids) {
    $form_id = $operation == 'delete' ? 'comment_multiple_delete_confirm' : 'comment_admin_overview';
    $data = array(
      'operation' => $operation,
      'op' => $operation == 'delete' ? t('Delete all') : t('Update'),
      'confirm' => TRUE,
      'form_id' => $form_id,
    );
    foreach ($cids as $cid) {
      $data['comments'][$cid] = $cid;
    }
    $form = extadmin_get_form($form_id, $data, '');
    $data['form_token'] = $form['form_token']['#default_value'];
    $form['#skip_validation'] = TRUE;
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Returns a list of possible operations;
   */
  public function GetListOptions() {
    $result = array();

    $result['operations'] = array();
    foreach (comment_operations() as $operation => $array) {
      $result['operations'][$operation] = $array[0];
    }

    return $result;
  }

  /**
   * Returns a list of comments.
   *
   * @see comment_admin_overview()
   */
  public function GetList($page, $start, $limit, $sort) {
    if ($sort) {
      switch ($sort[0]->property) {
        case 'nid__title':
          $order_by = 'b.title';
          break;

        case 'status__published':
          $order_by = 'a.status';
          break;

        default:
          $order_by = 'a.' . $sort[0]->property;
          break;
      }
      $order_by = ' ORDER BY ' . $order_by . ' ' . $sort[0]->direction;
    }

    $comments = array();
    $count = db_result(db_query('SELECT count(*) FROM {comments}'));
    $result = db_query('SELECT a.*, b.title nid__title FROM {comments} a INNER JOIN {node} b ON (a.nid=b.nid)' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($comment = db_fetch_object($result)) {
      $comment->status__published = !$comment->status;
      $comments[] = $comment;
    }
    return array(
      'total' => $count,
      'comments' => $comments,
    );
  }

  /**
   * Processes comment form submissions.
   *
   * @see comment_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'comment_form';
    $_POST['name'] = $_POST['author'];
    $_POST['pid'] = $_POST['pid'] ? $_POST['pid'] : 0;
    $form = extadmin_get_form($form_id, array(), $_POST);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Generates the basic commenting form.
   *
   * @see comment_form()
   */
  public function GetForm($cid) {
    $comment = _comment_load($cid);
    if ($comment->uid) {
      $comment->registered_name = $comment->name;
    }
    return extadmin_get_form_items('comment_form', array(), (array) $comment);
  }
}
