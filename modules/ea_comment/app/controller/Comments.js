/**
 * Controller for comments list.
 *
 * @see ExtAdmin.comment.view.List
 */
Ext.define('ExtAdmin.comment.controller.Comments', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'comment_admin',
  formSelector: 'comment_form',

  listButtons: ['#edit', '#delete', '#update-options'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#update-options menuitem': {
        click: function(item) {
          this.performOperation(item.id.replace('comment-operation-', ''));
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: function() {
          this.performOperation('delete');
        }
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button);
        }
      }
    };
  },

  /**
   * Populate operations list.
   */
  initList: function() {
    ExtAdmin.CommentService.GetListOptions(function(result) {
      var menu = this.getListItem('#update-options').menu;
      Ext.Object.each(result.operations, function(key, value) {
        menu.add({
          id: 'comment-operation-' + key,
          text: value
        });
      });
    }, this);
  },

  edit: function(record) {
    this.getMain().setView(
      'cid' + record.get('cid'), {
        xtype: 'comment_form',
        args: [ record.get('cid') ]
      },
      Drupal.t('Edit comment')
    );
  },

  performOperation: function(operation) {
    var doOperation = function(operation) {
      var cids = [];
      Ext.each(this.getList().getSelectionModel().getSelection(), function(record) {
        cids.push(record.get('cid'));
      });

      ExtAdmin.CommentService.PerformOperation({ operation: operation, cids: cids});
      this.refreshList();
    }
    if(operation != 'delete') {
      doOperation.call(this, operation);
    }
    else {
      this.confirm(
        Drupal.t('Are you sure you want to delete these comments and all their children?') + ' ' + Drupal.t('This action cannot be undone.'),
        function() {
          doOperation.call(this, operation);
        }
      );
    }
  }
});
