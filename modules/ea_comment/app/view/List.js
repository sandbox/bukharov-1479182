Ext.define('ExtAdmin.comment.view.List', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.comment_admin',

  multiSelect: true,

  columns: [{
    header: Drupal.t('Subject'),
    dataIndex: 'subject',
    flex: 1
  }, {
    header: Drupal.t('Author'),
    dataIndex: 'name'
  }, {
    header: Drupal.t('Posted in'),
    dataIndex: 'nid__title',
    flex: 1
  }, {
    header: Drupal.t('Published'),
    dataIndex: 'status__published',
    xtype: 'checkcolumn'
  }, {
    header: Drupal.t('Time'),
    dataIndex: 'timestamp',
    xtype: 'datecolumn'
  }, {
    header: Drupal.t('Hostname'),
    dataIndex: 'hostname',
    hidden: true
  }, {
    header: Drupal.t('Mail'),
    dataIndex: 'mail',
    hidden: true
  }, {
    header: Drupal.t('Homepage'),
    dataIndex: 'homepage',
    hidden: true
  }],

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.CommentService.GetList,
        reader: {
          type: 'json',
          idProperty: 'cid',
          root: 'comments'
        }
      },
      remoteSort: true,
      fields: [
        { name: 'cid', type: 'int' },
        { name: 'nid', type: 'int' },
        { name: 'pid', type: 'int' },
        { name: 'nid__title', type: 'string' },
        { name: 'uid', type: 'int' },
        { name: 'subject', type: 'string' },
        { name: 'comment', type: 'string' },
        { name: 'hostname', type: 'string' },
        { name: 'timestamp', type: 'date', dateFormat: 'timestamp' },
        { name: 'status', type: 'int'},
        { name: 'status__published', type: 'boolean'},
        { name: 'name', type: 'string' },
        { name: 'mail', type: 'string' },
        { name: 'homepage', type: 'string' }
      ]
    });

    Ext.apply(this, {
      store: store,
      tbar: [{
        itemId: 'edit',
        text: Drupal.t('Edit'),
        iconCls: 'ea-icon-edit',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }, '->', {
        itemId: 'update-options',
        text: Drupal.t('Update options'),
        iconCls: 'ea-icon-update-options',
        disabled: true,
        menu: {
          items: []
        }
      }],
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying comments {0} - {1} of {2}')
      }
    });

    this.callParent(arguments);
  }
});
