Ext.define('ExtAdmin.comment.view.Form', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.comment_form',

  tbar: [{
    itemId: 'save',
    text: Drupal.t('Save'),
    iconCls: 'ea-icon-save'
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      autoLoad: true,
      directFn: ExtAdmin.CommentService.GetForm,
      directParams: {
        cid: config.args[0]
      },
      api: {
        submit: ExtAdmin.CommentService.Update
      },
      paramsAsHash: true
    })]);
  }

});
