ExtAdmin.Application.registerController([
  'ExtAdmin.user.controller.AccessRules',
  'ExtAdmin.user.controller.Permissions',
  'ExtAdmin.user.controller.Roles',
  'ExtAdmin.user.controller.Users'
]);
