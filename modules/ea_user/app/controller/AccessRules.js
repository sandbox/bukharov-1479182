/**
 * Controller for access rules list.
 *
 * @see ExtAdmin.user.view.AccessRuleList
 */
Ext.define('ExtAdmin.user.controller.AccessRules', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'user_admin_access',
  formSelector: 'user_admin_access_form',

  listButtons: ['#edit', '#delete'],

  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: function() {
          Ext.create('ExtAdmin.user.view.AccessRule');
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#check': {
        click: this.check
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, {
            op: this.getForm().aid ? Drupal.t('Save rule') : Drupal.t('Add rule'),
            modal: true
          });
        }
      }
    };
  },

  edit: function(record) {
    Ext.create('ExtAdmin.user.view.AccessRule', { aid: record.get('aid') });
  },

  doDelete: function() {
    var types = {
      user: 'username',
      mail: 'e-mail',
      host: 'host'
    };

    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the ' + types[record.get('type')] + ' rule for {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('mask')),
      function() {
        ExtAdmin.AccessRuleService.Delete({ aid: record.get('aid') });
        this.refreshList();
      }
    );
  },

  check: function() {
    var allow = {
      user: Drupal.t('The username {0} is allowed.'),
      mail: Drupal.t('The e-mail address {0} is allowed.'),
      host: Drupal.t('The hostname {0} is allowed.')
    };
    var deny = {
      user: Drupal.t('The username {0} is not allowed.'),
      mail: Drupal.t('The e-mail address {0} is not allowed.'),
      host: Drupal.t('The hostname {0} is not allowed.')
    };
    var type = this.getListItem('#type').getValue();
    var test = this.getListItem('#test').getValue();
    ExtAdmin.AccessRuleService.Check({ type: type, test: test }, function(result) {
      Ext.MessageBox.alert(
        Drupal.t('Check rules'),
        Ext.String.format(result.success ? allow[type] : deny[type], test)
      );
    });
  }
});
