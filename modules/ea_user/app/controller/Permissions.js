/**
 * Controller for permissions tree.
 *
 * @see ExtAdmin.user.view.PermissionTree
 */
Ext.define('ExtAdmin.user.controller.Permissions', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'user_admin_perm',

  controlList: function() {
    return {
      'treepanel': {
        cellclick: this.update
      },
      '#save': {
        click: this.save
      }
    };
  },

  update: function(view, cell, index, record) {
    var column = this.getListItem('treepanel').headerCt.getHeaderAtIndex(index);
    if(column.dataIndex.indexOf('role_') == 0) {
      record.set(column.dataIndex, !record.get(column.dataIndex));
    }
  },

  save: function() {
    var records = [];
    Ext.each(this.getListItem('treepanel').getStore().getUpdatedRecords(), function(record) {
      records.push(record.data);
      record.commit();
    });

    this.getList().setLoading(true);
    ExtAdmin.RoleService.UpdatePermissions({ records: records }, function() {
      this.getList().setLoading(false);
    }, this);
  }
});
