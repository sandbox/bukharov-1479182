/**
 * Controller for users list.
 *
 * @see ExtAdmin.user.view.List
 */
Ext.define('ExtAdmin.user.controller.Users', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'user_admin',
  formSelector: 'user_register',

  listButtons: ['#edit', '#delete', '#update-options'],

  multiEdit: true,
  editOnDblClick: true,

  controlList: function() {
    return {
      '#add': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: function() {
          this.performOperation('delete');
        }
      },
      '#update-options menuitem': {
        click: function(item) {
          if(item.id.indexOf('user-operation') == -1) {
            return;
          }
          this.performOperation(item.id.replace('user-operation-', ''));
        }
      }
    };
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          var form = button.up(this.formSelector);
          this.submitForm(button, { op: form.uid ? Drupal.t('Save') : Drupal.t('Create new account'), close: form.category == 'account' });
        }
      },
      '#category menuitem': {
        click: this.setCategory
      }
    };
  },

  controlExtra: function() {
    return {
      'user_admin_settings #save': {
        click: function(button) {
          this.submitForm(button, {
            op: Drupal.t('Save configuration'),
            selector: 'user_admin_settings'
          });
        }
      },
      'user_admin_settings #reset': {
        click: function(button) {
          this.submitForm(button, {
            op: Drupal.t('Reset to defaults'),
            selector: 'user_admin_settings'
          });
        }
      }
    };
  },

  /**
   * Populate user operations list.
   */
  initList: function() {
    ExtAdmin.UserService.GetListOptions(function(result) {
      var menu = this.getListItem('#update-options').menu;
      Ext.Object.each(result, function(operation, label) {
        if(!Ext.isObject(label)) {
          menu.add({
            id: 'user-operation-' + operation,
            text: label
          });
        }
        else {
          var items = [];
          Ext.Object.each(label.operations, function(operation, label) {
            items.push({
              id: 'user-operation-' + operation,
              text: label
            });
          });
          menu.add({
            text: label.label,
            menu: {
              items: items
            }
          });
        }
      });
    }, this);
  },

  /**
   * Populate profile categories list.
   */
  initForm: function(form) {
    if(!form.uid) {
      return;
    }

    ExtAdmin.UserService.GetFormOptions(function(result) {
      if(!result || !result.length) {
        return;
      }

      var category = form.down('#category');
      category.setText(result[0].title);
      Ext.each(result, function(item) {
        category.menu.add({
          id: 'profile-category-' + item.id,
          text: item.title
        });
      });

      category.setVisible(true);
      form.down('#category-text').setVisible(true);
    }, this);
  },

  add: function() {
    this.getMain().setView(
      'user-register',
      { xtype: 'user_register' },
      Drupal.t('Add user')
    );
  },

  edit: function(record) {
    this.getMain().setView(
      'user-' + record.get('uid'),
      { xtype: 'user_register', args: [ record.get('uid') ]},
      record.get('name')
    );
  },

  performOperation: function(operation) {
    var doOperation = function(operation) {
      var uids = [];
      Ext.each(this.getList().getSelectionModel().getSelection(), function(record) {
        uids.push(record.get('uid'));
      });

      ExtAdmin.UserService.PerformOperation({ operation: operation, uids: uids});
      this.refreshList();
    }

    if(operation != 'delete') {
      doOperation.call(this, operation);
    }
    else {
      this.confirm(
        Drupal.t('Are you sure you want to delete these users?') + ' ' + Drupal.t('This action cannot be undone.'),
        function() {
          doOperation.call(this, operation);
        }
      );
    }
  },

  setCategory: function(item) {
    var id = item.id.replace('profile-category-', '');
    var button = item.parentMenu.floatParent;
    var form = button.up(this.formSelector);

    form.category = id;
    form.loader.directParams.category = id;
    form.loader.load();

    button.setText(item.text);
  }
});
