/**
 * Controller for roles list.
 *
 * @see ExtAdmin.user.view.Role
 */
Ext.define('ExtAdmin.user.controller.Roles', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'user_admin_new_role gridpanel',

  listButtons: ['#edit', '#delete'],

  controlList: function() {
    return {
      '.': {
        edit: this.update
      },
      '#add': {
        click: function(button) {
          this.add(button);
          this.doRefresh = true;
        }
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  controlExtra: function() {
    return {
      'user_admin_new_role': {
        close: function() {
          if(this.doRefresh) {
            this.doRefresh = false;
            this.refreshPermissions();
          }
        }
      },
      'user_admin_perm #add-role': {
        click: function(button) {
          this.add(button);
          this.refreshPermissions();
        }
      }
    };
  },

  update: function(editor) {
    var record = editor.record;
    ExtAdmin.RoleService.Update({
      rid: record.get('rid'),
      name: record.get('name')
    });
    record.commit();
    this.doRefresh = true;
  },

  add: function(button) {
    var field = button.ownerCt.down('#name');
    var name = field.getValue();

    if(!name) {
      field.markInvalid(Drupal.t('You must specify a valid role name.'));
      return;
    }

    ExtAdmin.RoleService.Update({ rid: 0, name: name});
    this.refreshList();
  },

  edit: function(record) {
    this.getList().getPlugin('roweditor').startEdit(record, 0);
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    ExtAdmin.RoleService.Delete({ rid: record.get('rid') });
    this.getList().getStore().remove(record);
    this.doRefresh = true;
  },

  refreshPermissions: function() {
    var panel = Ext.ComponentQuery.query('user_admin_perm')[0];
    if(!panel) {
      return;
    }

    panel.refresh();
  }
});
