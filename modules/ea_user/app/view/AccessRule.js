Ext.define('ExtAdmin.user.view.AccessRule', {
  extend: 'Ext.window.Window',
  alias: 'widget.user_admin_access_form',

  modal: true,
  autoHeight: true,
  width: 350,
  layout: 'fit',
  resizable: false,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config || {}, {
      title: config && config.aid ? Drupal.t('Edit rule') : Drupal.t('Add rule'),
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directFn: ExtAdmin.AccessRuleService.GetForm,
        directParams: {
          aid: config ? config.aid : null
        },
        api: {
          submit: ExtAdmin.AccessRuleService.Update
        },
        paramsAsHash: true,
        listeners: {
          load: this.show,
          scope: this
        }
      }
    })]);
  }
});
