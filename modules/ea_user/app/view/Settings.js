Ext.define('ExtAdmin.user.view.Settings', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.user_admin_settings',

  autoScroll: true,

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directParams: {
        form_id: 'user_admin_settings',
        args: []
      }
    })]);
  }
});
