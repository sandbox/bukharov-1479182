Ext.define('ExtAdmin.user.view.AccessRuleList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.user_admin_access',

  columns: [{
    header: Drupal.t('Access type'),
    dataIndex: 'status',
    flex: 1,
    renderer: function(v) {
      return v ? Drupal.t('Allow') : Drupal.t('Deny');
    }
  }, {
    header: Drupal.t('Rule type'),
    dataIndex: 'type',
    flex: 1
  }, {
    header: Drupal.t('Mask'),
    dataIndex: 'mask',
    flex: 1
  }],

  initComponent: function() {
    Ext.apply(this, {
      store: {
        autoLoad: true,
        proxy: {
          type: 'direct',
          directFn: ExtAdmin.AccessRuleService.GetList,
          reader: {
            type: 'json',
            idProperty: 'id'
          }
        },
        fields: [
          { name: 'aid', type: 'int' },
          { name: 'mask', type: 'string' },
          { name: 'type', type: 'string' },
          { name: 'status', type: 'int' }
        ]
      },
      tbar: [{
        itemId: 'add',
        text: Drupal.t('Add rule'),
        iconCls: 'ea-icon-access-rule'
      }, '-', {
        itemId: 'edit',
        text: Drupal.t('Edit'),
        iconCls: 'ea-icon-edit',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }, '->', {
        xtype: 'combobox',
        itemId: 'type',
        valueField: 'value',
        displayField: 'text',
        forceSelection: true,
        editable: false,
        value: 'user',
        store: {
          fields: ['value', 'text'],
          data: [{
            value: 'user',
            text: Drupal.t('Username')
          }, {
            value: 'mail',
            text: Drupal.t('E-mail')
          }, {
            value: 'host',
            text: Drupal.t('Hostname')
          }]
        }
      }, {
        xtype: 'textfield',
        itemId: 'test'
      }, {
        itemId: 'check',
        text: Drupal.t('Check rules'),
        iconCls: 'ea-icon-check-rules'
      }]
    });

    this.callParent(arguments);
  }
});
