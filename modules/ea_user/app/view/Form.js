Ext.define('ExtAdmin.user.view.Form', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.user_register',

  autoScroll: true,
  category: 'account',

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-save',
    text: Drupal.t('Save')
  }, '->', {
    itemId: 'category-text',
    xtype: 'tbtext',
    text: Drupal.t('Category') + ': ',
    hidden: true
  }, {
    itemId: 'category',
    text: '',
    menu: {
      items: []
    },
    hidden: true
  }],

  constructor: function(config) {
    this.uid = config.args ? config.args[0] : 0;
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directFn: ExtAdmin.UserService.GetForm,
      directParams: {
        uid: this.uid
      },
      api: {
        submit: ExtAdmin.UserService.Update
      }
    })]);
  }
});
