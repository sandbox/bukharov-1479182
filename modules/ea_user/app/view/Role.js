/**
 * View extension to open roles list window.
 */
Ext.override(ExtAdmin.view.Main, {
  user_admin_new_role_view: function(id, config, title) {
    Ext.create('ExtAdmin.user.view.Role', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.user.view.Role', {
  extend: 'Ext.window.Window',
  alias: 'widget.user_admin_new_role',

  modal: true,
  autoShow: true,
  height: 225,
  width: 400,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'gridpanel',
    border: false,
    plugins: {
      ptype: 'rowediting',
      pluginId: 'roweditor'
    },
    tbar: [{
      itemId: 'name',
      xtype: 'textfield'
    }, {
      itemId: 'add',
      iconCls: 'ea-icon-role',
      text: Drupal.t('Add role')
    }, '-', {
      itemId: 'edit',
      iconCls: 'ea-icon-edit',
      text: Drupal.t('Edit'),
      disabled: true
    }, {
      itemId: 'delete',
      iconCls: 'ea-icon-delete',
      text: Drupal.t('Delete'),
      disabled: true
    }],
    columns: [{
      header: Drupal.t('Name'),
      dataIndex: 'name',
      flex: 1,
      editor: {
        allowBlank: false
      }
    }],
    store: {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.RoleService.GetList,
        reader: {
          type: 'json',
          idProperty: 'rid'
        }
      },
      fields: [
        { name: 'rid', type: 'ind' },
        { name: 'name', type: 'string' },
        { name: 'locked', type: 'boolean' }
      ]
    }
  }
});
