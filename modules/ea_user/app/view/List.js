Ext.define('ExtAdmin.user.view.List', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.user_admin',

  multiSelect: true,

  columns: [{
    header: Drupal.t('Username'),
    dataIndex: 'name',
    flex: 1
  }, {
    header: Drupal.t('E-mail'),
    dataIndex: 'mail',
    flex: 1
  }, {
    header: Drupal.t('Active'),
    dataIndex: 'status',
    xtype: 'checkcolumn'
  }, {
    header: Drupal.t('Member for'),
    dataIndex: 'created',
    renderer: function(v) {
      return Ext.Date.formatInterval(v);
    }
  }, {
    header: Drupal.t('Last access'),
    dataIndex: 'access',
    renderer: function(v) {
      return Ext.String.format(Drupal.t('{0} ago'), Ext.Date.formatInterval(v));
    }
  }],

  initComponent: function() {
    if(!this.rowBodyTpl) {
      ExtAdmin.user.view.List.prototype.rowBodyTpl = new Ext.XTemplate(
        '<tpl if="roles">',
        '<table>',
        '<tr>',
          '<th>' + Drupal.t('Roles') + ':</th>',
          '<td>{roles}</td>',
        '</tr>',
        '</table>',
        '</tpl>'
      );
    }

    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.UserService.GetList,
        reader: {
          type: 'json',
          idProperty: 'uid',
          root: 'users'
        }
      },
      remoteSort: true,
      fields: [
        { name: 'uid', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'mail', type: 'string' },
        { name: 'status', type: 'boolean'},
        { name: 'created', type: 'date', dateFormat: 'timestamp' },
        { name: 'access', type: 'date', dateFormat: 'timestamp' },
        { name: 'roles' }
      ]
    });

    Ext.apply(this, {
      store: store,
      features: [{
        ftype: 'rowbody',
        getAdditionalData: function(data, idx, record, orig) {
          var headerCt = this.view.headerCt,
            colspan  = headerCt.getColumnCount();

          return {
            rowBody: this.view.up('gridpanel').rowBodyTpl.apply(data),
            rowBodyCls: this.rowBodyCls,
            rowBodyColspan: colspan
          };
        }
      }, {
        ftype: 'rowwrap'
      }],
      tbar: [{
        itemId: 'add',
        text: Drupal.t('Add user'),
        iconCls: 'ea-icon-user'
      }, '-', {
        itemId: 'edit',
        text: Drupal.t('Edit'),
        iconCls: 'ea-icon-edit',
        disabled: true
      }, {
        itemId: 'delete',
        text: Drupal.t('Delete'),
        iconCls: 'ea-icon-delete',
        disabled: true
      }, '->', {
        itemId: 'update-options',
        text: Drupal.t('Update options'),
        iconCls: 'ea-icon-update-options',
        disabled: true,
        menu: {
          items: []
        }
      }],
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying users {0} - {1} of {2}')
      }
    });

    this.callParent(arguments);
  }
});
