Ext.define('ExtAdmin.user.view.PermissionTree', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.user_admin_perm',
  layout: 'fit',

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-save',
    text: Drupal.t('Save permissions')
  }, '->', {
    itemId: 'name',
    xtype: 'textfield'
  }, {
    itemId: 'add-role',
    iconCls: 'ea-icon-role',
    text: Drupal.t('Add role')
  }],

  loadRoles: function() {
    ExtAdmin.RoleService.GetList(function(result) {
      this.roles = result;
    }, this);
  },

  load: function() {
    this.setLoading(true);
    ExtAdmin.RoleService.GetPermissionTree(function(result) {
      this.setLoading(false);

      this.add(this.createTree(result));
    }, this);
  },

  onAdded: function() {
    this.callParent(arguments);
    this.loadRoles();
  },

  afterRender: function() {
    this.callParent(arguments);
    Ext.defer(this.load, 50, this);
  },

  refresh: function() {
    this.removeAll();
    this.loadRoles();
    this.load();
  },

  createTree: function(nodes) {
    var fields = [
      { name: 'id', type: 'string' },
      { name: 'text', type: 'string' }
    ];

    var columns = [{
      xtype: 'treecolumn',
      header: Drupal.t('Permission'),
      dataIndex: 'text',
      flex: 1
    }];

    Ext.each(this.roles, function(role) {
      fields.push({
        name: 'role_' + role.rid,
        type: 'boolean'
      });

      columns.push({
        header: role.name,
        dataIndex: 'role_' + role.rid,
        width: 150,
        renderer: function(v, meta, record, rowIndex, colIndex, store, view) {
          if(record.isModified(view.panel.headerCt.getHeaderAtIndex(colIndex).dataIndex)) {
            meta.tdCls += ' x-grid-dirty-cell';
          }

          if(!record.data.leaf) {
            return '&nbsp;';
          }

          meta.tdCls += ' ea-grid-checkbox';
          if(v) {
            meta.tdCls += ' ea-grid-checkbox-checked';
          }
        }
      });
    });

    return {
      xtype: 'treepanel',
      border: false,
      rootVisible: false,
      store: {
        root: {
          expanded: true,
          children: nodes
        },
        fields: fields
      },
      columns: columns
    };
  }
});
