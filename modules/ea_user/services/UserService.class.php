<?php

/**
 * @file
 * UserService class definition.
 */

include_once drupal_get_path('module', 'user') . '/user.admin.inc';
include_once drupal_get_path('module', 'user') . '/user.pages.inc';

/**
 * User management service.
 */
class UserService extends ServiceBase {

  /**
   * Returns a list of registered users.
   *
   * @see user_admin_account()
   */
  public function GetList($page, $start, $limit, $sort) {
    if ($sort) {
      $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;
    }
    $users = array();
    $count = db_result(db_query('SELECT count(*) FROM {users}'));
    $result = db_query('SELECT uid, name, pass, mail, created, access, status FROM {users} WHERE uid > 0' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($user = db_fetch_object($result)) {
      $roles = array();
      $roles_result = db_query('SELECT b.name FROM {users_roles} a INNER JOIN {role} b ON (a.rid=b.rid AND uid=%d)', $user->uid);
      while ($role = db_fetch_object($roles_result)) {
        $roles[] = $role->name;
      }
      $user->roles = implode(', ', $roles);
      $users[] = $user;
    }

    return array(
      'total' => $count,
      'users' => $users,
    );
  }

  /**
   * Returns user editing or user registration form.
   *
   * @see user_profile_form()
   * @see user_register()
   */
  public function GetForm($uid, $category) {
    if (!$uid) {
      return extadmin_get_form_items('user_register');
    }

    $user = user_load($uid);
    $category = $category ? $category : 'account';
    return extadmin_get_form_items('user_profile_form', array(), $user, $category);
  }

  /**
   * Creates a new user or changes an existing one.
   *
   * @see user_register_submit()
   * @see user_profile_form_submit()
   */
  public function Update($category, $formHandler = TRUE) {
    if ($_POST['form_id'] == 'user_register') {
      $form_id = 'user_register';
      $form = extadmin_get_form($form_id, $_POST);
    }
    else {
      $form_id = 'user_profile_form';
      $user = user_load($_POST['_account']);
      $form = extadmin_get_form($form_id, $_POST, $user, $_POST['_category']);
    }
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Processes the user administration update form.
   *
   * @see user_admin_account_submit()
   */
  public function PerformOperation($operation, $uids) {
    $_SESSION['user_overview_filter'] = array();
    if ($operation == 'delete') {
      $form_id = 'user_multiple_delete_confirm';
      $op = t('Delete all');
    }
    else {
      $form_id = 'user_admin_account';
      $op = t('Update');
    }
    $data = array(
      'operation' => $operation,
      'op' => $op,
      'confirm' => TRUE,
      'form_id' => $form_id,
    );
    foreach ($uids as $uid) {
      $data['accounts'][$uid] = $uid;
    }
    $form = extadmin_get_form($form_id, $data);
    $data['form_token'] = $form['form_token']['#default_value'];
    $form['#skip_validation'] = TRUE;
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Returns a list of available user operations.
   */
  public function GetListOptions() {
    $i = 1;
    $result = array();
    foreach (module_invoke_all('user_operations') as $operation => $array) {
      if (!is_array($array['label'])) {
        $result[$operation] = $array['label'];
      }
      else {
        $result['group-' . $i] = array(
          'label' => $operation,
          'operations' => $array['label'],
        );
        $i++;
      }
    }
    return $result;
  }

  /**
   * Returns a list of profile categories.
   */
  public function GetFormOptions() {
    global $user;

    $empty_account = new stdClass();
    $categories = array();
    foreach (_user_categories($empty_account) as $category) {
      $callback = isset($category['access callback']) ? $category['access callback'] : 'user_edit_access';
      $args = isset($category['access arguments']) ? $category['access arguments'] : array();
      $args[0] = $user;

      if (call_user_func_array($callback, $args)) {
        $categories[] = array(
          'id' => $category['name'],
          'title' => $category['title'],
        );
      }
    }
    return $categories;
  }

}
