<?php

/**
 * @file
 * RoleService class definition.
 */

/**
 * Roles management service.
 */
class RoleService extends ServiceBase {

  /**
   * Returns a list of configured roles.
   *
   * @see user_admin_role()
   */
  public function GetList() {
    $roles = array();
    $result = db_query('SELECT * FROM {role}');
    while ($role = db_fetch_object($result)) {
      $role->locked = in_array($role->rid, array(DRUPAL_ANONYMOUS_RID, DRUPAL_AUTHENTICATED_RID));
      $roles[] = $role;
    }
    return $roles;
  }

  /**
   * Returns permissions tree.
   *
   * Root nodes represent modules and leaves represent permissions.
   *
   * @see user_admin_perm()
   */
  public function GetPermissionTree() {
    $form = extadmin_get_form('user_admin_perm');
    $nodes = array();
    foreach ($form['permission'] as $key => $data) {
      if (is_numeric($key)) {
        if ($node) {
          $nodes[] = $node;
          unset($node);
        }

        $node = array(
          'id' => $data['#value'],
          'text' => t('@module module', array('@module' => drupal_render($data))),
          'iconCls' => 'ea-icon-module',
          'expanded' => TRUE,
          'children' => array(),
        );
      }
      else {
        $child = array(
          'id' => $data['#value'],
          'text' => t($key),
          'iconCls' => 'ea-icon-permission',
          'leaf' => TRUE,
        );
        foreach ($form['role_names'] as $id => $value) {
          $child['role_' . $id] = in_array(
            $key,
            $form['checkboxes'][$id]['#default_value']
          );
        }
        $node['children'][] = $child;
      }
    }

    // Don't forget the last module.
    if ($node) {
      $nodes[] = $node;
    }

    return $nodes;
  }

  /**
   * Save changes to permissions.
   *
   * @see user_admin_perm_submit()
   */
  public function UpdatePermissions($records) {
    $form_id = 'user_admin_perm';
    $data = array(
      'op' => t('Save permissions'),
      'form_id' => $form_id,
    );
    $form = extadmin_get_form($form_id);
    $data['form_token'] = $form['form_token']['#default_value'];
    // Collect current permissions configuration.
    foreach ($form['checkboxes'] as $id => $item) {
      foreach ($item['#default_value'] as $value) {
        $data[$id][$value] = $value;
      }
    }

    // Apply changes.
    foreach ($records as $record) {
      $record = (array) $record;
      foreach ($form['role_names'] as $id => $value) {
        if ($record['role_' . $id] && !$data[$id][$record['id']]) {
          $data[$id][$record['id']] = $record['id'];
        }
        if (!$record['role_' . $id] && $data[$id][$record['id']]) {
          unset($data[$id][$record['id']]);
        }
      }
    }

    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Creates a new role or changes an existing one.
   *
   * @see user_admin_role_submit()
   */
  public function Update($rid, $name) {
    $form_id = $rid ? 'user_admin_role' : 'user_admin_new_role';
    $data = array(
      'rid' => $rid,
      'name' => $name,
      'op' => t($rid ? 'Save role' : 'Add role'),
      'form_id' => $form_id,
    );
    // user_admin_role form relies on arg() function to get role id.
    // Uri inside this function is extadmin/api, so we need to change that.
    $q = $_GET['q'];
    $_GET['q'] = 'admin/user/roles/edit/' . $rid;
    $form = extadmin_get_form($form_id);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    $_GET['q'] = $q;
    return $this->formResult();
  }

  /**
   * Deletes a role.
   *
   * @see user_admin_role_submit()
   */
  public function Delete($rid) {
    $form_id = 'user_admin_role';
    $data = array(
      'rid' => $rid,
      'name' => TRUE,
      'op' => t('Delete role'),
      'form_id' => $form_id,
    );
    // user_admin_role form relies on arg() function to get role id.
    // Uri inside this function is extadmin/api, so we need to change that.
    $q = $_GET['q'];
    $_GET['q'] = 'admin/user/roles/edit/' . $rid;
    $form = extadmin_get_form($form_id);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    $_GET['q'] = $q;
    return $this->formResult();
  }
}
