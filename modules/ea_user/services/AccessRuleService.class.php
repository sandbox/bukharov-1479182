<?php

/**
 * @file
 * AccessRuleService class definition.
 */

/**
 * Access rules management service.
 */
class AccessRuleService extends ServiceBase {

  /**
   * Lists all access rules.
   *
   * @see user_admin_access()
   */
  public function GetList() {
    $rules = array();
    $result = db_query('SELECT * FROM {access}');
    while ($rule = db_fetch_object($result)) {
      $rules[] = $rule;
    }
    return $rules;
  }

  /**
   * Returns a form to configure access rules.
   *
   * @see user_admin_access_form()
   */
  public function GetForm($aid) {
    $edit = array(
      'aid' => 0,
    );
    if ($aid) {
      $edit = db_fetch_array(
        db_query('SELECT * FROM {access} WHERE aid = %d', $aid)
      );
    }
    return extadmin_get_form_items(
      'user_admin_access_form',
      array(),
      $edit,
      $aid ? t('Add rule') : t('Save rule')
    );
  }

  /**
   * Processes user_admin_access_form submits.
   *
   * @see user_admin_access_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form_id = 'user_admin_access_form';
    $form = extadmin_get_form($form_id, $_POST, $_POST, $_POST['op']);
    extadmin_process_form($form_id, $form, $_POST);
    return $this->formResult();
  }

  /**
   * Deletes access rule.
   *
   * @see user_admin_access_delete_confirm_submit()
   */
  public function Delete($aid) {
    db_query('DELETE FROM {access} WHERE aid = %d', $aid);

    return array(
      'success' => TRUE,
    );
  }

  /**
   * Performs check against configured access rules.
   *
   * @see user_admin_access_check_submit()
   */
  public function Check($type, $test) {
    return array(
      'success' => !drupal_is_denied($type, $test),
    );
  }
}
