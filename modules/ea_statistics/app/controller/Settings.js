/**
 * Controller for Statistics settings window.
 *
 * @see ExtAdmin.statistics.view.Settings
 */
Ext.define('ExtAdmin.statistics.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    statistics_access_logging_settings: {
      modal: true
    }
  }
});
