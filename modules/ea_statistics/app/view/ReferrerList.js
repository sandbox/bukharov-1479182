Ext.override(ExtAdmin.view.Main, {
  statistics_top_referrers_view: function(id, config, title) {
    Ext.create('ExtAdmin.statistics.view.ReffererList', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.statistics.view.ReffererList', {
  extend: 'Ext.window.Window',
  alias: 'widget.statistics_top_referrers',

  modal: true,
  autoShow: true,
  height: 300,
  width: 600,
  layout: 'fit',
  resizable: false,

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.StatisticsService.GetReferrers,
        reader: {
          type: 'json',
          root: 'records'
        }
      },
      remoteSort: true,
      sorters: [{
        property: 'hits',
        direction: 'DESC'
      }],
      fields: [
        { name: 'hits', type: 'int' },
        { name: 'url', type: 'string' },
        { name: 'last', type: 'date', dateFormat: 'timestamp' }
      ]
    });

    Ext.apply(this, {
      items: {
        xtype: 'gridpanel',
        border: false,
        columns: [{
          xtype: 'numbercolumn',
          header: Drupal.t('Hits'),
          dataIndex: 'hits',
          format: '0',
          align: 'right'
        }, {
          header: Drupal.t('Url'),
          dataIndex: 'url',
          flex: 1
        }, {
          xtype: 'datecolumn',
          format: 'Y-m-d H:i:s',
          header: Drupal.t('Last visit'),
          dataIndex: 'last',
          width: 150
        }],
        store: store,
        bbar: {
          xtype: 'pagingtoolbar',
          store: store,
          displayInfo: true,
          displayMsg: Drupal.t('Displaying entries {0} - {1} of {2}')
        }
      }
    });

    this.callParent(arguments);
  }
});
