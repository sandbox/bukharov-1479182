Ext.override(ExtAdmin.view.Main, {
  statistics_top_pages_view: function(id, config, title) {
    Ext.create('ExtAdmin.statistics.view.PageList', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.statistics.view.PageList', {
  extend: 'Ext.window.Window',
  alias: 'widget.statistics_top_pages',

  modal: true,
  autoShow: true,
  height: 300,
  width: 600,
  layout: 'fit',
  resizable: false,

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.StatisticsService.GetPages,
        reader: {
          type: 'json',
          root: 'records'
        }
      },
      remoteSort: true,
      sorters: [{
        property: 'hits',
        direction: 'DESC'
      }],
      fields: [
        { name: 'hits', type: 'int' },
        { name: 'path', type: 'string' },
        { name: 'average_time', type: 'float' },
        { name: 'total_time', type: 'float' }
      ]
    });

    Ext.apply(this, {
      items: {
        xtype: 'gridpanel',
        border: false,
        columns: [{
          xtype: 'numbercolumn',
          header: Drupal.t('Hits'),
          dataIndex: 'hits',
          format: '0',
          align: 'right'
        }, {
          header: Drupal.t('Page'),
          dataIndex: 'path',
          flex: 1
        }, {
          header: Drupal.t('Average Time'),
          dataIndex: 'average_time',
          align: 'right',
          renderer: function(v) {
            return Ext.String.format(Drupal.t('{0} ms'), v.toFixed());
          }
        }, {
          header: Drupal.t('Total Time'),
          dataIndex: 'total_time',
          align: 'right',
          renderer: function(v) {
            return Ext.Date.formatDelta(v);
          }
        }],
        store: store,
        bbar: {
          xtype: 'pagingtoolbar',
          store: store,
          displayInfo: true,
          displayMsg: Drupal.t('Displaying entries {0} - {1} of {2}')
        }
      }
    });

    this.callParent(arguments);
  }
});
