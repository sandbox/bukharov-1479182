Ext.define('ExtAdmin.statistics.view.HitList', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.statistics_recent_hits',

  columns: [{
    xtype: 'datecolumn',
    format: 'Y-m-d H:i:s',
    header: Drupal.t('Date'),
    dataIndex: 'timestamp',
    width: 150
  }, {
    header: Drupal.t('Page'),
    dataIndex: 'path',
    flex: 1
  }, {
    header: Drupal.t('Title'),
    dataIndex: 'title',
    flex: 2
  }, {
    header: Drupal.t('User'),
    dataIndex: 'uid__name',
    renderer: function(v) {
      return v ? v : 'Anonymous';
    }
  }, {
    header: Drupal.t('Hostname'),
    dataIndex: 'hostname',
    hidden: true
  }],

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.StatisticsService.GetHits,
        reader: {
          type: 'json',
          idProperty: 'aid',
          root: 'records'
        }
      },
      remoteSort: true,
      sorters: [{
        property: 'timestamp',
        direction: 'DESC'
      }],
      fields: [
        { name: 'aid', type: 'int' },
        { name: 'path', type: 'string' },
        { name: 'title', type: 'string' },
        { name: 'uid__name', type: 'string' },
        { name: 'hostname', type: 'string' },
        { name: 'timestamp', type: 'date', dateFormat: 'timestamp' }
      ]
    });

    Ext.apply(this, {
      store: store,
      bbar: {
        xtype: 'pagingtoolbar',
        store: store,
        displayInfo: true,
        displayMsg: Drupal.t('Displaying entries {0} - {1} of {2}')
      }
    });

    this.callParent(arguments);
  }
});
