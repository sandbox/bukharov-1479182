Ext.override(ExtAdmin.view.Main, {
  statistics_top_visitors_view: function(id, config, title) {
    Ext.create('ExtAdmin.statistics.view.VisitorList', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.statistics.view.VisitorList', {
  extend: 'Ext.window.Window',
  alias: 'widget.statistics_top_visitors',

  modal: true,
  autoShow: true,
  height: 300,
  width: 600,
  layout: 'fit',
  resizable: false,

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.StatisticsService.GetVisitors,
        reader: {
          type: 'json',
          root: 'records'
        }
      },
      remoteSort: true,
      sorters: [{
        property: 'hits',
        direction: 'DESC'
      }],
      fields: [
        { name: 'hits', type: 'int' },
        { name: 'uid', type: 'int' },
        { name: 'uid__name', type: 'string' },
        { name: 'total', type: 'float' }
      ]
    });

    Ext.apply(this, {
      items: {
        xtype: 'gridpanel',
        border: false,
        columns: [{
          xtype: 'numbercolumn',
          header: Drupal.t('Hits'),
          dataIndex: 'hits',
          format: '0',
          align: 'right'
        }, {
          header: Drupal.t('Visitor'),
          dataIndex: 'uid__name',
          flex: 1
        }, {
          header: Drupal.t('Total Time'),
          dataIndex: 'total',
          align: 'right',
          renderer: function(v) {
            return Ext.Date.formatDelta(v);
          }
        }],
        store: store,
        bbar: {
          xtype: 'pagingtoolbar',
          store: store,
          displayInfo: true,
          displayMsg: Drupal.t('Displaying entries {0} - {1} of {2}')
        }
      }
    });

    this.callParent(arguments);
  }
});
