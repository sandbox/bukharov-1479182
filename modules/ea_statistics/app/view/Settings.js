/**
 * View extension to open Statistics settings window.
 */
Ext.override(ExtAdmin.view.Main, {
  statistics_access_logging_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.statistics.view.Settings', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.statistics.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.statistics_access_logging_settings',

  modal: true,
  autoShow: true,
  width: 350,
  height: 350,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'statistics_access_logging_settings',
      args: []
    },
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        // This window is only 350 pixels wide, but default dupalcombo width is 400.
        // We remove width attribute to use default combobox width.
        delete defaults.width;
      }
      return defaults;
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
