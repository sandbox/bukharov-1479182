<?php

/**
 * @file
 * StatisticsService class definition.
 */

include_once drupal_get_path('module', 'statistics') . '/statistics.admin.inc';

/**
 * Statistics management service.
 */
class StatisticsService extends ServiceBase {

  /**
   * Returns a list of recent hits.
   *
   * @see statistics_recent_hits()
   */
  public function GetHits($start, $limit, $sort) {
    if ($sort) {
      switch ($sort[0]->property) {
        case 'uid__name':
          $order_by = 'u.name';
          break;

        default:
          $order_by = 'a.' . $sort[0]->property;
          break;
      }
      $order_by = ' ORDER BY ' . $order_by . ' ' . $sort[0]->direction;
    }

    $records = array();
    $count = db_result(db_query('SELECT count(*) FROM {accesslog}'));
    $result = db_query('SELECT a.aid, a.path, a.title, a.uid, u.name uid__name, a.timestamp FROM {accesslog} a LEFT JOIN {users} u ON u.uid = a.uid' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($record = db_fetch_object($result)) {
      $records[] = $record;
    }
    return array(
      'total' => $count,
      'records' => $records,
    );
  }

  /**
   * Returns a list of top referrers.
   *
   * @see statistics_top_referrers()
   */
  public function GetReferrers($start, $limit, $sort) {
    if ($sort) {
      $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;
    }

    $records = array();
    $count = db_result(db_query("SELECT COUNT(DISTINCT(url)) FROM {accesslog} WHERE url <> '' AND url NOT LIKE '%%%s%%'", $_SERVER['HTTP_HOST']));
    $result = db_query("SELECT url, COUNT(url) AS hits, MAX(timestamp) AS last FROM {accesslog} WHERE url NOT LIKE '%%%s%%' AND url <> '' GROUP BY url" . $order_by . ' LIMIT %d, %d', $_SERVER['HTTP_HOST'], $start, $limit);
    while ($record = db_fetch_object($result)) {
      $records[] = $record;
    }
    return array(
      'total' => $count,
      'records' => $records,
    );
  }

  /**
   * Returns a list of top pages.
   *
   * @see statistics_top_pages()
   */
  public function GetPages($start, $limit, $sort) {
    if ($sort) {
      $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;
    }

    $records = array();
    $count = db_result(db_query("SELECT COUNT(DISTINCT(path)) FROM {accesslog}"));
    $result = db_query("SELECT COUNT(path) AS hits, path, MAX(title) AS title, AVG(timer) AS average_time, SUM(timer) AS total_time FROM {accesslog} GROUP BY path" . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($record = db_fetch_object($result)) {
      $records[] = $record;
    }
    return array(
      'total' => $count,
      'records' => $records,
    );
  }

  /**
   * Returns a list of top visitors.
   *
   * @see statistics_top_visitors()
   */
  public function GetVisitors($start, $limit, $sort) {
    if ($sort) {
      switch ($sort[0]->property) {
        case 'uid__name':
          $order_by = 'u.name';
          break;

        default:
          $order_by = $sort[0]->property;
          break;
      }
      $order_by = ' ORDER BY ' . $order_by . ' ' . $sort[0]->direction;
    }

    $records = array();
    $count = db_result(db_query("SELECT COUNT(*) FROM (SELECT DISTINCT uid, hostname FROM {accesslog}) AS unique_visits"));
    $result = db_query("SELECT COUNT(a.uid) AS hits, a.uid, u.name uid__name, a.hostname,  SUM(a.timer) AS total, ac.aid FROM {accesslog} a LEFT JOIN {access} ac ON ac.type = 'host' AND LOWER(a.hostname) LIKE (ac.mask) LEFT JOIN {users} u ON a.uid = u.uid GROUP BY a.hostname, a.uid, u.name, ac.aid" . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($record = db_fetch_object($result)) {
      $records[] = $record;
    }
    return array(
      'total' => $count,
      'records' => $records,
    );
  }
}
