/**
 * View extension to open Syslog settings window.
 */
Ext.override(ExtAdmin.view.Main, {
  syslog_admin_settings_view: function(id, config, title) {
    Ext.create('ExtAdmin.syslog.view.Settings', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.syslog.view.Settings', {
  extend: 'Ext.window.Window',
  alias: 'widget.syslog_admin_settings',

  modal: true,
  autoShow: true,
  height: 175,
  width: 300,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'drupalform',
    border: false,
    autoLoad: true,
    directParams: {
      form_id: 'syslog_admin_settings',
      args: []
    },
    defaults: function(item) {
      var defaults = ExtAdmin.view.DrupalForm.prototype.defaults.call(this, item);
      if(item.xtype == 'drupalcombo') {
        // This window is only 300 pixels wide, but default dupalcombo width is 400.
        // We remove width attribute to use default combobox width.
        delete defaults.width;
      }
      return defaults;
    }
  },

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }]

});
