/**
 * Controller for Syslog settings.
 *
 * @see ExtAdmin.syslog.view.Settings
 */
Ext.define('ExtAdmin.syslog.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    syslog_admin_settings: {
      modal: true
    }
  }

});
