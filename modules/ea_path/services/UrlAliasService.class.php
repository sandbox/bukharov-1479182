<?php

/**
 * @file
 * UrlAliasService class definition.
 */

include_once drupal_get_path('module', 'path') . '/path.admin.inc';

/**
 * URL aliases management service.
 */
class UrlAliasService extends ServiceBase {

  /**
   * Returns a list of of all defined URL aliases.
   *
   * @see path_admin_overview()
   */
  public function GetList($start, $limit, $sort) {
    if ($sort) {
      $order_by = ' ORDER BY ' . $sort[0]->property . ' ' . $sort[0]->direction;
    }

    $count = db_result(db_query("SELECT COUNT(*) FROM {url_alias} WHERE language != ''"));
    $multilanguage = (module_exists('locale') || $count);
    if ($multilanguage) {
      $languages = language_list();
    }

    $aliases = array();
    $count = db_result(db_query('SELECT count(*) FROM {url_alias}'));
    $result = db_query('SELECT * FROM {url_alias}' . $order_by . ' LIMIT %d, %d', $start, $limit);
    while ($alias = db_fetch_object($result)) {
      if ($multilanguage) {
        $alias->language__name = $alias->language ? t($languages[$alias->language]->name) : '';
      }
      $aliases[] = $alias;
    }
    return array(
      'total' => $count,
      'aliases' => $aliases,
    );
  }

  /**
   * Returns a list of enabled languages.
   */
  public function GetLanguages() {
    $languages = language_list();
    $languages[''] = array('language' => '', 'name' => t('All'));
    return array_values($languages);
  }

  /**
   * Saves a new URL alias to the database.
   *
   * @see path_admin_form_submit()
   */
  public function Update($alias) {
    $form_id = 'path_admin_form';
    $data = (array) $alias;
    $data += array(
      'op' => $alias->pid ? t('Update alias') : t('Create new alias'),
      'form_id' => $form_id,
    );
    $form = extadmin_get_form($form_id, $data, $alias->pid ? (array) $alias : array());
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Deletes an URL alias.
   */
  public function Delete($pid) {
    path_admin_delete($pid);
    return array(
      'success' => TRUE,
    );
  }
}
