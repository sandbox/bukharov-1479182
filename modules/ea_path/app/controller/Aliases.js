/**
 * Controller for URL aliases list.
 *
 * @see ExtAdmin.path.view.List
 */
Ext.define('ExtAdmin.path.controller.Aliases', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'path_admin_overview gridpanel',

  listButtons: ['#edit', '#delete'],

  controlList: function() {
    return {
      '.': {
        afterrender: function() {
          this.getList().getPlugin('roweditor').on({
            beforeedit: this.beforeEdit,
            canceledit: this.cancelEdit,
            edit: this.save,
            scope: this
          });
        }
      },
      '#add': {
        click: this.add
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      }
    };
  },

  add: function() {
    var editor = this.getList().getPlugin('roweditor');
    if(editor.editing) {
      return;
    }

    var store = this.getList().getStore();
    var record = store.createModel({});

    store.insert(0, [ record ]);
    editor.startEdit(record, 0);
  },

  beforeEdit: function(e) {
    var editor = this.getList().getPlugin('roweditor').getEditor();
    var form = editor.getForm();

    form.findField('language').valueNotFoundText = e.record.get('language__name') || Drupal.t('All');
  },

  cancelEdit: function(e) {
    if(!e.record.get('pid')) {
      this.getList().getStore().removeAt(0);
    }
  },

  save: function(e) {
    ExtAdmin.UrlAliasService.Update({ alias: e.record.data });
    this.refreshList();
  },

  edit: function(record) {
    var editor = this.getList().getPlugin('roweditor');

    editor.startEdit(record, 0);
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete path alias {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('dst')),
      function() {
        ExtAdmin.UrlAliasService.Delete({ pid: record.get('pid') });
        this.refreshList();
      }
    );
  }
});
