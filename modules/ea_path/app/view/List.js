/**
 * View extension to open a window with URL aliases list.
 */
Ext.override(ExtAdmin.view.Main, {
  path_admin_overview: function(id, config, title) {
    Ext.create('ExtAdmin.path.view.List', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.path.view.List', {
  extend: 'Ext.window.Window',
  alias: 'widget.path_admin_overview',

  modal: true,
  autoShow: true,
  height: 350,
  width: 500,
  layout: 'fit',
  resizable: false,

  initComponent: function() {
    var store = Ext.create('Ext.data.Store', {
      autoLoad: true,
      proxy: {
        type: 'direct',
        directFn: ExtAdmin.UrlAliasService.GetList,
        reader: {
          type: 'json',
          idProperty: 'pid',
          root: 'aliases'
        }
      },
      remoteSort: true,
      fields: [
        { name: 'pid', type: 'int' },
        { name: 'src', type: 'string' },
        { name: 'dst', type: 'string' },
        { name: 'language', type: 'string' },
        { name: 'language__name', type: 'string' }
      ]
    });

    var columns = [{
      header: Drupal.t('Alias'),
      dataIndex: 'dst',
      flex: 1,
      editor: {
        xtype: 'textfield',
        allowBlank: false
      }
    }, {
      header: Drupal.t('System'),
      dataIndex: 'src',
      flex: 1,
      editor: {
        xtype: 'textfield',
        allowBlank: false
      }
    }];

    if(Drupal.settings.path_multilanguage) {
      columns.push({
        header: Drupal.t('Language'),
        dataIndex: 'language',
        renderer: function(v, meta, record) {
          return v ? record.get('language__name') : Drupal.t('All');
        },
        editor: {
          xtype: 'drupalcombo',
          allowBlank: true,
          displayField: 'name',
          valueField: 'language',
          editable: false,
          forceSelection: true,
          store: {
            fields: ['language', 'name'],
            proxy: {
              type: 'direct',
              directFn: ExtAdmin.UrlAliasService.GetLanguages,
              reader: {
                type: 'json'
              }
            }
          }
        }
      });
    }

    Ext.apply(this, {
      items: {
        xtype: 'gridpanel',
        plugins: {
          ptype: 'rowediting',
          pluginId: 'roweditor'
        },
        border: false,
        store: store,
        tbar: [{
          itemId: 'add',
          iconCls: 'ea-icon-url-alias',
          text: Drupal.t('Add alias')
        }, '-', {
          itemId: 'edit',
          iconCls: 'ea-icon-edit',
          text: Drupal.t('Edit'),
          disabled: true
        }, {
          itemId: 'delete',
          iconCls: 'ea-icon-delete',
          text: Drupal.t('Delete'),
          disabled: true
        }],
        columns: columns,
        bbar: {
          xtype: 'pagingtoolbar',
          store: store,
          displayInfo: true,
          displayMsg: Drupal.t('Displaying aliases {0} - {1} of {2}')
        }
      }
    });

    this.callParent(arguments);
  }
});
