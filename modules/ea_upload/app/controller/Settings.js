/**
 * Controller for Upload settings window.
 *
 * @see ExtAdmin.upload.view.Settings
 */
Ext.define('ExtAdmin.upload.controller.Settings', {
  extend: 'ExtAdmin.controller.SettingsBase',

  formSelectors: {
    upload_admin_settings: {
      modal: false
    }
  }
});
