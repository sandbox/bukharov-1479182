Ext.define('ExtAdmin.upload.view.Settings', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.upload_admin_settings',

  tbar: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save configuration')
  }, {
    itemId: 'reset',
    iconCls: 'ea-icon-reset',
    text: Drupal.t('Reset to defaults')
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      title: config.title,
      autoLoad: true,
      directParams: {
        form_id: 'upload_admin_settings',
        args: []
      }
    })]);
  }
});
