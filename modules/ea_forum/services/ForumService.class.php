<?php

/**
 * @file
 * ForumService class definition.
 */

include_once drupal_get_path('module', 'forum') . '/forum.admin.inc';

/**
 * Forums management service.
 */
class ForumService extends ServiceBase {

  /**
   * Generates a list of nodes representing forums/forum containers.
   */
  private function getNodes($terms, $parent = 0) {
    $nodes = array();
    foreach (array_filter($terms, create_function('$term', 'return in_array(' . $parent . ', $term->parents);')) as $term) {
      $type = in_array($term->tid, variable_get('forum_containers', array())) ? 'container' : 'forum';
      $nodes[] = array(
        'id' => $term->tid,
        'text' => $term->name,
        'weight' => $term->weight,
        'type' => $type,
        'iconCls' => 'ea-icon-' . $type,
        'expanded' => TRUE,
        'children' => $this->getNodes($terms, $term->tid),
      );
    }
    return $nodes;
  }

  /**
   * Returns a tree of existing forums and containers
   *
   * @see forum_overview()
   */
  public function GetTree() {
    $vid = variable_get('forum_nav_vocabulary', '');
    return $this->getNodes(taxonomy_get_tree($vid));
  }

  /**
   * Returns a form for adding a forum to the forum vocabulary
   * or for adding a container to the forum vocabulary.
   *
   * @see forum_form_forum()
   * @see forum_form_container()
   */
  public function GetForm($type, $tid) {
    $form_id = 'forum_form_' . $type;
    return extadmin_get_form_items($form_id, array(), $tid ? forum_term_load($tid) : array());
  }

  /**
   * Processes forum form and container form submissions.
   *
   * @see forum_form_submit()
   */
  public function Update($formHandler = TRUE) {
    $form = extadmin_get_form($_POST['form_id'], array(), $_POST['tid'] ? forum_term_load($_POST['tid']) : array());
    extadmin_process_form($_POST['form_id'], $form, $_POST);
    return $this->formResult();
  }

  /**
   * Emulates forum_confirm_delete form submit to delete a forum taxonomy term.
   *
   * @see forum_confirm_delete_submit()
   */
  public function Delete($tid) {
    $form_id = 'forum_confirm_delete';
    $data = array(
      'op' => t('Delete'),
      'confirm' => TRUE,
    );

    $form = extadmin_get_form($form_id, $data, $tid);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Saves changes to forum hierarchy.
   */
  public function UpdateTree($terms) {
    foreach ($terms as $item) {
      $term = (array) taxonomy_get_term($item->id);
      $term['weight'] = $item->weight;
      $term['parent'] = array($item->parent);
      taxonomy_save_term($term);
    }
    return array(
      'success' => TRUE,
    );
  }
}
