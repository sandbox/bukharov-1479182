/**
 * Controller for forums tree.
 *
 * @see ExtAdmin.forum.view.Tree
 */
Ext.define('ExtAdmin.forum.controller.Forums', {
  extend: 'ExtAdmin.controller.TreeBase',

  listSelector: 'forum_overview',
  formSelector: 'forum_form_main',

  listButtons: ['#edit', '#delete', '#up', '#down'],

  controlList: function() {
    return Ext.apply({
      '#add-container': {
        click: this.addContainer
      },
      '#add-forum': {
        click: this.addForum
      },
      '#edit': {
        click: this.editSelected
      },
      '#delete': {
        click: this.doDelete
      },
      '#settings': {
        click: function() {
          Ext.create('ExtAdmin.forum.view.Settings');
        }
      }
    }, this.callParent());
  },

  controlForm: function() {
    return {
      '#save': {
        click: function(button) {
          this.submitForm(button, { modal: true });
        }
      }
    };
  },

  controlExtra: function() {
    return {
      'forum_admin_settings #save': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'forum_admin_settings',
            op: Drupal.t('Save configuration'),
            modal: true
          });
        }
      },
      'forum_admin_settings #reset': {
        click: function(button) {
          this.submitForm(button, {
            selector: 'forum_admin_settings',
            op: Drupal.t('Reset to defaults'),
            modal: true
          });
        }
      }
    };
  },

  addContainer: function(item) {
     Ext.create('ExtAdmin.forum.view.Form', { type: 'container' });
  },

  addForum: function(item) {
     Ext.create('ExtAdmin.forum.view.Form', { type: 'forum' });
  },

  edit: function(record) {
    Ext.create('ExtAdmin.forum.view.Form', {
      type: record.get('type'),
      tid: record.get('id')
    });
  },

  doDelete: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to delete the forum {0}?') + ' ' + Drupal.t('This action cannot be undone.'), record.get('text')),
      function() {
        ExtAdmin.ForumService.Delete({ tid: record.get('id') });
        this.refreshList();
      }
    );
  },

  saveChanges: function(node) {
    var updated = [];

    node.eachChild(function(child) {
      child.set('parent', node.get('id') ? node.get('id') : 0);
      child.set('weight', node.indexOf(child));

      if(child.dirty) {
        updated.push(child.data);
        child.commit();
      }
    });

    if(updated.length) {
      ExtAdmin.ForumService.UpdateTree({ terms: updated });
    }
  }
});
