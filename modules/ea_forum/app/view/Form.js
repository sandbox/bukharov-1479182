Ext.define('ExtAdmin.forum.view.Form', {
  extend: 'Ext.window.Window',
  alias: 'widget.forum_form_main',

  modal: true,
  width: 450,
  height: 320,
  layout: 'fit',
  resizable: false,
  autoShow: true,

  buttons: [{
    itemId: 'save',
    iconCls: 'ea-icon-ok',
    text: Drupal.t('Save')
  }],

  initComponent: function() {
    Ext.apply(this, {
      title: this.getTitle(),
      items: {
        xtype: 'drupalform',
        border: false,
        autoLoad: true,
        directFn: ExtAdmin.ForumService.GetForm,
        directParams: {
          type: this.type,
          tid: this.tid
        },
        api: {
          submit: ExtAdmin.ForumService.Update
        }
      },
      paramsAsHash: true
    });
    this.callParent();
  },

  getTitle: function() {
    switch(this.type) {
      case 'forum':
        return this.tid ? Drupal.t('Edit forum') : Drupal.t('Add forum');
      case 'container':
        return this.tid ? Drupal.t('Edit container') : Drupal.t('Add container');
    }
  }
});
