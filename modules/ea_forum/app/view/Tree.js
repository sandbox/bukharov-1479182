Ext.define('ExtAdmin.forum.view.Tree', {
  extend: 'Ext.tree.Panel',
  alias: 'widget.forum_overview',

  rootVisible: false,

  viewConfig: {
    plugins: {
      ptype: 'treeviewdragdrop',
      appendOnly: true
    }
  },

  store: {
    proxy: {
      type: 'direct',
      directFn: ExtAdmin.ForumService.GetTree,
      api: { destroy: Ext.emptyFn },
      reader: {
        type: 'json'
      }
    },
    fields: [
      { name: 'parent', type: 'int' },
      { name: 'text', type: 'string' },
      { name: 'type', type: 'string' },
      { name: 'weight', type: 'int' }
    ]
  },

  tbar: [{
    itemId: 'add-container',
    text: Drupal.t('Add container'),
    iconCls: 'ea-icon-container'
  }, {
    itemId: 'add-forum',
    text: Drupal.t('Add forum'),
    iconCls: 'ea-icon-forum'
  }, {
    itemId: 'edit',
    text: Drupal.t('Edit'),
    iconCls: 'ea-icon-edit',
    disabled: true
  }, {
    itemId: 'delete',
    text: Drupal.t('Delete'),
    iconCls: 'ea-icon-delete',
    disabled: true
  }, '-', {
    itemId: 'up',
    text: Drupal.t('Move up'),
    iconCls: 'ea-icon-up',
    disabled: true
  }, {
    itemId: 'down',
    text: Drupal.t('Move down'),
    iconCls: 'ea-icon-down',
    disabled: true
  }, '->', {
    itemId: 'settings',
    text: Drupal.t('Settings'),
    iconCls: 'ea-icon-settings'
  }]
});
