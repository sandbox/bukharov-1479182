<?php

/**
 * @file
 * TriggerService class definition.
 */

include_once drupal_get_path('module', 'trigger') . '/trigger.admin.inc';

/**
 * Triggers management service.
 */
class TriggerService extends ServiceBase {

  /**
   * Returns a list of available actions.
   */
  private function getAvailableActions($type, $op) {
    $actions = array();
    $functions = array();
    // Restrict the options list to actions that declare
    // support for this hook-op combination.
    foreach (actions_list() as $func => $metadata) {
      if (isset($metadata['hooks']['any']) || (isset($metadata['hooks'][$type]) && is_array($metadata['hooks'][$type]) && (in_array($op, $metadata['hooks'][$type])))) {
        $functions[] = $func;
      }
    }
    foreach (actions_actions_map(actions_get_all_actions()) as $aid => $action) {
      if (in_array($action['callback'], $functions)) {
        $actions[] = array(
          'aid' => $aid,
          'name' => $action['description'],
        );
      }
    }
    return $actions;
  }

  /**
   * Returns a list of configured triggers.
   *
   * @see trigger_assign()
   */
  public function GetList($type) {
    $result = array();
    $hooks = module_invoke_all('hook_info');
    foreach ($hooks as $module => $hook) {
      if (isset($hook[$type])) {
        foreach ($hook[$type] as $op => $description) {
          if (!sizeof($this->getAvailableActions($type, $op))) {
            continue;
          }

          // Insert one empty record to allow user to add more triggers
          // by clicking into empty row.
          $result[] = array(
            'op' => $op,
            'hook' => $type,
            'description' => $description['runs when'],
          );
          $actions = _trigger_get_hook_actions($type, $op);
          foreach ($actions as $aid => $name) {
            $result[] = array(
              'op' => $op,
              'hook' => $type,
              'aid' => md5($aid),
              'aid__name' => $name,
              'description' => $description['runs when'],
            );
          }
        }
      }
    }
    return $result;
  }

  /**
   * Returns a list of available trigger types.
   */
  public function GetListOptions() {
    $result = array();

    if (module_exists('comment')) {
      $result[] = array(
        'id' => 'comment',
        'name' => t('Comments'),
      );
    }
    if (module_exists('node')) {
      $result[] = array(
        'id' => 'nodeapi',
        'name' => t('Content'),
      );
    }
    $result[] = array(
      'id' => 'cron',
      'name' => t('Cron'),
    );
    if (module_exists('taxonomy')) {
      $result[] = array(
        'id' => 'taxonomy',
        'name' => t('Taxonomy'),
      );
    }
    if (module_exists('user')) {
      $result[] = array(
        'id' => 'user',
        'name' => t('Users'),
      );
    }

    $extra = array();
    $hooks = module_invoke_all('hook_info');
    foreach ($hooks as $module => $hook) {
      if (in_array($module, array('node', 'comment', 'user', 'system', 'taxonomy'))) {
        continue;
      }
      $info = unserialize(db_result(db_query("SELECT info FROM {system} WHERE name = '%s'", $module)));
      $extra[] = array(
        'id' => $module,
        'name' => $info['name'],
      );
    }

    if (sizeof($extra)) {
      // Insert a separator if we've got some additional types.
      $result[] = NULL;
    }

    return array_merge($result, $extra);
  }

  /**
   * Returns a list of available actions.
   */
  public function GetActions($type, $op) {
    return $this->getAvailableActions($type, $op);
  }

  /**
   * Assigns action to a trigger.
   *
   * @see trigger_assign_form_submit()
   */
  public function Update($op, $type, $aid) {
    $form_id = 'trigger_' . $type . '_' . $op . '_assign_form';
    $data = array(
      'op' => t('Assign'),
      'aid' => $aid,
      'operation' => $op,
      'hook' => $type,
      'form_id' => $form_id,
    );

    $form = extadmin_get_form($form_id, $data, $type, $op, '');
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }

  /**
   * Removes trigger assignment.
   *
   * @see trigger_unassign_submit()
   */
  public function Delete($op, $type, $aid) {
    $form_id = 'trigger_unassign';
    $data = array(
      'op' => t('Unassign'),
      'aid' => $aid,
      'operation' => $op,
      'hook' => $type,
      'confirm' => TRUE,
      'form_id' => $form_id,
    );

    $form = extadmin_get_form($form_id, $data, $type, $op, $aid);
    $data['form_token'] = $form['form_token']['#default_value'];
    extadmin_process_form($form_id, $form, $data);
    return $this->formResult();
  }
}
