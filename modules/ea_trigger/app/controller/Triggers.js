/**
 * Controller for triggers list.
 *
 * @see ExtAdmin.trigger.view.List
 */
Ext.define('ExtAdmin.trigger.controller.Triggers', {
  extend: 'ExtAdmin.controller.ListBase',

  listSelector: 'trigger_assign gridpanel',

  listButtons: ['#unassign'],

  controlList: function() {
    return {
      '#unassign': {
        click: this.unassign
      },
      '#type menuitem': {
        click: this.setType
      }
    };
  },

  /**
   * Populate trigger type selector.
   */
  initList: function() {
    this.getList().getPlugin('roweditor').on({
      beforeedit: this.beforeEdit,
      edit: this.save,
      scope: this
    });

    ExtAdmin.TriggerService.GetListOptions(function(result) {
      var menu = this.getListItem('#type').menu;

      Ext.each(result, function(type) {
        if(type.id) {
          menu.add({
            id: 'trigger-' + type.id,
            text: type.name
          });
        }
        else {
          menu.add({
            xtype: 'menuseparator'
          });
        }
      });
    }, this);
  },

  getButtonState: function(button, selected) {
    var base = this.callParent(arguments);

    switch(button) {
      case '#unassign':
        return base && selected[0].get('aid');
    }

    return base;
  },

  beforeEdit: function(e) {
    if(e.record.get('aid')) {
      return false;
    }

    var editor = this.getList().getPlugin('roweditor').getEditor();
    var form = editor.getForm();
    form.findField('aid').getStore().getProxy().extraParams = {
      op: e.record.get('op'),
      type: e.record.get('hook')
    };
  },

  unassign: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    this.confirm(
      Ext.String.format(Drupal.t('Are you sure you want to unassign the action {0}? You can assign it again later if you wish.'), record.get('aid__name')),
      function() {
        ExtAdmin.TriggerService.Delete({
          type: record.get('hook'),
          op: record.get('op'),
          aid: record.get('aid')
        });
        this.refreshList();
      }
    );
  },

  setType: function(item) {
    var id = item.id.replace('trigger-', '');
    var button = item.parentMenu.floatParent;
    var form = button.up(this.formSelector);

    this.getList().getStore().getProxy().extraParams.type = id;
    this.refreshList();

    button.setText(item.text);
  },

  save: function(e) {
    ExtAdmin.TriggerService.Update({
      type: e.record.get('hook'),
      op: e.record.get('op'),
      aid: e.record.get('aid')
    });
    this.refreshList();
  }
});
