Ext.override(ExtAdmin.view.Main, {
  trigger_assign_view: function(id, config, title) {
    Ext.create('ExtAdmin.trigger.view.List', {
      title: title
    });
  }
});

Ext.define('ExtAdmin.trigger.view.List', {
  extend: 'Ext.window.Window',
  alias: 'widget.trigger_assign',

  modal: true,
  autoShow: true,
  height: 400,
  width: 500,
  layout: 'fit',
  resizable: false,

  items: {
    xtype: 'gridpanel',
    border: false,
    features: {
      ftype: 'grouping',
      groupHeaderTpl: '{name}',
      hideGroupedHeader: true
    },
    plugins: {
      ptype: 'rowediting',
      pluginId: 'roweditor',
      clicksToEdit: 1
    },

    columns: [{
      header: Drupal.t('Description'),
      dataIndex: 'description'
    }, {
      header: Drupal.t('Action'),
      dataIndex: 'aid',
      flex: 1,
      renderer: function(v, meta, record) {
        return v ? record.get('aid__name') : '';
      },
      editor: {
        xtype: 'drupalcombo',
        allowBlank: false,
        displayField: 'name',
        valueField: 'aid',
        editable: false,
        forceSelection: true,
        store: {
          fields: ['aid', 'name'],
          proxy: {
            type: 'direct',
            directFn: ExtAdmin.TriggerService.GetActions,
            reader: {
              type: 'json'
            }
          }
        }
      }
    }],

    store: {
      autoLoad: true,
      proxy: {
        type: 'direct',
        extraParams: {
          type: 'nodeapi'
        },
        directFn: ExtAdmin.TriggerService.GetList,
        reader: {
          type: 'json'
        }
      },
      groupField: 'description',
      fields: [
        { name: 'hook', type: 'string' },
        { name: 'op', type: 'string' },
        { name: 'aid', type: 'string' },
        { name: 'aid__name', type: 'string' },
        { name: 'description', type: 'string' }
      ]
    },

    tbar: [{
      itemId: 'unassign',
      iconCls: 'ea-icon-delete',
      text: Drupal.t('Unassign'),
      disabled: true
    }, '->', 'Type: ', {
      itemId: 'type',
      text: Drupal.t('Content'),
      menu: {
        items: []
      }
    }]
  }
});
