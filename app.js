Ext.define('ExtAdmin.Application', {
  extend: 'Ext.app.Application',
  name: 'ExtAdmin',

  statics: {
    /**
     * Static method to register controllers.
     */
    registerController: function(controller) {
      if(Ext.isArray(controller)) {
        this.prototype.controllers = this.prototype.controllers.concat(controller);
      }
      else {
        this.prototype.controllers.push(controller);
      }
    }
  },

  // Default controllers.
  controllers: [ 'Header', 'Login' ],

  getUser: function() {
    return this.currentUser;
  },

  /**
   * Set user and render either login form or UI.
   */
  setUser: function(user) {
    if(!user || !user.uid) {
      return this.renderLogin();
    }

    this.currentUser = user;
    this.renderUi();
  },

  /**
   * Render login form
   */
  renderLogin: function() {
    // Destroy previous viewport if it exists.
    if(this.viewport) {
      this.viewport.destroy();
    }

    this.viewport = Ext.create('Ext.container.Viewport', {
      layout: 'ux.center',
      items: {
        xtype: 'user_login'
      }
    });
  },

  /**
   * Render UI
   */
  renderUi: function() {
    // Destroy previous viewport if it exists.
    if(this.viewport) {
      this.viewport.destroy();
    }

    this.main = Ext.create('ExtAdmin.view.Main', {
      region: 'center'
    });

    this.viewport = Ext.create('Ext.container.Viewport', {
      layout: 'border',
      items: [{
        xtype: 'header',
        region: 'north'
      }, this.main]
    });
  },

  /**
   * Main method of the application.
   */
  launch: function() {
    ExtAdmin.AppInstance = this;

    Ext.tip.QuickTipManager.init();

    // Get current session user.
    ExtAdmin.SessionService.GetUser(function(user) {
      Ext.fly('loading').remove();
      Ext.fly('loading-mask').fadeOut({ remove: true });

      this.setUser(user);
    }, this);
  }
});

// Create application on page load.
Ext.onReady(function() {
  ExtAdmin.Application.instance = Ext.create('ExtAdmin.Application');
});
