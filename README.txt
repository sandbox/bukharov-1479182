= ExtAdmin for Drupal 6 = 

Provides alternate administration interface that turns Drupal into a rich 
application. Built on Sencha's Ext JS 4 it brings a new UI that changes 
everything.

== Requirements ==

Requires ExtAdmin Theme to work (http://drupal.org/sandbox/bukharov/1479234).
ExtJS is also reqired. Current ExtAdmin version does not support ExtJS 4.1.
Please download ExtJS 4.0.7 GPL from this page:
http://www.sencha.com/products/extjs/download/ext-js-4.0.7
Put the contents in ext directory, make sure ext-all.js is in the directory root.

== Supported Browsers ==

* Internet Explorer 6+
* Firefox 3.6+ (PC, Mac)
* Safari 5+
* Chrome 10+
* Opera 11+ (PC, Mac)

== Supported Modules ==

The first version of ExtAdmin supports only core Drupal modules. 

== Demo Site ==

http://drupal6.extjsadmin.com/admin

Username: demo
Password: demo
