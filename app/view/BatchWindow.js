Ext.define('ExtAdmin.view.BatchWindow', {
  extend: 'Ext.window.Window',
  alias: 'widget.batchwindow',

  title: Drupal.t('Rebuilding content permissions'),
  width: 300,
  height: 100,
  closable: false,
  resizable: false,
  constrain: true,
  layout: 'fit',
  autoShow: true,

  items: {
    xtype: 'panel',
    bodyPadding: 10,
    border: false,
    layout: {
      type: 'vbox',
      align: 'stretch',
      pack: 'center'
    },
    items: {
      xtype: 'progressbar'
    }
  },

  initComponent: function() {
    var batch = this.batch.sets[0];

    this.title = batch.title;
    this.callParent();

    this.down('progressbar').updateText(batch.init_message);
  }

});
