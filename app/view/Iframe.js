Ext.define('ExtAdmin.view.Iframe', {
  extend: 'Ext.panel.Panel',
  alias: 'widget.iframe',

  onRender: function() {
    this.callParent(arguments);
    this.iframe = this.body.createChild({
      tag: 'iframe',
      src: 'about:blank',
      name: this.id,
      frameborder: 0,
      style: {
        width: '100%',
        height: '100%'
      },
      scrolling: Ext.isIE ? 'yes' : 'auto'
    });
  },

  afterRender: function() {
    if(this.args) {
      this.iframe.dom.src = this.args[0];
    }
    this.callParent(arguments);
  }
});
