/**
 * Base class for all forms.
 */
Ext.define('ExtAdmin.view.DrupalForm', {
  extend: 'Ext.form.Panel',
  alias: 'widget.drupalform',

  bodyPadding: 10,

  defaults: function(item) {
    var defaults = {
      anchor: '99%',
      labelAlign: 'top',
      flex: 1
    };

    switch(item.xtype) {
      case 'displayfield':
        delete defaults.flex;
        break;

      case 'splitter':
        return;

      case 'checkbox':
      case 'radio':
      case 'numberfield':
        delete defaults.anchor;
        break;

      case 'datefield':
      case 'combo':
      case 'drupalcombo':
      case 'filefield':
        delete defaults.anchor;
        delete defaults.flex;
        defaults.width = 400;
        break;

      case 'fieldcontainer':
        delete defaults.flex;
        defaults.defaults = this.defaults;
        break;

      case 'fieldset':
      case 'container':
        return {
          anchor: '99%',
          defaults: this.defaults
        };
    }
    return defaults;
  },

  constructor: function(config) {
    this.addEvents('load', 'submitsuccess');
    this.callParent([Ext.applyIf(config, {
      api: {
        submit: ExtAdmin.FormService.Process
      },
      loader: Ext.create('Ext.ux.DirectComponentLoader', {
        target: this,
        directFn: config.directFn ? config.directFn : ExtAdmin.FormService.GetItems,
        directParams: config.directParams,
        autoLoad: config.autoLoad,
        loadMask: true,
        listeners: {
          load: function() {
            this.fireEvent('load', this);
          },
          scope: this
        }
      })
    })]);
  }
});
