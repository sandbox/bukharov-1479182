Ext.define('ExtAdmin.view.Login', {
  extend: 'ExtAdmin.view.DrupalForm',
  alias: 'widget.user_login',

  title: Drupal.t('ExtAdmin Login'),
  width: 400,
  height: 258,
  frame: true,
  cls: 'ea-user_login',

  buttons: [{
    itemId: 'login',
    text: Drupal.t('Login'),
    iconCls: 'ea-icon-ok'
  }, {
    itemId: 'reset',
    text: Drupal.t('Reset'),
    iconCls: 'ea-icon-reset'
  }],

  constructor: function(config) {
    this.callParent([Ext.apply(config, {
      autoLoad: true,
      directParams: {
        form_id: 'user_login',
        args: []
      },
      api: {
        submit: ExtAdmin.SessionService.SetUser
      },
      paramsAsHash: true
    })]);
  },

  initComponent: function() {
    this.bodyPadding = undefined;
    this.callParent(arguments);
  }
});
