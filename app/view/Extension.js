/**
 * Registry of view extensions.
 */
Ext.define('ExtAdmin.view.Extension', {
  extensions: {},

  statics: {
    register: function(selector, fn) {
      if(Ext.isObject(selector)) {
        Ext.apply(this.prototype.extensions, selector);
      }
      else {
        if(!Ext.isArray(this.prototype.extensions[selector])) {
          this.prototype.extensions[selector] = [];
        }

        if(Ext.isString(selector)) {
          this.prototype.extensions[selector].push(fn);
        }
        if(Ext.isArray(selector)) {
          Ext.each(selector, function(string) {
            this.prototype.extensions[string].push(fn);
          }, this);
        }
      }
    }
  },

  initComponent: function(component) {
    Ext.Object.each(this.extensions, function(selector, funcs) {
      if(component.is(selector)) {
        Ext.each(funcs, function(fn) {
          fn.call(component);
        });
      }
    });
  }
});
