Ext.define('ExtAdmin.view.Header', {
  extend: 'Ext.container.Container',
  alias: 'widget.header',

  cls: 'ea-header',
  height: 52,

  items: [{
    xtype: 'toolbar',
    itemId: 'navigation'
  }]
});
