/**
 * Main container.
 */
Ext.define('ExtAdmin.view.Main', {
  extend: 'Ext.tab.Panel',

  itemId: 'main',
  padding: 5,
  layout: 'border',

  items: [{
    xtype: 'welcome'
  }],

  /**
   * Change active view.
   */
  setView: function(id, config, title) {
    var xtype = config.xtype;
    var id = xtype + '-' + id + '-view';
    var item = this.getComponent(id);

    // Create new view only if there is no view with such id.
    if(!item) {
      // When default behavior is not needed we add an extension to this class.
      // So before creating view check if there is an extension defined for it.
      var func = xtype + '_view';
      if(Ext.isFunction(this[func])) {
        item = this[func](id, config, title);
      }
      else {
        // Check if there are any classes registered for specified xtype.
        if(!Ext.ClassManager.getNameByAlias('widget.' + xtype)
          && !Ext.ComponentManager.isRegistered(xtype)
        ) {
          Ext.MessageBox.alert('ExtAdmin', 'This action is not supported by ExtAdmin.');
          return;
        }

        item = this.add({
          xtype: xtype,
          cls: 'ea-' + xtype,
          id: id,
          title: title,
          args: config.args,
          closable: true
        });
      }
    }

    if(item) {
      this.setActiveTab(item);
    }

    return item;
  }
});
