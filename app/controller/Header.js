/**
 * Controller for header component.
 *
 * @see ExtAdmin.view.Header
 */
Ext.define('ExtAdmin.controller.Header', {
  extend: 'ExtAdmin.controller.Base',

  refs: [{
    selector: 'header #navigation',
    ref: 'navigation'
  }, {
    selector: '#main',
    ref: 'main'
  }],

  init: function() {
    this.control({
      'header #navigation': {
        afterrender: this.load
      },
      'header #navigation menuitem': {
        click: this.itemClick
      },
      'header #logout': {
        click: this.logout
      }
    });
  },

  load: function() {
    ExtAdmin.NavigationService.GetTree(function (nodes) {
      var toolbar = this.getNavigation();
      Ext.each(nodes, function(node) {
        toolbar.add({
          text: node.text,
          menu: this.createMenu(node),
          iconCls: node.iconCls
        });
      }, this);

      toolbar.add({ xtype: 'tbfill' });
      toolbar.add({
        itemId: 'logout',
        text: ExtAdmin.Application.instance.getUser().name,
        iconCls: 'ea-icon-user'
      });
    }, this);
  },

  createMenu: function(node) {
    var items = [];
    Ext.each(node.children, function(child) {
      var item = Ext.copyTo({}, child, ['id', 'text', 'config', 'iconCls']);

      if(child.children && child.children.length) {
        item.menu = this.createMenu(child);
      }

      items.push(item);
    }, this);

    return {
      items: items
    }
  },

  itemClick: function(item) {
    if(item.menu) {
      return;
    }

    this.getMain().setView(
      item.id,
      item.config,
      item.text
    );
  },

  logout: function() {
    this.confirm(
      Drupal.t('Are you sure you want to log out?'),
      function() {
        ExtAdmin.SessionService.Logout(function() {
          window.location.href = '/admin?' + Math.random();
        });
      }
    );
  }
});
