/**
 * Base class for all ExtAdmin controllers.
 */
Ext.define('ExtAdmin.controller.Base', {
  extend: 'Ext.app.Controller',

  constructor: function() {
    // Add references defined in derived classes
    this.refs = [{
      ref: 'main',
      selector: '#main'
    }].concat(this.refs || []);

    this.callParent(arguments);
  },

  /**
   * Helper function to handle form submits.
   *
   * @param button
   *  A button of the form to be submitted.
   * @param config
   *  Configuration object, possible options:
   *   - op: Text value to submit as form operation.
   *   - modal: Flag indicating if submitted form is in modal window.
   *   - close: Flag that tells this function to close form container after submit.
   *   - selector: Component query selector for the form container.
   *   - params: An object with additional parameters to post.
   *   - success: A callback function for successful submit.
   */
  submitForm: function(button, config) {
    // Apply defaults.
    config = Ext.applyIf(config || {}, {
      op: Drupal.t('Save'),
      modal: false,
      close: true
    });

    // If the function was called for a modal window get window panel, otherwise get parent panel for the button.
    var panel = config.modal ? button.up('window') : button.up(config.selector);
    var form = config.modal ? panel.down('form').getForm() : panel.getForm();

    // Validate the form before submit.
    if(!form.isValid()) {
      return;
    }

    // Disable the button to prevent double submit.
    button.disable();
    form.submit({
      // Since we already validated the form, we don't need further validation.
      clientValidation: false,

      params: Ext.apply(config.params || {}, {
        op: config.op
      }),

      // Successful submit callback.
      success: function(form, action) {
        button.enable();

        // Call custom success function if provided.
        if(Ext.isFunction(config.success)) {
          config.success.call(this, form, action);
        }

        panel.fireEvent('submitsuccess', form, action);

        // Close form container if needed.
        if(config.close) {
          if(config.modal) {
            panel.close();
          }
          else {
            this.getMain().remove(panel);
          }
        }
      },

      // Failed submit callback.
      failure: function(form, action) {
        button.enable();
        var error = '';
        if(action.failureType == action.CONNECT_FAILURE) {
          error = Ext.String.format(Drupal.t('Connection failure: {0}'), action.response.statusText);
        }
        else {
          Ext.Object.each(action.result.errors, function(key, value) {
            // If there is a field for this error, ExtJS will handle error display.
            if(form.findField(key)) {
              error = '';
              return false;
            }
            // If we couldn't find any field for the error, we save it for later to show it in a message box.
            error = value;
          });
        }

        if(error) {
          Ext.MessageBox.alert(Drupal.t('Error'), error);
        }
      },
      scope: this
    });
  },

  /**
   * Helper function to show confirmation messages.
   *
   * @param message
   *  Confirmation message.
   * @param callback
   *  A function to call if user confirms the action.
   * @param title
   *  Message box title.
   */
  confirm: function(message, callback, title) {
    Ext.MessageBox.show({
      title: title || Drupal.t('Confirmation'),
      msg: message,
      buttons: Ext.Msg.YESNO,
      fn: function(id) {
        if(id == 'yes') {
          callback.call(this);
        }
      },
      scope: this
    });
  }
});
