/**
 * Base class for all ExtAdmin list controllers.
 */
Ext.define('ExtAdmin.controller.ListBase', {
  extend: 'ExtAdmin.controller.Base',

  constructor: function() {
    this.refs = [];

    // Add a reference to controlled list if defined.
    if(this.listSelector) {
      this.refs.push({
        ref: 'list',
        selector: this.listSelector
      });
    }

    // Add a reference to corresponding form if defined.
    if(this.formSelector) {
      this.refs.push({
        ref: 'form',
        selector: this.formSelector
      });
    }

    this.callParent(arguments);
  },

  init: function() {
    var config = {};

    if(this.listSelector) {
      config[this.listSelector] = {};

      // Process rules from derived classes.
      Ext.Object.each(this.controlList(), function(key, value) {
        config[this.listSelector + (key == '.' ? '' : (' ' + key))] = value;
      }, this);

      // Apply default rules.
      Ext.applyIf(config[this.listSelector], {
        afterrender: this.initList,
        selectionchange: this.onSelectionChange,
        itemdblclick: this.onItemdblClick
      });
    }

    if(this.formSelector) {
      config[this.formSelector] = {};

      // Process rules from derived classes.
      Ext.Object.each(this.controlForm(), function(key, value) {
        config[this.formSelector + (key == '.' ? '' : (' ' + key))] = value;
      }, this);

      // Apply default rules.
      Ext.applyIf(config[this.formSelector], {
        added: this.initForm
      });
    }

    // Process rules that aren't related to a list or a form associated with that list.
    Ext.apply(config, this.controlExtra());

    this.control(config);
  },

  /**
   * Stub method for control rules. Derived classes override it if necessary.
   */
  controlExtra: function() {
    return {};
  },

  /**
   * Stub method for control rules. Derived classes override it if necessary.
   */
  controlList: function() {
    return {};
  },

  /**
   * Stub method for control rules. Derived classes override it if necessary.
   */
  controlForm: function() {
    return {};
  },

  /**
   * Helper function to refresh the data in controlled list.
   */
  refreshList: function() {
    if(!this.listSelector) {
      return;
    }

    var list = this.getList();
    if(list) {
      list.getStore().load();
    }
  },

  /**
   * Helper function to find list child items.
   */
  getListItem: function(selector) {
    return this.getList().down(selector);
  },

  /**
   * List initialization method stub.
   */
  initList: Ext.emptyFn,

  /**
   * Form initialization method stub.
   */
  initForm: Ext.emptyFn,

  /**
   * Returns button state. True for enabled, false for disabled.
   */
  getButtonState: function(button, selected) {
    return selected.length > 0;
  },

  /**
   * Change list button states based on selection.
   */
  updateButtons: function(selected) {
    Ext.each(this.listButtons, function(button) {
      this.getListItem(button).setDisabled(!this.getButtonState(button, selected));
    }, this);
  },

  onSelectionChange: function(model, selected, opts) {
    if(this.listButtons) {
      this.updateButtons(selected);
    }
  },

  onItemdblClick: function(view, record) {
    if(this.editOnDblClick) {
      this.edit(record);
    }
  },

  /**
   * Helper function to edit multiple items or single item based on selection.
   */
  editSelected: function() {
    var selection = this.getList().getSelectionModel().getSelection();
    if(this.multiEdit) {
      Ext.each(selection, function(record) {
        this.edit(record);
      }, this);
    }
    else {
      this.edit(selection[0]);
    }
  },

  /**
   * Overridden submitForm from ExtAdmin.controller.Base
   */
  submitForm: function(button, config) {
    config = config || {};

    // Refresh the list on successful submit.
    var success = function() {
      if(!config || config.refresh !== false) {
        this.refreshList();
      }
    }

    // If config already had success callback, append our callback to provided one.
    config.success = Ext.isFunction(config.success) ? Ext.Function.createSequence(config.success, success, this) : success;

    // If no selector provided use form selector for submit.
    if(!config.selector) {
      config.selector = this.formSelector;
    }

    this.callParent(arguments);
  }
});
