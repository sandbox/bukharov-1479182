/**
 * Base class for all settings dialogues in ExtAdmin.
 */
Ext.define('ExtAdmin.controller.SettingsBase', {
  extend: 'ExtAdmin.controller.Base',

  // Common buttons used in settings forms
  formButtons: {
    '#save': Drupal.t('Save configuration'),
    '#reset': Drupal.t('Reset to defaults')
  },

  init: function() {
    var config = {};

    if(this.formSelectors) {
      // Apply default control rules to forms specified by derived class.
      Ext.Object.each(this.formSelectors, function(selector, options) {
        Ext.Object.each(this.formButtons, function(button_selector, op) {
          config[selector + ' ' + button_selector] = {
            click: function(button) {
              this.submitForm(button, Ext.apply({
                op: op,
                selector: selector
              }, options));
            }
          };
        }, this);
      }, this);
    }

    Ext.apply(config, this.controlExtra());

    this.control(config);
  },

  controlExtra: function() {
    return {};
  }
});
