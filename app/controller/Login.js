/**
 * Controller for login form.
 *
 * @see ExtAdmin.view.Login
 */
Ext.define('ExtAdmin.controller.Login', {
  extend: 'ExtAdmin.controller.Base',

  refs: [{
    selector: 'user_login #login',
    ref: 'button'
  }],

  init: function() {
    this.control({
      'user_login #login': {
        click: this.login
      },
      'user_login #reset': {
        click: this.reset
      },
      'user_login textfield': {
        specialkey: function(field, e) {
          if(e.getKey() == e.ENTER) {
            this.login(this.getButton());
          }
        }
      }
    });
  },

  login: function(button) {
    button.disable();
    button.up('form').getForm().submit({
      clientValidation: false,
      success: function(form, action) {
        window.location.href = Drupal.settings.basePath + 'admin?' + Math.random();
      },
      failure: function(form, action) {
        button.enable();
        Ext.MessageBox.alert(
          Drupal.t('Error'),
          action.result.error || Drupal.t('Unable to process request. Please try again later.')
        );
      },
      scope: button
    });
  },

  reset: function(button) {
    button.up('form').getForm().reset();
  }
});
