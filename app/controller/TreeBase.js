/**
 * Base class for all tree controllers in ExtAdmin.
 */
Ext.define('ExtAdmin.controller.TreeBase', {
  extend: 'ExtAdmin.controller.ListBase',

  /**
   * Overridden controlList from ExtAdmin.controller.ListBase
   */
  controlList: function() {
    this.expanded = [];
    return {
      '.': {
        itemcollapse: this.collapse,
        itemexpand: this.expand,
        load: this.load
      },
      '#up': {
        click: this.selectedUp
      },
      '#down': {
        click: this.selectedDown
      },
      'treeview': {
        drop: function(node, data, record, pos) {
          this.updateButtons(data.records);
          this.saveChanges(data.records[0].parentNode);
        }
      }
    };
  },

  /**
   * Remove node from expanded nodes list.
   */
  collapse: function(node) {
    Ext.Array.remove(this.expanded, node.internalId);
  },

  /**
   * Add node to expanded nodes list.
   */
  expand: function(node) {
    var index = Ext.Array.indexOf(this.expanded, node.internalId);
    if(index < 0) {
      this.expanded.push(node.internalId);
    }
  },

  /**
   * Expand nodes from expanded list on node load.
   *
   * Every time a TreePanel reloads, all nodes return to their initial state.
   * So we store a list of expanded nodes and expand them again after parent node was loaded.
   */
  load: function(store, parent) {
    Ext.each(this.expanded, function(id) {
      var node = this.getList().getStore().getNodeById(id);
      if(node && id != parent.internalId && !node.data.expanded) {
        node.expand();
      }
    }, this);
  },

  getButtonState: function(button, selected) {
    var record = selected[0];
    var base = this.callParent(arguments);

    switch(button) {
      case '#up':
        return base && (!record.isFirst() || !record.parentNode.isFirst());
      case '#down':
        return base && (!record.isLast() || !record.parentNode.isLast());
    }

    return base;
  },

  /**
   * Move selected node up.
   */
  selectedUp: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    if(record.isFirst()) {
      if(record.parentNode.previousSibling) {
        record.parentNode.previousSibling.expand();
        record.parentNode.previousSibling.appendChild(record);
      }
    }
    else {
      record.parentNode.insertBefore(record, record.previousSibling);
    }

    this.updateButtons([record]);
    this.saveChanges(record.parentNode);
  },

  /**
   * Move selected node down.
   */
  selectedDown: function() {
    var record = this.getList().getSelectionModel().getSelection()[0];
    if(record.isLast()) {
      if(record.parentNode.nextSibling) {
        record.parentNode.nextSibling.expand();
        record.parentNode.nextSibling.insertChild(0, record);
      }
    }
    else {
      record.parentNode.insertChild(record.parentNode.indexOf(record.nextSibling) + 1, record);
    }

    this.updateButtons([record]);
    this.saveChanges(record.parentNode);
  },

  /**
   * Save changes to the tree. Should be implemented by derived classes.
   */
  saveChanges: Ext.emptyFn
});
