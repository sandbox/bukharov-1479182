/**
 * Overrides standard combobox
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.field.ComboBox
 */
Ext.define('Ext.ux.DrupalComboBox', {
  extend: 'Ext.form.field.ComboBox',
  alias: 'widget.drupalcombo',

  constructor: function(config) {
    var proxy;

    // If auto complete configured, provide an adapter to convert Drupal data into ExtJS format.
    if(config.autocomplete_path) {
      proxy = {
        type: 'ajax',
        url: '/' + config.autocomplete_path,
        getUrl: function(request) {
          return this.url + '/' + request.params.query;
        },
        extractResponseData: function(response) {
          var obj = Ext.decode(response.responseText);
          var data = [];
          for(var id in obj) {
            data.push({
              id: id,
              text: obj[id]
            });
          }
          response.responseText = Ext.encode(data);
          return response;
        }
      };
    }
    else {
      proxy = {
        type: 'direct',
        directFn: ExtAdmin.FormService.GetData,
        extraParams: {
          data_id: config.data_id
        },
        reader: {
          type: 'json'
        }
      };
    }

    if(config.data_id || config.autocomplete_path) {
      Ext.apply(config, {
        store: {
          fields: ['id', 'text'],
          proxy: proxy
        }
      });
    }

    this.callParent(arguments);
  },

  setDisplayValue: function(text) {
    this.displayValue = text;
    this.setRawValue(text);
    this.clearInvalid();
  },

  getDisplayValue: function () {
    if (this.displayValue && (!this.displayTplData || this.displayTplData && !this.displayTplData.length)) {
      return this.displayValue;
    }

    return this.callParent();
  },

  /**
   * Overrides standard setValue method.
   *
   * Standard setValue method requires all items to be present in the data store to work.
   * But our data isn't loaded yet when setValue gets called, so we set the value manually.
   *
   */
  setValue: function (value) {
    this.callParent(arguments);

    if ((!Ext.isDefined(this.value) || this.value === null || Ext.isArray(this.value) && !this.value.length) && value) {
        this.value = value;
    }
  }
});
