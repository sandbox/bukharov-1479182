Ext.override(Ext.form.Basic, {
    /**
     * Improved version of standard findField function. When searching by id also checks itemId property.
     */
    findField: function(id) {
        return this.getFields().findBy(function(f) {
            return f.id === id || f.itemId === id || f.getName() === id;
        });
    }
});
