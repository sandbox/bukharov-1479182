Ext.define('Ext.ux.layout.Center', {
  extend: 'Ext.layout.container.Fit',
  alias: 'layout.ux.center',

  setItemSize : function(item, width, height){
    this.owner.addCls('ux-layout-center');
    if (item && height > 0) {
      if (width) {
        width = item.width;
        if (Ext.isNumber(item.widthRatio)) {
          width = Math.round(this.owner.el.getWidth() * item.widthRatio);
        }
      }
      this.owner.getEl().setStyle('padding-top', Math.round((height - item.height)/2) + 'px');
      item.setSize(width, item.height);
      item.getEl().setStyle('margin', '0 auto 0 auto');
    }
  },
  destroy : function(){
    this.callParent();

    this.owner.removeCls('ux-layout-center');
    this.owner.getEl().setStyle('padding-top', '0px');
  }
});
