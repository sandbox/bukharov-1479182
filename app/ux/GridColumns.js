/**
 * Column that displays booleans as check marks.
 */
Ext.define('Ext.ux.column.CheckBoolean', {
  extend: 'Ext.grid.column.Column',
  alias: 'widget.checkcolumn',

  renderer: function(v, meta) {
    if(v) {
      meta.tdCls += ' ea-grid-checkmark';
    }
    return '&#160;';
  }
});
