/**
 * Ext.form.FieldSet extensions.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.form.FieldSet
 */
Ext.override(Ext.form.FieldSet, {

  /**
   * For some reason ExtJS guys decided not to fire collapse/expand events from FieldSet, so we fix that.
   */
  toggle: function() {
    this.callOverridden();
    if(this.collapsed) {
      this.fireEvent('collapse');
    }
    else {
      this.fireEvent('expand');
    }
  }
});
