/**
 * Component loader that uses Ext.Direct to load components into a container.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.panel.Panel-cfg-loader
 * @see http://www.sencha.com/products/extjs/extdirect
 */
Ext.define('Ext.ux.DirectComponentLoader', {

  mixins: {
    observable: 'Ext.util.Observable'
  },

  autoLoad: false,

  target: null,

  loadMask: false,

  isLoader: true,

  removeAll: true,

  constructor: function(config) {
    var me = this,
      autoLoad;

    config = config || {};
    Ext.apply(me, config);
    me.setTarget(me.target);
    me.addEvents('load');

    me.mixins.observable.constructor.call(me);
    if (me.autoLoad) {
      autoLoad = me.autoLoad;
      if (autoLoad === true) {
        autoLoad = {};
      }
      me.load(autoLoad);
    }
  },

  setTarget: function(target) {
    var me = this;

    if (Ext.isString(target)) {
      target = Ext.getCmp(target);
    }

    me.target = target;
  },

  removeMask: function() {
    this.target.setLoading(false);
  },

  addMask: function() {
    this.target.setLoading(true);
  },

  load: function(options) {
    if (!this.target) {
      Ext.Error.raise('A valid target is required when loading content');
    }

    if(this.loadMask) {
      this.addMask(this.loadMask);
    }

    this.directFn(this.directParams, function(items) {
      if(this.removeAll) {
        this.target.removeAll();
      }

      this.target.add(items);

      if(this.loadMask) {
        this.removeMask();
      }
      this.fireEvent('load', this);
    }, this);
  }
});
