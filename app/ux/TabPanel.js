/**
 * Ext.tab.Panel extensions.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.tab.Panel
 */
Ext.override(Ext.tab.Panel, {

  /**
   * Overrides doRemove.
   *
   * Standard doRemove function always activates the first tab if a tab is removed.
   * It is awkward so we fix that.
   */
  doRemove: function(item) {
    var prev = item.prev();
    if(prev && !item.next()) {
      this.setActiveTab(prev);
    }

    this.callOverridden(arguments);
  }
});
