/**
 * Ext.Date object extensions.
 *
 * @see http://docs.sencha.com/ext-js/4-0/#!/api/Ext.Date
 */
Ext.apply(Ext.Date, {

  /**
   * Format a time interval with the requested granularity.
   */
  formatDelta: function(delta, granularity) {
    if(!granularity) {
      granularity = 2;
    }

    delta = delta / 1000;
    var result = [];
    var units = {
      31536000: ['1 year', '@count years'],
      604800: ['1 week', '@count weeks'],
      86400: ['1 day', '@count days'],
      3600: ['1 hour', '@count hours'],
      60: ['1 min', '@count min'],
      1: ['1 sec', '@count sec']
    };

    Ext.Object.each(units, function(key, value) {
      if(delta >= key) {
        var num = parseInt(delta / key);
        result.push(Drupal.formatPlural(num, value[0], value[1]));
        delta %= key;
        granularity--;

        if(!granularity) {
          return false;
        }
      }
    });

    return result.length ? result.join(' ') : Drupal.t('0 sec');
  },

  /**
   * Format a time interval with the requested granularity.
   */
  formatInterval: function(date, granularity) {
    return Ext.Date.formatDelta(new Date() - date, granularity);
  }
});
