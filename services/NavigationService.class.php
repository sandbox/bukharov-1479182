<?php

/**
 * @file
 * NavigationService class definition.
 */

include_once drupal_get_path('module', 'system') . '/system.admin.inc';

/**
 * Service to process administration menu.
 */
class NavigationService extends ServiceBase {

  /**
   * Returns ExtJS xtype based on menu link data.
   */
  private function getXtype($callback, $link, &$args) {
    switch ($callback) {
      case 'locale_inc_callback':
        if ($args[0] == 'drupal_get_form') {
          array_shift($args);
          $xtype = array_shift($args);
          return $xtype;
        }
        break;

      case 'drupal_get_form':
        return array_shift($args);
      case 'system_admin_menu_block_page':
        return str_replace('/', '_', $link['link_path']);
    }
    return $callback;
  }

  /**
   * Generates a list of nodes representing menu links for given parent link.
   */
  private function getChildren($node) {
    $result = array();
    foreach ($node['below'] as $child) {
      $args = unserialize($child['link']['page_arguments']);
      $xtype = $this->getXtype($child['link']['page_callback'], $child['link'], $args);
      if ($xtype == 'help_main' || $child['link']['hidden']) {
        continue;
      }

      foreach ($args as $i => $v) {
        if (is_numeric($v)) {
          $url = explode('/', $child['link']['link_path']);
          $args[$i] = $url[$v];
        }
      }
      $item = array(
        'id' => $child['link']['mlid'],
        'text' => $child['link']['title'],
        'iconCls' => 'ea-icon-' . $xtype,
        'config' => array(
          'xtype' => $xtype,
          'args' => $args,
        ),
        'expanded' => TRUE,
      );

      if ($child['below'] && $xtype != 'menu_overview_page') {
        $item['children'] = $this->getChildren($child);
      }

      $item['leaf'] = sizeof($item['children']) == 0;

      $result[] = $item;
    }
    return $result;
  }

  /**
   * Returns administration menu tree.
   */
  public function GetTree() {
    $tree = menu_tree_all_data();
    foreach ($tree as $item) {
      if ($item['link']['page_callback'] == 'system_main_admin_page') {
        $admin = $item;
      }
      if ($item['link']['page_callback'] == 'node_add_page') {
        $content = $item;
      }
      if ($admin && $content) {
        break;
      }
    }
    $result = array();
    if ($content) {
      $result = array_merge($result, $this->getChildren(array(
        'below' => array($content),
      )));
    }
    return array_merge($result, $this->getChildren($admin));
  }
}
