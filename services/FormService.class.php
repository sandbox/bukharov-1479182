<?php

/**
 * @file
 * FormService class definition.
 */

/**
 * Form handling service.
 *
 * It is used by Ext.view.DrupalForm if derived class
 * doesn't provide its own functions.
 */
class FormService extends ServiceBase {

  /**
   * Get ExtJS presentation of form elements.
   */
  public function GetItems($form_id, $args) {
    array_unshift($args, $form_id);
    return call_user_func_array('extadmin_get_form_items', $args);
  }

  /**
   * Return form data stored in session.
   */
  public function GetData($data_id) {
    return $_SESSION['extadmin']['data'][$data_id];
  }

  /**
   * Process form submit.
   */
  public function Process($formHandler = TRUE) {
    $form = extadmin_get_form($_POST['form_id'], $_POST);
    extadmin_process_form($_POST['form_id'], $form, $_POST);
    return $this->formResult();
  }
}
