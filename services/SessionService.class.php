<?php

/**
 * @file
 * SessionService class definition.
 */

include_once drupal_get_path('module', 'system') . '/system.admin.inc';

/**
 * Session management service.
 */
class SessionService extends ServiceBase {

  /**
   * Returns only safe members of user object.
   */
  private function exposeUser($user) {
    return array(
      'uid' => $user->uid,
      'name' => $user->name,
      'mail' => $user->mail,
    );
  }

  /**
   * Returns current user.
   */
  public function GetUser() {
    global $user;
    return $this->exposeUser(user_access('access administration pages') ? $user : array());
  }

  /**
   * Set current user based on provided user name and password.
   *
   * @see user_login_submit()
   */
  public function SetUser($name, $pass, $formHandler = TRUE) {
    $user = user_authenticate(array(
      'name' => $name,
      'pass' => $pass,
    ));

    if (isset($user) && user_access('access administration pages')) {
      return array(
        'success' => TRUE,
        'user' => $this->exposeUser($user),
      );
    }

    return array(
      'success' => FALSE,
      'error' => isset($user) ? t('Access denied. You are not authorized to access this page.') : t('Sorry, unrecognized username or password.'),
    );
  }

  /**
   * Logs the current user out.
   *
   * @see user_logout()
   */
  public function Logout() {
    global $user;

    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));
    session_destroy();
    $null = NULL;
    user_module_invoke('logout', $null, $user);
    $user = drupal_anonymous_user();
  }
}
