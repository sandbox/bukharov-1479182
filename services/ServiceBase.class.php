<?php

/**
 * @file
 * ServiceBase class definition.
 */

/**
 * Base class for all services.
 */
class ServiceBase {

  /**
   * Process form result and output errors if there are any.
   */
  protected function formResult($values = array(), $errors = array(), $clear = TRUE) {
    $form_errors = form_get_errors();
    $errors = array_merge($errors, is_array($form_errors) ? $form_errors : array());
    $success = empty($errors);
    $result = array(
      'success' => $success,
    );

    drupal_get_messages($success ? 'status' : 'error', $clear);
    if ($success) {
      $result = array_merge($result, $values);
    }
    else {
      $result['errors'] = array();
      foreach ($errors as $key => $error) {
        $items = explode('][', $key);
        $key = array_shift($items) . (sizeof($items) ? '[' . implode('][', $items) . ']' : '');
        $result['errors'][$key] = is_array($error) ? array_map('strip_tags', $error) : strip_tags($error);
      }
    }

    return $result;
  }

  /**
   * Helper function to generate ORDER BY statement.
   */
  protected function getOrderBy($sort) {
    if (!$sort) {
      return '';
    }

    return ' ORDER BY ' . implode(', ', array_map(create_function('$sort', 'return $sort->property . " " . $sort->direction;'), $sort));
  }

}
